﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using SwiftApi.Models;
using SwiftApi.Models.Api;
using SwiftApi.Models.Classes;
using SwiftApi.Models.Configuration;
using SwiftApi.Repositories;
using SwiftApi.Repositories.Interfaces;
using SwiftApi.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SwiftApi.Controllers
{
    [ApiController]
    [Route("api/v1/Swift")]
    [Authorize]
    public class SwiftApiController : Controller
    {
        private readonly SwiftConfig _swiftConfig;
        private readonly ILogger<SwiftApiController> _logger;
        private readonly ExchangeConfig _exchangeConfig;
        private readonly SwiftRepository _swiftRepository;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="swiftConfig"></param>
        /// <param name="logger"></param>
        public SwiftApiController(SwiftConfig swiftConfig, ILogger<SwiftApiController> logger, ExchangeConfig exchangeConfig)
        {
            _swiftConfig = swiftConfig;
            _logger = logger;
            _exchangeConfig = exchangeConfig;
            _swiftRepository = new SwiftRepository(_swiftConfig, _logger);
        }

        /// <summary>
        /// Endpoint for the MT101 swift file.
        /// </summary>
        /// <param name="mt101"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT101")]
        [SwaggerOperation(Tags = new[] { "MT101" }, Summary = "Generate a MT101 Swift File")]
        public async Task<ActionResult> MT101(MT101 mt101, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT101(mt101, currencies, mt101.ValidateSwiftCode, mt101.ValidateIBAN);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt101), clientId);
            }
            else
            {
                _logger.LogWarning("MT101 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt101), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}. ClientId={ClientId}", JSON.Serializer(mt101), mt101.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT101(mt101, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT101, string.IsNullOrEmpty(mt101.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT101, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt101.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt101.Email))
                        {
                            mt101.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt101.Email) || mt101.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT101, mt101.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt101.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt101.Email))
                                {
                                    clientDetails.Email = mt101.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT101, mt101.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT101, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT101 for Order Ref - {}. ClientId={ClientId}", mt101.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT103 swift file.
        /// </summary>
        /// <param name="mt103"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT103")]
        [SwaggerOperation(Tags = new[] { "MT103" }, Summary = "Generate a MT103 Swift File")]
        public async Task<ActionResult> MT103(MT103 mt103, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT103(mt103, currencies, mt103.ValidateSwiftCode, mt103.ValidateIBAN);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt103), clientId);
            }
            else
            {
                _logger.LogWarning("MT103 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt103), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt103), mt103.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT103(mt103, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT103, string.IsNullOrEmpty(mt103.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT103, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt103.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt103.Email))
                        {
                            mt103.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt103.Email) || mt103.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT103, mt103.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt103.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt103.Email))
                                {
                                    clientDetails.Email = mt103.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT103, mt103.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT103, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT103 for Order Ref - {}. ClientId={ClientId}", mt103.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the Pain 001 swift file.
        /// </summary>
        /// <param name="pain001"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("Pain001")]
        [SwaggerOperation(Tags = new[] { "PAIN 001" }, Summary = "Generate a Pain 001 Swift File")]
        public async Task<IActionResult> Pain001(Pain001 pain001, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Fetch a list of valid countries to validate against.
            List<Countries> countries = await genericFunctionsRepository.FetchCountries();
            if (countries == null)
            {
                string msg = "Error retrieving country details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidatePain001(pain001, currencies, countries, pain001.ValidateSwiftCode, pain001.ValidateIBAN);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(pain001), clientId);
            }
            else
            {
                _logger.LogWarning("Pain 001 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(pain001), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(pain001), pain001.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchPain001(pain001, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.PAIN001, string.IsNullOrEmpty(pain001.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.PAIN001, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!pain001.Ftp)
                    {
                        if (string.IsNullOrEmpty(pain001.Email))
                        {
                            pain001.Email = string.Empty; /* clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(pain001.Email) || pain001.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.PAIN001, pain001.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!pain001.Ftp)
                            {
                                if (!string.IsNullOrEmpty(pain001.Email))
                                {
                                    clientDetails.Email = pain001.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.PAIN001, pain001.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.PAIN001, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift Pain 001 for Order Ref - {}. ClientId={ClientId}", pain001.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("Pain001NatWest")]
        [SwaggerOperation(Tags = new[] { "PAIN 001 NatWest" }, Summary = "Generate a Pain 001 NatWest Swift File")]
        public async Task<IActionResult> Pain001NatWest(Pain001NatWest pain001NatWest, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Fetch a list of valid countries to validate against.
            List<Countries> countries = await genericFunctionsRepository.FetchCountries();
            if (countries == null)
            {
                string msg = "Error retrieving country details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidatePain001(pain001NatWest, currencies, countries, pain001NatWest.ValidateSwiftCode, pain001NatWest.ValidateIBAN);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(pain001NatWest), clientId);
            }
            else
            {
                _logger.LogWarning("Pain 001 NatWest validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(pain001NatWest), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(pain001NatWest), pain001NatWest.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchPain001NatWest(pain001NatWest, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.PAIN001NatWest, string.IsNullOrEmpty(pain001NatWest.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.PAIN001NatWest, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!pain001NatWest.Ftp)
                    {
                        if (string.IsNullOrEmpty(pain001NatWest.Email))
                        {
                            pain001NatWest.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(pain001NatWest.Email) || pain001NatWest.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.PAIN001NatWest, pain001NatWest.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!pain001NatWest.Ftp)
                            {
                                if (!string.IsNullOrEmpty(pain001NatWest.Email))
                                {
                                    clientDetails.Email = pain001NatWest.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.PAIN001NatWest, pain001NatWest.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.PAIN001NatWest, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        dynamic parsedJson = JsonConvert.DeserializeObject(result);
                        return Ok(JsonConvert.SerializeObject(parsedJson, Formatting.Indented));
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        dynamic parsedJson = JsonConvert.DeserializeObject(result);
                        return Ok(JsonConvert.SerializeObject(parsedJson, Formatting.Indented));
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift Pain 001 NatWest for Order Ref - {}. ClientId={ClientId}", pain001NatWest.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the Pain 001 swift file.
        /// </summary>
        /// <param name="pain001"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("Pacs008")]
        [SwaggerOperation(Tags = new[] { "PACS 008" }, Summary = "Generate a Pacs 008 Swift File")]
        public async Task<IActionResult> Pacs008(Pacs008 pacs008, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Fetch a list of valid countries to validate against.
            List<Countries> countries = await genericFunctionsRepository.FetchCountries();
            if (countries == null)
            {
                string msg = "Error retrieving country details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidatePacs008(pacs008, currencies, countries, pacs008.ValidateSwiftCode, pacs008.ValidateIBAN);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(pacs008), clientId);
            }
            else
            {
                _logger.LogWarning("Pain 001 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(pacs008), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(pacs008), pacs008.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchPacs008(pacs008, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.PACS008, string.IsNullOrEmpty(pacs008.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.PACS008, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!pacs008.Ftp)
                    {
                        if (string.IsNullOrEmpty(pacs008.Email))
                        {
                            pacs008.Email = string.Empty; /* clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(pacs008.Email) || pacs008.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.PAIN001, pacs008.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!pacs008.Ftp)
                            {
                                if (!string.IsNullOrEmpty(pacs008.Email))
                                {
                                    clientDetails.Email = pacs008.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.PACS008, pacs008.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.PACS008, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift Pacs 008 for Order Ref - {}. ClientId={ClientId}", pacs008.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT192 swift file.
        /// </summary>
        /// <param name="mt192"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT192")]
        [SwaggerOperation(Tags = new[] { "MT192" }, Summary = "Generate a MT192 Swift File")]
        public async Task<ActionResult> MT192(MT192 mt192, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt192, mt192.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt192), clientId);
            }
            else
            {
                _logger.LogWarning("MT192 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt192), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt192), mt192.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT192(mt192, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT192, string.IsNullOrEmpty(mt192.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT192, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt192.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt192.Email))
                        {
                            mt192.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt192.Email) || mt192.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT192, mt192.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt192.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt192.Email))
                                {
                                    clientDetails.Email = mt192.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT192, mt192.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT192, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT199 for Order Ref - {}. ClientId={ClientId}", mt192.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT199 Swift File.
        /// </summary>
        /// <param name="mt199"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT199")]
        [SwaggerOperation(Tags = new[] { "MT199" }, Summary = "Generate a MT199 Swift File")]
        public async Task<ActionResult> MT199(MT199 mt199, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt199, mt199.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt199), clientId);
            }
            else
            {
                _logger.LogWarning("MT199 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt199), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt199), mt199.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT199(mt199, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT199, string.IsNullOrEmpty(mt199.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT199, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt199.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt199.Email))
                        {
                            mt199.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt199.Email) || mt199.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT199, mt199.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt199.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt199.Email))
                                {
                                    clientDetails.Email = mt199.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT199, mt199.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT199, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT199 for Order Ref - {}. ClientId={ClientId}", mt199.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT200 swift file.
        /// </summary>
        /// <param name="mt200"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT200")]
        [SwaggerOperation(Tags = new[] { "MT200" }, Summary = "Generate a MT200 Swift File")]
        public async Task<ActionResult> MT200(MT200 mt200, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT200(mt200, currencies, mt200.ValidateSwiftCode, mt200.ValidateIBAN);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt200), clientId);
            }
            else
            {
                _logger.LogWarning("MT200 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt200), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt200), mt200.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT200(mt200, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT200, string.IsNullOrEmpty(mt200.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT200, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt200.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt200.Email))
                        {
                            mt200.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt200.Email) || mt200.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT200, mt200.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt200.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt200.Email))
                                {
                                    clientDetails.Email = mt200.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT200, mt200.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT200, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT200 for Order Ref - {}. ClientId={ClientId}", mt200.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT202 swift file.
        /// </summary>
        /// <param name="mt202"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT202")]
        [SwaggerOperation(Tags = new[] { "MT202" }, Summary = "Generate a MT202 Swift File")]
        public async Task<ActionResult> MT202(MT202 mt202, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT202(mt202, currencies, mt202.ValidateSwiftCode, mt202.ValidateIBAN);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt202), clientId);
            }
            else
            {
                _logger.LogWarning("MT202 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt202), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt202), mt202.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT202(mt202, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT202, string.IsNullOrEmpty(mt202.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT202, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt202.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt202.Email))
                        {
                            mt202.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt202.Email) || mt202.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT202, mt202.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt202.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt202.Email))
                                {
                                    clientDetails.Email = mt202.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT202, mt202.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT202, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT202 for Order Ref - {}. ClientId={ClientId}", mt202.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT210 swift file.
        /// </summary>
        /// <param name="mt210"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT210")]
        [SwaggerOperation(Tags = new[] { "MT210" }, Summary = "Generate a MT210 Swift File")]
        public async Task<ActionResult> MT210(MT210 mt210, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT210(mt210, currencies, mt210.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt210), clientId);
            }
            else
            {
                _logger.LogWarning("MT210 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt210), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt210), mt210.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT210(mt210, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT210, string.IsNullOrEmpty(mt210.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT210, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt210.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt210.Email))
                        {
                            mt210.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt210.Email) || mt210.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT210, mt210.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt210.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt210.Email))
                                {
                                    clientDetails.Email = mt210.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT210, mt210.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT210, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT210 for Order Ref - {}. ClientId={ClientId}", mt210.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT92 swift file.
        /// </summary>
        /// <param name="mt292"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT292")]
        [SwaggerOperation(Tags = new[] { "MT292" }, Summary = "Generate a MT292 Swift File")]
        public async Task<ActionResult> MT292(MT292 mt292, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt292, mt292.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt292), clientId);
            }
            else
            {
                _logger.LogWarning("MT292 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt292), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt292), mt292.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT292(mt292, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT292, string.IsNullOrEmpty(mt292.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT292, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt292.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt292.Email))
                        {
                            mt292.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt292.Email) || mt292.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT292, mt292.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt292.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt292.Email))
                                {
                                    clientDetails.Email = mt292.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT292, mt292.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT292, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT292 for Order Ref - {}. ClientId={ClientId}", mt292.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT299 swift file.
        /// </summary>
        /// <param name="mt299"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT299")]
        [SwaggerOperation(Tags = new[] { "MT299" }, Summary = "Generate a MT299 Swift File")]
        public async Task<ActionResult> MT299(MT299 mt299, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt299, mt299.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt299), clientId);
            }
            else
            {
                _logger.LogWarning("MT299 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt299), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt299), mt299.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT299(mt299, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT299, string.IsNullOrEmpty(mt299.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT299, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt299.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt299.Email))
                        {
                            mt299.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt299.Email) || mt299.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT299, mt299.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt299.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt299.Email))
                                {
                                    clientDetails.Email = mt299.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT299, mt299.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT299, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }

                //var errorMsg = ("Error generating the MT299 Swift File, please contact Dashro Solitions for further information. ClientId={ClientId}", clientId);
                //ResponseBody responseBody = new ResponseBody
                //{
                //    Message = errorMsg.ToString()
                //};

                //return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT299 for Order Ref - {}. ClientId={ClientId}", mt299.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT380 swift file.
        /// </summary>
        /// <param name="mt380"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT380")]
        [SwaggerOperation(Tags = new[] { "MT380" }, Summary = "Generate a MT380 Swift File")]
        public async Task<ActionResult> MT380(MT380 mt380, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT380(mt380, currencies, mt380.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt380), clientId);
            }
            else
            {
                _logger.LogWarning("MT380 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt380), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt380), mt380.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT380(mt380, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT380, string.IsNullOrEmpty(mt380.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT380, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt380.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt380.Email))
                        {
                            mt380.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt380.Email) || mt380.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT380, mt380.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt380.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt380.Email))
                                {
                                    clientDetails.Email = mt380.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT380, mt380.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT380, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT380 for Order Ref - {}. ClientId={ClientId}", mt380.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT381 swift file.
        /// </summary>
        /// <param name="mt381"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT381")]
        [SwaggerOperation(Tags = new[] { "MT381" }, Summary = "Generate a MT381 Swift File")]
        public async Task<ActionResult> MT381(MT381 mt381, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT381(mt381, currencies, mt381.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt381), clientId);
            }
            else
            {
                _logger.LogWarning("MT381 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt381), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt381), mt381.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT381(mt381, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT381, string.IsNullOrEmpty(mt381.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT381, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt381.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt381.Email))
                        {
                            mt381.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt381.Email) || mt381.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT381, mt381.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt381.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt381.Email))
                                {
                                    clientDetails.Email = mt381.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT381, mt381.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT381, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT381 for Order Ref - {}. ClientId={ClientId}", mt381.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT92 swift file.
        /// </summary>
        /// <param name="mt392"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT392")]
        [SwaggerOperation(Tags = new[] { "MT392" }, Summary = "Generate a MT392 Swift File")]
        public async Task<ActionResult> MT392(MT392 mt392, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt392, mt392.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt392), clientId);
            }
            else
            {
                _logger.LogWarning("MT392 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt392), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt392), mt392.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT392(mt392, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT392, string.IsNullOrEmpty(mt392.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT392, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt392.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt392.Email))
                        {
                            mt392.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt392.Email) || mt392.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT392, mt392.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt392.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt392.Email))
                                {
                                    clientDetails.Email = mt392.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT392, mt392.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT392, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT392 for Order Ref - {}. ClientId={ClientId}", mt392.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT399 swift file.
        /// </summary>
        /// <param name="mt399"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT399")]
        [SwaggerOperation(Tags = new[] { "MT399" }, Summary = "Generate a MT399 Swift File")]
        public async Task<ActionResult> MT399(MT399 mt399, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt399, mt399.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt399), clientId);
            }
            else
            {
                _logger.LogWarning("MT399 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt399), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt399), mt399.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT399(mt399, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT399, string.IsNullOrEmpty(mt399.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT399, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt399.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt399.Email))
                        {
                            mt399.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt399.Email) || mt399.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT399, mt399.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt399.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt399.Email))
                                {
                                    clientDetails.Email = mt399.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT399, mt399.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT399, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }

                //var errorMsg = ("Error generating the MT399 Swift File, please contact Dashro Solitions for further information. ClientId={ClientId}", clientId);
                //ResponseBody responseBody = new ResponseBody
                //{
                //    Message = errorMsg.ToString()
                //};

                //return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT399 for Order Ref - {}. ClientId={ClientId}", mt399.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT492 swift file.
        /// </summary>
        /// <param name="mt492"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT492")]
        [SwaggerOperation(Tags = new[] { "MT492" }, Summary = "Generate a MT492 Swift File")]
        public async Task<ActionResult> MT492(MT492 mt492, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt492, mt492.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt492), clientId);
            }
            else
            {
                _logger.LogWarning("MT492 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt492), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt492), mt492.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT492(mt492, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT492, string.IsNullOrEmpty(mt492.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT492, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt492.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt492.Email))
                        {
                            mt492.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt492.Email) || mt492.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT492, mt492.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt492.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt492.Email))
                                {
                                    clientDetails.Email = mt492.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT492, mt492.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT492, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT492 for Order Ref - {}. ClientId={ClientId}", mt492.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT499 swift file.
        /// </summary>
        /// <param name="mt499"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT499")]
        [SwaggerOperation(Tags = new[] { "MT499" }, Summary = "Generate a MT499 Swift File")]
        public async Task<ActionResult> MT499(MT499 mt499, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt499, mt499.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt499), clientId);
            }
            else
            {
                _logger.LogWarning("MT499 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt499), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt499), mt499.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT499(mt499, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT499, string.IsNullOrEmpty(mt499.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT499, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt499.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt499.Email))
                        {
                            mt499.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt499.Email) || mt499.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT499, mt499.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt499.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt499.Email))
                                {
                                    clientDetails.Email = mt499.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT499, mt499.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT499, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }

                //var errorMsg = ("Error generating the MT499 Swift File, please contact Dashro Solitions for further information. ClientId={ClientId}", clientId);
                //ResponseBody responseBody = new ResponseBody
                //{
                //    Message = errorMsg.ToString()
                //};

                //return BadRequest(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT499 for Order Ref - {}. ClientId={ClientId}", mt499.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT502 swift file.
        /// </summary>
        /// <param name="mt502"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT502")]
        [SwaggerOperation(Tags = new[] { "MT502" }, Summary = "Generate a MT502 Swift File")]
        public async Task<ActionResult> MT502(MT502 mt502, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT502(mt502, currencies, mt502.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt502), clientId);
            }
            else
            {
                _logger.LogWarning("MT502 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt502), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt502), mt502.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT502(mt502, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT502, string.IsNullOrEmpty(mt502.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT502, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt502.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt502.Email))
                        {
                            mt502.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt502.Email) || mt502.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT502, mt502.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt502.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt502.Email))
                                {
                                    clientDetails.Email = mt502.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT502, mt502.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT502, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT502 for Order Ref - {}. ClientId={ClientId}", mt502.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT508 swift file.
        /// </summary>
        /// <param name="mt508"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT508")]
        [SwaggerOperation(Tags = new[] { "MT508" }, Summary = "Generate a MT508 Swift File")]
        public async Task<ActionResult> MT508(MT508 mt508, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT508(mt508, mt508.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt508), clientId);
            }
            else
            {
                _logger.LogWarning("MT508 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt508), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt508), mt508.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT508(mt508, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT508, string.IsNullOrEmpty(mt508.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT508, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt508.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt508.Email))
                        {
                            mt508.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt508.Email) || mt508.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT508, mt508.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt508.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt508.Email))
                                {
                                    clientDetails.Email = mt508.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT508, mt508.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT508, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT508 for Order Ref - {}. ClientId={ClientId}", mt508.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT509 swift file.
        /// </summary>
        /// <param name="mt509"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT509")]
        [SwaggerOperation(Tags = new[] { "MT509" }, Summary = "Generate a MT509 Swift File")]
        public async Task<ActionResult> MT509(MT509 mt509, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT509(mt509, currencies, mt509.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt509), clientId);
            }
            else
            {
                _logger.LogWarning("MT509 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt509), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt509), mt509.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT509(mt509, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT509, string.IsNullOrEmpty(mt509.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT509, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt509.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt509.Email))
                        {
                            mt509.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt509.Email) || mt509.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT509, mt509.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt509.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt509.Email))
                                {
                                    clientDetails.Email = mt509.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT509, mt509.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT509, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT509 for Order Ref - {}. ClientId={ClientId}", mt509.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT515 swift file.
        /// </summary>
        /// <param name="mt515"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT515")]
        [SwaggerOperation(Tags = new[] { "MT515" }, Summary = "Generate a MT515 Swift File")]
        public async Task<ActionResult> MT515(MT515 mt515, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT515(mt515, currencies, mt515.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt515), clientId);
            }
            else
            {
                _logger.LogWarning("MT515 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt515), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt515), mt515.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT515(mt515, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT515, string.IsNullOrEmpty(mt515.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT515, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt515.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt515.Email))
                        {
                            mt515.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt515.Email) || mt515.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT515, mt515.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt515.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt515.Email))
                                {
                                    clientDetails.Email = mt515.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT515, mt515.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT515, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT515 for Order Ref - {}. ClientId={ClientId}", mt515.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT535 swift file.
        /// </summary>
        /// <param name="mt535"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT535")]
        [SwaggerOperation(Tags = new[] { "MT535" }, Summary = "Generate a MT535 Swift File")]
        public async Task<ActionResult> MT535(MT535 mt535, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT535(mt535, currencies, mt535.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt535), clientId);
            }
            else
            {
                _logger.LogWarning("MT535 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt535), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt535), mt535.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT535(mt535, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT535, string.IsNullOrEmpty(mt535.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT535, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt535.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt535.Email))
                        {
                            mt535.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt535.Email) || mt535.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        string finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT535, mt535.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt535.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt535.Email))
                                {
                                    clientDetails.Email = mt535.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT535, mt535.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT535, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT535 for Order Ref - {}. ClientId={ClientId}", mt535.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT536 swift file.
        /// </summary>
        /// <param name="mt536"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT536")]
        [SwaggerOperation(Tags = new[] { "MT536" }, Summary = "Generate a MT536 Swift File")]
        public async Task<ActionResult> MT536(MT536 mt536, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT536(mt536, currencies, mt536.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt536), clientId);
            }
            else
            {
                _logger.LogWarning("MT536 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt536), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt536), mt536.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT536(mt536, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT536, string.IsNullOrEmpty(mt536.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT536, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt536.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt536.Email))
                        {
                            mt536.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt536.Email) || mt536.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        string finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT536, mt536.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt536.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt536.Email))
                                {
                                    clientDetails.Email = mt536.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT536, mt536.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT536, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT536 for Order Ref - {}. ClientId={ClientId}", mt536.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT537 swift file.
        /// </summary>
        /// <param name="mt537"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT537")]
        [SwaggerOperation(Tags = new[] { "MT537" }, Summary = "Generate a MT537 Swift File")]
        public async Task<ActionResult> MT537(MT537 mt537, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT537(mt537, mt537.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt537), clientId);
            }
            else
            {
                _logger.LogWarning("MT537 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt537), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt537), mt537.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT537(mt537, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT537, string.IsNullOrEmpty(mt537.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT537, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt537.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt537.Email))
                        {
                            mt537.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt537.Email) || mt537.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        string finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT537, mt537.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt537.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt537.Email))
                                {
                                    clientDetails.Email = mt537.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT537, mt537.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT537, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT537 for Order Ref - {}. ClientId={ClientId}", mt537.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT540 swift file.
        /// </summary>
        /// <param name="mt540"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT540")]
        [SwaggerOperation(Tags = new[] { "MT540" }, Summary = "Generate a MT540 Swift File")]
        public async Task<ActionResult> MT540(MT540 mt540, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT540(mt540, mt540.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt540), clientId);
            }
            else
            {
                _logger.LogWarning("MT540 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt540), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt540), mt540.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT540(mt540, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT540, string.IsNullOrEmpty(mt540.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT540, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt540.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt540.Email))
                        {
                            mt540.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt540.Email) || mt540.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT540, mt540.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt540.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt540.Email))
                                {
                                    clientDetails.Email = mt540.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT540, mt540.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT540, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT540 for Order Ref - {}. ClientId={ClientId}", mt540.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT541 swift file.
        /// </summary>
        /// <param name="mt541"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT541")]
        [SwaggerOperation(Tags = new[] { "MT541" }, Summary = "Generate a MT541 Swift File")]
        public async Task<ActionResult> MT541(MT541 mt541, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT541(mt541, currencies, mt541.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt541), clientId);
            }
            else
            {
                _logger.LogWarning("MT541 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt541), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt541), mt541.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT541(mt541, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT541, string.IsNullOrEmpty(mt541.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT541, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt541.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt541.Email))
                        {
                            mt541.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt541.Email) || mt541.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT541, mt541.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt541.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt541.Email))
                                {
                                    clientDetails.Email = mt541.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT541, mt541.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT541, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT541 for Order Ref - {}. ClientId={ClientId}", mt541.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT542 Swift file.
        /// </summary>
        /// <param name="mt542"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT542")]
        [SwaggerOperation(Tags = new[] { "MT542" }, Summary = "Generate a MT542 Swift File")]
        public async Task<ActionResult> MT542(MT542 mt542, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            var genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            var sqlRepository = new SqlRepository(_swiftConfig, _logger);
            var fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            var valiated = swiftValidationRepository.ValidateMT542(mt542, mt542.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt542), clientId);
            }
            else
            {
                _logger.LogWarning("MT542 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt542), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt542), mt542.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT542(mt542, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT542, string.IsNullOrEmpty(mt542.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT542, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt542.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt542.Email))
                        {
                            mt542.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt542.Email) || mt542.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT542, mt542.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt542.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt542.Email))
                                {
                                    clientDetails.Email = mt542.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT542, mt542.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT542, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT543 for Order Ref - {}. ClientId={ClientId}", mt542.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT543 Swift file.
        /// </summary>
        /// <param name="mt543"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT543")]
        [SwaggerOperation(Tags = new[] { "MT543" }, Summary = "Generate a MT543 Swift File")]
        public async Task<ActionResult> MT543(MT543 mt543, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT543(mt543, currencies, mt543.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt543), clientId);
            }
            else
            {
                _logger.LogWarning("MT543 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt543), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt543), mt543.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT543(mt543, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT543, string.IsNullOrEmpty(mt543.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT543, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt543.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt543.Email))
                        {
                            mt543.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt543.Email) || mt543.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT543, mt543.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt543.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt543.Email))
                                {
                                    clientDetails.Email = mt543.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT543, mt543.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT543, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT543 for Order Ref - {}. ClientId={ClientId}", mt543.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT545 Swift file.
        /// </summary>
        /// <param name="mt545"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT545")]
        [SwaggerOperation(Tags = new[] { "MT545" }, Summary = "Generate a MT545 Swift File")]
        public async Task<ActionResult> MT545(MT545 mt545, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT545(mt545, currencies, mt545.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt545), clientId);
            }
            else
            {
                _logger.LogWarning("MT545 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt545), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt545), mt545.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT545(mt545, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT545, string.IsNullOrEmpty(mt545.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT545, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt545.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt545.Email))
                        {
                            mt545.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt545.Email) || mt545.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT545, mt545.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt545.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt545.Email))
                                {
                                    clientDetails.Email = mt545.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT545, mt545.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT545, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT545 for Order Ref - {}. ClientId={ClientId}", mt545.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT547 Swift file.
        /// </summary>
        /// <param name="mt547"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT547")]
        [SwaggerOperation(Tags = new[] { "MT547" }, Summary = "Generate a MT547 Swift File")]
        public async Task<ActionResult> MT547(MT547 mt547, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT547(mt547, currencies, mt547.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt547), clientId);
            }
            else
            {
                _logger.LogWarning("MT547 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt547), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt547), mt547.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT547(mt547, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT547, string.IsNullOrEmpty(mt547.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT547, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt547.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt547.Email))
                        {
                            mt547.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt547.Email) || mt547.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT547, mt547.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt547.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt547.Email))
                                {
                                    clientDetails.Email = mt547.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT547, mt547.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT547, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT547 for Order Ref - {}. ClientId={ClientId}", mt547.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT548 Swift file.
        /// </summary>
        /// <param name="mt548"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT548")]
        [SwaggerOperation(Tags = new[] { "MT548" }, Summary = "Generate a MT548 Swift File")]
        public async Task<ActionResult> MT548(MT548 mt548, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT548(mt548, currencies, mt548.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt548), clientId);
            }
            else
            {
                _logger.LogWarning("MT548 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt548), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt548), mt548.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT548(mt548, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT548, string.IsNullOrEmpty(mt548.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT548, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt548.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt548.Email))
                        {
                            mt548.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt548.Email) || mt548.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        string finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT548, mt548.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt548.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt548.Email))
                                {
                                    clientDetails.Email = mt548.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT548, mt548.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT548, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT547 for Order Ref - {}. ClientId={ClientId}", mt548.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT565 Swift file.
        /// </summary>
        /// <param name="mt565"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT565")]
        [SwaggerOperation(Tags = new[] { "MT565" }, Summary = "Generate a MT565 Swift File")]
        public async Task<ActionResult> MT565(MT565 mt565, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT565(mt565, mt565.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt565), clientId);
            }
            else
            {
                _logger.LogWarning("MT565 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt565), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt565), mt565.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT565(mt565, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT565, string.IsNullOrEmpty(mt565.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT565, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt565.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt565.Email))
                        {
                            mt565.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt565.Email) || mt565.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        string finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT565, mt565.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt565.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt565.Email))
                                {
                                    clientDetails.Email = mt565.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT565, mt565.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT565, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT599 for Order Ref - {}. ClientId={ClientId}", mt565.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT92 swift file.
        /// </summary>
        /// <param name="mt592"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT592")]
        [SwaggerOperation(Tags = new[] { "MT592" }, Summary = "Generate a MT592 Swift File")]
        public async Task<ActionResult> MT592(MT592 mt592, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt592, mt592.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt592), clientId);
            }
            else
            {
                _logger.LogWarning("MT592 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt592), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt592), mt592.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT592(mt592, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT592, string.IsNullOrEmpty(mt592.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT592, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt592.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt592.Email))
                        {
                            mt592.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt592.Email) || mt592.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT592, mt592.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt592.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt592.Email))
                                {
                                    clientDetails.Email = mt592.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT592, mt592.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT592, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT592 for Order Ref - {}. ClientId={ClientId}", mt592.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT599 Swift file.
        /// </summary>
        /// <param name="mt599"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT599")]
        [SwaggerOperation(Tags = new[] { "MT599" }, Summary = "Generate a MT599 Swift File")]
        public async Task<ActionResult> MT599(MT599 mt599, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt599, mt599.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt599), clientId);
            }
            else
            {
                _logger.LogWarning("MT599 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt599), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt599), mt599.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT599(mt599, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT599, string.IsNullOrEmpty(mt599.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT599, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt599.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt599.Email))
                        {
                            mt599.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt599.Email) || mt599.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT599, mt599.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt599.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt599.Email))
                                {
                                    clientDetails.Email = mt599.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT599, mt599.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT599, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT599 for Order Ref - {}. ClientId={ClientId}", mt599.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT92 swift file.
        /// </summary>
        /// <param name="mt692"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT692")]
        [SwaggerOperation(Tags = new[] { "MT692" }, Summary = "Generate a MT692 Swift File")]
        public async Task<ActionResult> MT692(MT692 mt692, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt692, mt692.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt692), clientId);
            }
            else
            {
                _logger.LogWarning("MT692 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt692), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt692), mt692.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT692(mt692, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT692, string.IsNullOrEmpty(mt692.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT692, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt692.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt692.Email))
                        {
                            mt692.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt692.Email) || mt692.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT692, mt692.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt692.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt692.Email))
                                {
                                    clientDetails.Email = mt692.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT692, mt692.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT692, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT692 for Order Ref - {}. ClientId={ClientId}", mt692.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT599 Swift file.
        /// </summary>
        /// <param name="mt699"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT699")]
        [SwaggerOperation(Tags = new[] { "MT699" }, Summary = "Generate a MT699 Swift File")]
        public async Task<ActionResult> MT699(MT699 mt699, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt699, mt699.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt699), clientId);
            }
            else
            {
                _logger.LogWarning("MT699 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt699), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt699), mt699.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT699(mt699, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT699, string.IsNullOrEmpty(mt699.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT699, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt699.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt699.Email))
                        {
                            mt699.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt699.Email) || mt699.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT699, mt699.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt699.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt699.Email))
                                {
                                    clientDetails.Email = mt699.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT699, mt699.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT699, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT699 for Order Ref - {}. ClientId={ClientId}", mt699.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT92 swift file.
        /// </summary>
        /// <param name="mt792"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT792")]
        [SwaggerOperation(Tags = new[] { "MT792" }, Summary = "Generate a MT792 Swift File")]
        public async Task<ActionResult> MT792(MT792 mt792, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt792, mt792.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt792), clientId);
            }
            else
            {
                _logger.LogWarning("MT792 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt792), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt792), mt792.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT792(mt792, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT792, string.IsNullOrEmpty(mt792.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT792, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt792.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt792.Email))
                        {
                            mt792.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt792.Email) || mt792.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT792, mt792.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt792.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt792.Email))
                                {
                                    clientDetails.Email = mt792.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT792, mt792.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT792, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT792 for Order Ref - {}. ClientId={ClientId}", mt792.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT799 Swift file.
        /// </summary>
        /// <param name="mt799"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT799")]
        [SwaggerOperation(Tags = new[] { "MT799" }, Summary = "Generate a MT799 Swift File")]
        public async Task<ActionResult> MT799(MT799 mt799, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt799, mt799.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt799), clientId);
            }
            else
            {
                _logger.LogWarning("MT799 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt799), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt799), mt799.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT799(mt799, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT799, string.IsNullOrEmpty(mt799.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT799, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt799.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt799.Email))
                        {
                            mt799.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt799.Email) || mt799.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT799, mt799.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt799.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt799.Email))
                                {
                                    clientDetails.Email = mt799.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT799, mt799.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT799, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT799 for Order Ref - {}. ClientId={ClientId}", mt799.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT92 swift file.
        /// </summary>
        /// <param name="mt892"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT892")]
        [SwaggerOperation(Tags = new[] { "MT892" }, Summary = "Generate a MT892 Swift File")]
        public async Task<ActionResult> MT892(MT892 mt892, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt892, mt892.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt892), clientId);
            }
            else
            {
                _logger.LogWarning("MT892 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt892), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt892), mt892.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT892(mt892, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT892, string.IsNullOrEmpty(mt892.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT892, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt892.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt892.Email))
                        {
                            mt892.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt892.Email) || mt892.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT892, mt892.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt892.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt892.Email))
                                {
                                    clientDetails.Email = mt892.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT892, mt892.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT892, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT892 for Order Ref - {}. ClientId={ClientId}", mt892.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT899 Swift file.
        /// </summary>
        /// <param name="mt899"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT899")]
        [SwaggerOperation(Tags = new[] { "MT899" }, Summary = "Generate a MT899 Swift File")]
        public async Task<ActionResult> MT899(MT899 mt899, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt899, mt899.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt899), clientId);
            }
            else
            {
                _logger.LogWarning("MT899 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt899), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt899), mt899.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT899(mt899, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT899, string.IsNullOrEmpty(mt899.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT899, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt899.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt899.Email))
                        {
                            mt899.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt899.Email) || mt899.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT899, mt899.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt899.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt899.Email))
                                {
                                    clientDetails.Email = mt899.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT899, mt899.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT899, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT899 for Order Ref - {}. ClientId={ClientId}", mt899.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT900 swift file.
        /// </summary>
        /// <param name="mt900"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT900")]
        [SwaggerOperation(Tags = new[] { "MT900" }, Summary = "Generate a MT900 Swift File")]
        public async Task<ActionResult> MT900(MT900 mt900, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT900(mt900, currencies, mt900.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt900), clientId);
            }
            else
            {
                _logger.LogWarning("MT900 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt900), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt900), mt900.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT900(mt900, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT900, string.IsNullOrEmpty(mt900.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT900, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt900.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt900.Email))
                        {
                            mt900.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt900.Email) || mt900.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT900, mt900.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt900.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt900.Email))
                                {
                                    clientDetails.Email = mt900.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT900, mt900.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT900, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT900 for Order Ref - {}. ClientId={ClientId}", mt900.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT910 swift file.
        /// </summary>
        /// <param name="mt910"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT910")]
        [SwaggerOperation(Tags = new[] { "MT910" }, Summary = "Generate a MT910 Swift File")]
        public async Task<ActionResult> MT910(MT910 mt910, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT910(mt910, currencies, mt910.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt910), clientId);
            }
            else
            {
                _logger.LogWarning("MT910 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt910), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt910), mt910.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT910(mt910, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT910, string.IsNullOrEmpty(mt910.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT910, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt910.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt910.Email))
                        {
                            mt910.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt910.Email) || mt910.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT910, mt910.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt910.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt910.Email))
                                {
                                    clientDetails.Email = mt910.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT910, mt910.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT910, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT910 for Order Ref - {}. ClientId={ClientId}", mt910.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT920 Swift file.
        /// </summary>
        /// <param name="mt920"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT920")]
        [SwaggerOperation(Tags = new[] { "MT920" }, Summary = "Generate a MT920 Swift File")]
        public async Task<ActionResult> MT920(MT920 mt920, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT920(mt920, currencies, mt920.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt920), clientId);
            }
            else
            {
                _logger.LogWarning("MT920 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt920), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt920), mt920.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT920(mt920, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT920, string.IsNullOrEmpty(mt920.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT920, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt920.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt920.Email))
                        {
                            mt920.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt920.Email) || mt920.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT920, mt920.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt920.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt920.Email))
                                {
                                    clientDetails.Email = mt920.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT920, mt920.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT920, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT920 for Order Ref - {}. ClientId={ClientId}", mt920.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT940 swift file.
        /// </summary>
        /// <param name="mt94"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT940")]
        [SwaggerOperation(Tags = new[] { "MT940" }, Summary = "Generate a MT940 Swift File")]
        public async Task<ActionResult> MT940(MT94 mt94, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT94(mt94, currencies, mt94.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt94), clientId);
            }
            else
            {
                _logger.LogWarning("MT940 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt94), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt94), mt94.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT940(mt94, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT940, string.IsNullOrEmpty(mt94.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT940, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt94.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt94.Email))
                        {
                            mt94.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt94.Email) || mt94.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT940, mt94.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt94.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt94.Email))
                                {
                                    clientDetails.Email = mt94.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT940, mt94.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT940, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT940 for Order Ref - {}. ClientId={ClientId}", mt94.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT941 swift file.
        /// </summary>
        /// <param name="mt94"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT941")]
        [SwaggerOperation(Tags = new[] { "MT941" }, Summary = "Generate a MT941 Swift File")]
        public async Task<ActionResult> MT941(MT94 mt94, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT94(mt94, currencies, mt94.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt94), clientId);
            }
            else
            {
                _logger.LogWarning("MT941 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt94), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt94), mt94.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT940(mt94, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT941, string.IsNullOrEmpty(mt94.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT941, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt94.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt94.Email))
                        {
                            mt94.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt94.Email) || mt94.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT940, mt94.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt94.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt94.Email))
                                {
                                    clientDetails.Email = mt94.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT941, mt94.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT940, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT941 for Order Ref - {}. ClientId={ClientId}", mt94.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT942 swift file.
        /// </summary>
        /// <param name="mt94"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT942")]
        [SwaggerOperation(Tags = new[] { "MT942" }, Summary = "Generate a MT942 Swift File")]
        public async Task<ActionResult> MT942(MT94 mt94, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT94(mt94, currencies, mt94.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt94), clientId);
            }
            else
            {
                _logger.LogWarning("MT942 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt94), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt94), mt94.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT940(mt94, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT942, string.IsNullOrEmpty(mt94.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT942, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt94.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt94.Email))
                        {
                            mt94.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt94.Email) || mt94.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT940, mt94.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt94.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt94.Email))
                                {
                                    clientDetails.Email = mt94.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT942, mt94.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT940, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT942 for Order Ref - {}. ClientId={ClientId}", mt94.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT950 swift file.
        /// </summary>
        /// <param name="mt94"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT950")]
        [SwaggerOperation(Tags = new[] { "MT950" }, Summary = "Generate a MT950 Swift File")]
        public async Task<ActionResult> MT950(MT95 mt95, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT95(mt95, currencies, mt95.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt95), clientId);
            }
            else
            {
                _logger.LogWarning("MT950 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt95), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt95), mt95.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT950(mt95, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT950, string.IsNullOrEmpty(mt95.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT950, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt95.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt95.Email))
                        {
                            mt95.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt95.Email) || mt95.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT950, mt95.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt95.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt95.Email))
                                {
                                    clientDetails.Email = mt95.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT950, mt95.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT950, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT942 for Order Ref - {}. ClientId={ClientId}", mt95.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT970 swift file.
        /// </summary>
        /// <param name="mt95"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT970")]
        [SwaggerOperation(Tags = new[] { "MT970" }, Summary = "Generate a MT970 Swift File")]
        public async Task<ActionResult> MT970(MT95 mt95, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT95(mt95, currencies, mt95.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt95), clientId);
            }
            else
            {
                _logger.LogWarning("MT970 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt95), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt95), mt95.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT950(mt95, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT970, string.IsNullOrEmpty(mt95.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT970, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt95.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt95.Email))
                        {
                            mt95.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt95.Email) || mt95.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT970, mt95.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt95.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt95.Email))
                                {
                                    clientDetails.Email = mt95.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT970, mt95.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT970, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT970 for Order Ref - {}. ClientId={ClientId}", mt95.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT971 swift file.
        /// </summary>
        /// <param name="mt971"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT971")]
        [SwaggerOperation(Tags = new[] { "MT971" }, Summary = "Generate a MT971 Swift File")]
        public async Task<ActionResult> MT971(MT971 mt971, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT971(mt971, currencies, mt971.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt971), clientId);
            }
            else
            {
                _logger.LogWarning("MT971 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt971), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt971), mt971.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT971(mt971, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT971, string.IsNullOrEmpty(mt971.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT971, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt971.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt971.Email))
                        {
                            mt971.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt971.Email) || mt971.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT971, mt971.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt971.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt971.Email))
                                {
                                    clientDetails.Email = mt971.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT971, mt971.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT971, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT971 for Order Ref - {}. ClientId={ClientId}", mt971.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT972 swift file.
        /// </summary>
        /// <param name="mt95"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT972")]
        [SwaggerOperation(Tags = new[] { "MT972" }, Summary = "Generate a MT972 Swift File")]
        public async Task<ActionResult> MT972(MT95 mt95, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT95(mt95, currencies, mt95.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt95), clientId);
            }
            else
            {
                _logger.LogWarning("MT972 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt95), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt95), mt95.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT950(mt95, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT972, string.IsNullOrEmpty(mt95.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT972, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt95.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt95.Email))
                        {
                            mt95.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt95.Email) || mt95.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT972, mt95.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt95.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt95.Email))
                                {
                                    clientDetails.Email = mt95.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT972, mt95.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT972, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT972 for Order Ref - {}. ClientId={ClientId}", mt95.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT973 swift file.
        /// </summary>
        /// <param name="mt973"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT973")]
        [SwaggerOperation(Tags = new[] { "MT973" }, Summary = "Generate a MT973 Swift File")]
        public async Task<ActionResult> MT973(MT973 mt973, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT973(mt973, currencies, mt973.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt973), clientId);
            }
            else
            {
                _logger.LogWarning("MT973 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt973), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt973), mt973.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT973(mt973, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT973, string.IsNullOrEmpty(mt973.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT973, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt973.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt973.Email))
                        {
                            mt973.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt973.Email) || mt973.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT973, mt973.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt973.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt973.Email))
                                {
                                    clientDetails.Email = mt973.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT973, mt973.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT973, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT973 for Order Ref - {}. ClientId={ClientId}", mt973.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT92 swift file.
        /// </summary>
        /// <param name="mt992"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT992")]
        [SwaggerOperation(Tags = new[] { "MT992" }, Summary = "Generate a MT992 Swift File")]
        public async Task<ActionResult> MT992(MT992 mt992, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT92(mt992, mt992.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt992), clientId);
            }
            else
            {
                _logger.LogWarning("MT992 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt992), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Check to see if the clients account is active and they have available requests.
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt992), mt992.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT992(mt992, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT992, string.IsNullOrEmpty(mt992.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT992, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt992.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt992.Email))
                        {
                            mt992.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt992.Email) || mt992.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT992, mt992.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt992.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt992.Email))
                                {
                                    clientDetails.Email = mt992.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT992, mt992.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT992, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT992 for Order Ref - {}. ClientId={ClientId}", mt992.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the MT999 Swift file.
        /// </summary>
        /// <param name="mt999"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("MT999")]
        [SwaggerOperation(Tags = new[] { "MT999" }, Summary = "Generate a MT999 Swift File")]
        public async Task<ActionResult> MT999(MT999 mt999, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateMT99(mt999, mt999.ValidateSwiftCode);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(mt999), clientId);
            }
            else
            {
                _logger.LogWarning("MT999 validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(mt999), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(mt999), mt999.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchMT999(mt999, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT999, string.IsNullOrEmpty(mt999.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.MT999, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!mt999.Ftp)
                    {
                        if (string.IsNullOrEmpty(mt999.Email))
                        {
                            mt999.Email = string.Empty; /*clientDetails.Email;*/
                        }
                    }

                    //Only generate a Zip file if there is an email address, else we just return the Swift file as text.
                    if (!string.IsNullOrEmpty(mt999.Email) || mt999.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        result, SwiftTypes.MT999, mt999.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!mt999.Ftp)
                            {
                                if (!string.IsNullOrEmpty(mt999.Email))
                                {
                                    clientDetails.Email = mt999.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.MT999, mt999.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.MT999, clientDetails.ClientId);
                        }

                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                    else
                    {
                        //Return the Swift file to the calling method.
                        return Ok(result);
                    }
                }
                else //Error returned.
                {
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at Swift MT999 for Order Ref - {}. ClientId={ClientId}", mt999.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the Open Banking Domestic Payment.
        /// </summary>
        /// <param name="openBankingDomesticPayment"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("OpenBankingDomesticPayment")]
        [SwaggerOperation(Tags = new[] { "Open Banking Domestic Payment" }, Summary = "Generate an Open Banking Domestic Payment JSON File")]
        public async Task<ActionResult> OpenBankingDomesticPayment(OpenPankingDomesticPayment openBankingDomesticPayment, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateNatWestDomesticPayment(openBankingDomesticPayment, currencies);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(openBankingDomesticPayment), clientId);
            }
            else
            {
                _logger.LogWarning("Open banking payment validation errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(openBankingDomesticPayment), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(openBankingDomesticPayment), openBankingDomesticPayment.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchNatWestDomesticPayment(openBankingDomesticPayment, clientId);
                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.OpenBankingDomesticPayment, string.IsNullOrEmpty(openBankingDomesticPayment.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.OpenBankingDomesticPayment, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!openBankingDomesticPayment.Ftp)
                    {
                        if (string.IsNullOrEmpty(openBankingDomesticPayment.Email))
                        {
                            openBankingDomesticPayment.Email = string.Empty;
                        }
                    }

                    if (!string.IsNullOrEmpty(openBankingDomesticPayment.Email) || openBankingDomesticPayment.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.

                        dynamic finfile = JsonConvert.DeserializeObject(result);
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        JsonConvert.SerializeObject(finfile, Formatting.Indented), SwiftTypes.OpenBankingDomesticPayment, openBankingDomesticPayment.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!openBankingDomesticPayment.Ftp)
                            {
                                if (!string.IsNullOrEmpty(openBankingDomesticPayment.Email))
                                {
                                    clientDetails.Email = openBankingDomesticPayment.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.OpenBankingDomesticPayment, openBankingDomesticPayment.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.OpenBankingDomesticPayment, clientDetails.ClientId);
                        }
                    }

                    dynamic parsedJson = JsonConvert.DeserializeObject(result);
                    return Ok(JsonConvert.SerializeObject(parsedJson, Formatting.Indented));
                }
                else
                {
                    return BadRequest(result.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at open banking for Order Ref - {}. ClientId={ClientId}", openBankingDomesticPayment.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Endpoint for the Open Banking International Payment.
        /// </summary>
        /// <param name="openBankingInternationalPayment"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpPost("OpenBankingInternationalPayment")]
        [SwaggerOperation(Tags = new[] { "Open Banking International Payment" }, Summary = "Generate an Open Banking International Payment JSON File")]
        public async Task<ActionResult> OpenBankingInternationalPayment(OpenBankingInternationalPayment openBankingInternationalPayment, [FromQuery] int clientId)
        {
            string result = string.Empty;

            //Create new instances of the repositories.
            GenericFunctionsRepository genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, _logger);
            SqlRepository sqlRepository = new SqlRepository(_swiftConfig, _logger);
            SwiftValidateRepository swiftValidationRepository = new SwiftValidateRepository(_swiftConfig, _logger);
            FileRepository fileRepository = new FileRepository(_swiftConfig, _logger);
            EmailRepository emailRepository = new EmailRepository(_swiftConfig, _logger, _exchangeConfig);

            //Fetch the File Paths.
            List<FilePaths> filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the File Export Details.
            List<FileExport> fileExports = await sqlRepository.FetchFileExports(null, null);
            FileExport finToZip = fileExports.Where(x => x.FE_Key == "FinToZip").FirstOrDefault();

            //Fetch a list of valid currencies to validate against.
            List<Currencies> currencies = await genericFunctionsRepository.FetchCurrencies();
            if (currencies == null)
            {
                string msg = "Error retrieving currency details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            List<Countries> countries = await genericFunctionsRepository.FetchCountries();
            if (countries == null)
            {
                string msg = "Error retrieving country details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload
            string valiated = swiftValidationRepository.ValidateNatWestInternationalPayment(openBankingInternationalPayment, currencies,
                countries, openBankingInternationalPayment.ValidateSwiftCode, openBankingInternationalPayment.ValidateIBAN);
            if (string.IsNullOrEmpty(valiated))
            {
                _logger.LogInformation("Successfully validated the payload {}. ClientId={ClientId}", JSON.Serializer(openBankingInternationalPayment), clientId);
            }
            else
            {
                _logger.LogWarning("Open banking payment errors {} located in the payload {}. ClientId={ClientId}", valiated, JSON.Serializer(openBankingInternationalPayment), clientId);
                return BadRequest(valiated);
            }

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);
            ResponseBody validateClient = new ResponseBody();
            validateClient = ValidateClient(clientDetails);
            if (validateClient != null)
            {
                return BadRequest(validateClient);
            }

            try
            {
                //Save the Payload to SQL, also inserts the OrderRef into the Link Field table to link the JSON and the Swift File.
                _logger.LogInformation("JSON Payload ({}) OrderRef={OrderRef}.  ClientId={ClientId}", JSON.Serializer(openBankingInternationalPayment), openBankingInternationalPayment.OrderRef, clientId);

                //Retrieve the Swift file for the submitted payload.
                result = await _swiftRepository.FetchNatWestInternationalPayment(openBankingInternationalPayment, clientId);

                if (!result.Contains("Error"))
                {
                    //Record the successful generation of a swift file
                    if (!string.IsNullOrEmpty(result))
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.OpenBankingInternationalPayment, string.IsNullOrEmpty(openBankingInternationalPayment.Email) ? RequestStatus.Preview : RequestStatus.Completed);
                    }
                    else
                    {
                        sqlRepository.InsertClientRequest(clientId, clientDetails.ApiKey, SwiftTypes.OpenBankingInternationalPayment, RequestStatus.Failed);
                    }

                    //Use Clients email address if no email in the template.
                    if (!openBankingInternationalPayment.Ftp)
                    {
                        if (string.IsNullOrEmpty(openBankingInternationalPayment.Email))
                        {
                            openBankingInternationalPayment.Email = string.Empty;
                        }
                    }

                    if (!string.IsNullOrEmpty(openBankingInternationalPayment.Email) || openBankingInternationalPayment.Ftp)
                    {
                        //Generate .fin file to be emailed to the client.

                        dynamic finfile = JsonConvert.DeserializeObject(result);
                        var finFilePath = await fileRepository.GenerateFinFile(clientDetails, $"{filePaths.Find(f => f.PathName == "SwiftExtract").PathFilePath}{_swiftConfig.FinExtractFolder}",
                        JsonConvert.SerializeObject(finfile, Formatting.Indented), SwiftTypes.OpenBankingInternationalPayment, openBankingInternationalPayment.OrderRef);

                        if (!string.IsNullOrEmpty(finFilePath))
                        {
                            if (finToZip.FE_Name == "Y")
                            {
                                clientDetails = await CreateZipFile(finFilePath, clientDetails, fileRepository);
                            }
                            else
                            {
                                clientDetails.FilePath = finFilePath;
                                clientDetails.FileName = Path.GetFileName(finFilePath);
                            }

                            //Email the zipped .fin file to the client.
                            if (!openBankingInternationalPayment.Ftp)
                            {
                                if (!string.IsNullOrEmpty(openBankingInternationalPayment.Email))
                                {
                                    clientDetails.Email = openBankingInternationalPayment.Email;
                                }
                                await emailRepository.SendEmail(clientDetails, SwiftTypes.OpenBankingInternationalPayment, openBankingInternationalPayment.OrderRef);
                            }
                            else //Insert the file details to the table SafeSearchService_FtpFiles.
                            {
                                await sqlRepository.InsertFtpFiles(clientId, clientDetails.FilePath);
                            }
                        }
                        else
                        {
                            _logger.LogError("Error occured whilst attemping to create the ({}) Fin file. ClientId={ClientId}", SwiftTypes.OpenBankingInternationalPayment, clientDetails.ClientId);
                        }
                    }

                    dynamic parsedJson = JsonConvert.DeserializeObject(result);
                    return Ok(JsonConvert.SerializeObject(parsedJson, Formatting.Indented));
                }
                else
                {
                    return BadRequest(result.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error at open banking for Order Ref - {}. ClientId={ClientId}", openBankingInternationalPayment.OrderRef, clientId);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Generate the password protected zip file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="clientDetails"></param>
        /// <param name="fileRepository"></param>
        /// <returns></returns>
        private async Task<ClientDetails> CreateZipFile(string filePath, ClientDetails clientDetails, FileRepository fileRepository)
        {
            try
            {
                if (!string.IsNullOrEmpty(filePath))
                {
                    //Now zip the file and apply password protection.
                    var finZipFilePath = await fileRepository.GenerateFinZipFile(clientDetails, filePath);
                    if (!string.IsNullOrEmpty(finZipFilePath))
                    {
                        clientDetails.FilePath = finZipFilePath;
                        clientDetails.FileName = Path.GetFileName(finZipFilePath);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error occured whilst creating the zip file. Error message ({}).  ClientId={ClientId}", ex.Message, clientDetails.ClientId);
            }

            return clientDetails;
        }

        private ResponseBody ValidateClient(ClientDetails clientDetails)
        {
            if (!clientDetails.Active)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = "Unable to generate swift files as the account is inactive."
                };

                return responseBody;
            }
            else if (clientDetails.RequestsAvailable <= 0)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"You have used your swift allocation for today. Swift file allocation {clientDetails.RequestsAllocated}, swift file used {clientDetails.RequestsUsed}."
                };

                return responseBody;
            }

            //Check to see if the end date has been reached.
            if (DateTime.UtcNow > clientDetails.EndDate)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"Your end date ({String.Format("{0:dd/MM/yyyy}", clientDetails.EndDate)}) for generating swift files has been reached."
                };
                return responseBody;
            }

            return null;
        }
    }
}