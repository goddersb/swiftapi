﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Threading.Tasks;

namespace SwiftApi.Repositories.Repo_Extensions
{
    public class Extensions
    {
        private readonly ILogger _logger;

        public Extensions(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Convert SQl data reader to a List.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        public  async Task<List<T>> DataReaderMapToList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();
            while (dr.Read())
            {
                T obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                }
                list.Add(obj);
            }
            return list;
        }
    }
}
