﻿using Microsoft.Extensions.Logging;
using SwiftApi.Models;
using SwiftApi.Models.Api;
using SwiftApi.Models.Configuration;
using SwiftApi.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SwiftApi.Repositories
{
    public class SwiftValidateRepository : ISwiftValidate
    {
        private readonly StringBuilder _response = new StringBuilder();
        private static int _Ind = 1;

        public SwiftValidateRepository(SwiftConfig swiftConfig, ILogger logger)
        {
        }

        /// <summary>
        /// Validate the MT101 Swift Request.
        /// </summary>
        /// <param name="mt101"></param>
        /// <param name="currencies"></param>
        /// <param name="logger"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT101(MT101 mt101, List<Currencies> currencies, bool validateSwift, bool validateIban)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt101.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt101.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt101.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Settle Date.
            if (string.IsNullOrEmpty(mt101.SettleDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt101.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt101.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount.
            if (mt101.Amount <= 0 || !IsNumericFromTryParse(mt101.Amount.ToString()))
            {
                BuildResponse($"{(!IsNumericFromTryParse(mt101.Amount.ToString()) ? "Settle ammount must contain numeric values only." : "Settle ammount must be greater than 0.")}");
            }

            //Ccy.
            if (string.IsNullOrEmpty(mt101.Ccy) || !currencies.Exists(c => c.CurrencyName == mt101.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt101.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt101.Ccy} used.")}");
            }

            //Order Swift
            if (!string.IsNullOrEmpty(mt101.OrderSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt101.OrderSwift))
                    {
                        BuildResponse($"Order swift code ({mt101.OrderSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Order IBAN
            if (validateIban)
            {
                if (!string.IsNullOrEmpty(mt101.OrderIban))
                {
                    if (!ValidIBAN(mt101.OrderIban))
                    {
                        BuildResponse($"Order IBAN ({mt101.OrderIban}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Bank Swift and IBAN.
            if (!string.IsNullOrEmpty(mt101.BeneficiaryBankName))
            {
                if (!string.IsNullOrEmpty(mt101.BeneficiaryIBAN) && !string.IsNullOrEmpty(mt101.BeneficiaryBankIBAN) && !string.IsNullOrEmpty(mt101.BeneficiaryName))
                {
                    BuildResponse("Either a Beneficiary IBAN, Beneficiary Bank IBAN or Beneficiary Name must be supplied.");
                }

                if (validateSwift)
                {
                    if (!string.IsNullOrEmpty(mt101.BeneficiarySwift))
                    {
                        if (!ValidSwift(mt101.BeneficiarySwift))
                        {
                            BuildResponse($"Beneficiary swift code ({mt101.BeneficiarySwift}) appears to be incorrectly formatted.");
                        }
                    }

                    if (!string.IsNullOrEmpty(mt101.BeneficiaryBankSwift))
                    {
                        if (!ValidSwift(mt101.BeneficiaryBankSwift))
                        {
                            BuildResponse($"Beneficiary Bank swift code ({mt101.BeneficiaryBankSwift}) appears to be incorrectly formatted.");
                        }
                    }
                }

                if (validateIban)
                {
                    if (!string.IsNullOrEmpty(mt101.BeneficiaryIBAN))
                    {
                        if (!ValidIBAN(mt101.BeneficiaryIBAN))
                        {
                            BuildResponse($"Beneficiary bank IBAN ({mt101.BeneficiaryIBAN}) appears to be incorrectly formatted.");
                        }
                    }

                    if (!string.IsNullOrEmpty(mt101.BeneficiaryBankIBAN))
                    {
                        if (!ValidIBAN(mt101.BeneficiaryBankIBAN))
                        {
                            BuildResponse($"Beneficiary bank IBAN ({mt101.BeneficiaryBankIBAN}) appears to be incorrectly formatted.");
                        }
                    }
                }
            }

            //Beneficiary Sub Bank Swfit and IBAN
            if (!string.IsNullOrEmpty(mt101.BeneficiarySubBankName))
            {
                if (!string.IsNullOrEmpty(mt101.BeneficiarySubBankSwift))
                {
                    if (validateSwift)
                    {
                        if (!ValidSwift(mt101.BeneficiarySubBankSwift))
                        {
                            BuildResponse($"Beneficiary sub bank swift code ({mt101.BeneficiarySubBankSwift}) appears to be incorrectly formatted.");
                        }
                    }
                }
                else
                {
                    BuildResponse("Beneficiary sub bank swift code cannot be null or missing when the Beneficiary Sub Bank Name has been supplied.");
                }

                if (!string.IsNullOrEmpty(mt101.BeneficiarySubBankIBAN))
                {
                    if (validateIban)
                    {
                        if (!ValidIBAN(mt101.BeneficiarySubBankIBAN))
                        {
                            BuildResponse($"Beneficiary sub bank IBAN ({mt101.BeneficiarySubBankIBAN}) appears to be incorrectly formatted.");
                        }
                    }
                }
                else
                {
                    BuildResponse("Beneficiary sub bank IBAN cannot be null or missing when the Beneficiary Sub Bank Name has been supplied.");
                }
            }

            //Email.
            if (!string.IsNullOrEmpty(mt101.Email))
            {
                string[] emails = mt101.Email.Split(";");
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT103 Swift Request.
        /// </summary>
        /// <param name="mt103"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT103(MT103 mt103, List<Currencies> currencies, bool validateSwift, bool validateIban)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt103.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt103.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt103.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Settle Date.
            if (string.IsNullOrEmpty(mt103.SettleDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt103.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt103.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount.
            if (mt103.Amount < 0 || !IsNumericFromTryParse(mt103.Amount.ToString()))
            {
                BuildResponse($"{(!IsNumericFromTryParse(mt103.Amount.ToString()) ? "Settle ammount must contain numeric values only." : "Settle ammount must be 0 or greater.")}");
            }

            //Ccy.
            if (string.IsNullOrEmpty(mt103.Ccy) || !currencies.Exists(c => c.CurrencyName == mt103.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt103.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt103.Ccy} used.")}");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt103.OrderRef))
            {
                BuildResponse($"Order ref cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt103.OrderSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt103.OrderSwift))
                    {
                        BuildResponse($"Order swift code ({mt103.OrderSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Order IBAN/Account Number.
            if (!string.IsNullOrEmpty(mt103.OrderIbanAccountNumber))
            {
                if (!mt103.OrderIbanAccountNumber.All(Char.IsDigit))
                {
                    if (validateIban)
                    {
                        if (!ValidIBAN(mt103.OrderIbanAccountNumber))
                        {
                            BuildResponse($"Order IBAN ({mt103.OrderIbanAccountNumber}) appears to be incorrectly formatted.");
                        }
                    }
                }
            }

            //Beneficiary Swift Code.
            if (validateSwift)
            {
                if (!string.IsNullOrEmpty(mt103.BeneficiarySwift))
                {
                    if (!ValidSwift(mt103.BeneficiarySwift))
                    {
                        BuildResponse($"Beneficiary swift code ({mt103.BeneficiarySwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Benficiary IBAN.
            if (validateIban)
            {
                if (!string.IsNullOrEmpty(mt103.BeneficiaryIBAN))
                {
                    if (!ValidIBAN(mt103.BeneficiaryIBAN))
                    {
                        BuildResponse($"Beneficiary IBAN ({mt103.BeneficiaryIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Ccy.
            if (!string.IsNullOrEmpty(mt103.BeneficiaryCcy))
            {
                if (!currencies.Exists(c => c.CurrencyName == mt103.Ccy.ToUpper()))
                {
                    BuildResponse($"Invalid benficiary currency value {mt103.BeneficiaryCcy} used.");
                }
            }

            //Beneficiary Bank Swift Code.
            if (!string.IsNullOrEmpty(mt103.BeneficiaryBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt103.BeneficiaryBankSwift))
                    {
                        BuildResponse($"Beneficiary bank swift code ({mt103.BeneficiaryBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Bank IBAN.
            if (!string.IsNullOrEmpty(mt103.BeneficiaryBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(mt103.BeneficiaryBankIBAN))
                    {
                        BuildResponse($"Beneficiary bank IBAN ({mt103.BeneficiaryBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Sub Bank Swift Code.
            if (!string.IsNullOrEmpty(mt103.BeneficiarySubBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt103.BeneficiarySubBankSwift))
                    {
                        BuildResponse($"Beneficiary sub bank swift code ({mt103.BeneficiarySubBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Sub Bank IBAN.
            if (!string.IsNullOrEmpty(mt103.BeneficiarySubBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(mt103.BeneficiarySubBankIBAN))
                    {
                        BuildResponse($"Beneficiary sub bank IBAN ({mt103.BeneficiarySubBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email.
            if (!string.IsNullOrEmpty(mt103.Email))
            {
                string[] emails = mt103.Email.Split(";");
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT192 Swift Request.
        /// </summary>
        /// <param name="mt192"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT92<T>(T item, bool validateSwift)
        {
            dynamic mt92 = item;

                //Client Swift Code.
                if (!string.IsNullOrEmpty(mt92.ClientSwiftCode))
                {
                    if (validateSwift)
                    {
                        if (!ValidSwift(mt92.ClientSwiftCode))
                        {
                            BuildResponse($"Client swift code ({mt92.ClientSwiftCode}) appears to be incorrectly formatted.");
                        }
                    }
                }
                else
                {
                    BuildResponse("Client swift code cannot be null or missing.");
                }

                //Order Ref.
                if (string.IsNullOrEmpty(mt92.OrderRef))
                {
                    BuildResponse("Order Ref cannot be null or missing.");
                }

                //Order Related Ref.
                if (string.IsNullOrEmpty(mt92.OrderRelatedRef))
                {
                    BuildResponse("Order Related Ref cannot be null or missing.");
                }

                if (!string.IsNullOrEmpty(mt92.OrderRelatedDate.ToString()))
                {
                    if (DateTime.TryParse(mt92.OrderRelatedDate.ToString(), out DateTime tempTo) == false)
                    {
                        BuildResponse($"Invalid format for order related date ({mt92.OrderRelatedDate}), this must be yyyy-mm-dd and be a valid date");
                    }
                }

                //Order Swift Code.
                if (!string.IsNullOrEmpty(mt92.OrderSwift))
                {
                    if (validateSwift)
                    {
                        if (!ValidSwift(mt92.OrderSwift))
                        {
                            BuildResponse($"Order swift code ({mt92.OrderSwift}) appears to be incorrectly formatted.");
                        }
                    }
                }
                else
                {
                    BuildResponse("Order swift code cannot be null or missing.");
                }

                //Message Type.
                if (string.IsNullOrEmpty(mt92.MessageType.ToString()))
                {
                    BuildResponse("Message type cannot be null or missing.");
                }
                else
                {
                    if (!((string)Convert.ToString(mt92.MessageType)).All(char.IsDigit))
                    {
                        BuildResponse("Message type must only contain numeric values.");
                    }
                }

                //Email
                if (!string.IsNullOrEmpty(mt92.Email))
                {
                    string[] emails = mt92.Email.Split(';');
                    foreach (string email in emails)
                    {
                        if (!new EmailAddressAttribute().IsValid(email))
                        {
                            BuildResponse($"Invalid email address {email} used.");
                        }
                    }
                }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT199 to MT999 Swift Requests.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT99<T>(T item, bool validateSwift)
        {
            dynamic mt99 = item;

            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt99.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt99.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt99.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt99.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt99.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt99.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt99.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Message/Narrative
            if (string.IsNullOrEmpty(mt99.Message))
            {
                BuildResponse("Message cannot be null or missing.");
            }

            //Email
            if (!string.IsNullOrEmpty(mt99.Email))
            {
                string[] emails = mt99.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT200 Swift Request.
        /// </summary>
        /// <param name="mt200"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT200(MT200 mt200, List<Currencies> currencies, bool validateSwift, bool validateIban)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt200.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt200.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt200.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt200.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt200.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt200.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt200.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt200.SettleDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt200.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt200.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount.
            if (string.IsNullOrEmpty(mt200.Amount.ToString()))
            {
                BuildResponse("Amount cannot be null or missing.");
            }
            else if (!IsNumericFromTryParse(mt200.Amount.ToString()))
            {
                BuildResponse($"Settle ammount {mt200.Amount} must contain numeric values only.");
            }

            //Ccy
            if (string.IsNullOrEmpty(mt200.Ccy) || !currencies.Exists(c => c.CurrencyName == mt200.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt200.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt200.Ccy} used.")}");
            }

            //Order IBAN.
            if (!string.IsNullOrEmpty(mt200.OrderIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(mt200.OrderIBAN))
                    {
                        BuildResponse($"Order IBAN ({mt200.OrderIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Ben IBAN.
            if (!string.IsNullOrEmpty(mt200.BenIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(mt200.BenIBAN))
                    {
                        BuildResponse($"Beneficiary IBAN ({mt200.BenIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt200.Email))
            {
                string[] emails = mt200.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }
            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT202 Swift Request.
        /// </summary>
        /// <param name="mt202"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT202(MT202 mt202, List<Currencies> currencies, bool validateSwift, bool validateIban)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt202.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt202.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt202.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Client swift code cannot be null or missing.");
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt202.SettleDate.ToString()))
            {
                BuildResponse("Settle Date cannot be null or missing.");
            }

            //Amount.
            if (string.IsNullOrEmpty(mt202.Amount.ToString()))
            {
                BuildResponse("Amount cannot be null or missing.");
            }
            else if (!IsNumericFromTryParse(mt202.Amount.ToString()))
            {
                BuildResponse($"Settle ammount {mt202.Amount} must contain numeric values only.");
            }

            //Currency.
            if (string.IsNullOrEmpty(mt202.Ccy) || !currencies.Exists(c => c.CurrencyName == mt202.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt202.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt202.Ccy} used.")}");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt202.OrderRef))
            {
                BuildResponse($"Order ref cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt202.OrderSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt202.OrderSwift))
                    {
                        BuildResponse($"Order swift code ({mt202.OrderSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Order Account Number.
            if (!string.IsNullOrEmpty(mt202.OrderAccountNumber))
            {
                if (!mt202.OrderAccountNumber.Replace("-", "").All(char.IsNumber))
                {
                    BuildResponse($"Order account number must contain numeric values only.");
                }
            }

            //Order IBAN.
            if (!string.IsNullOrEmpty(mt202.OrderIban))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(mt202.OrderIban))
                    {
                        BuildResponse($"Order IBAN ({mt202.OrderIban}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Swift Code.
            if (!string.IsNullOrEmpty(mt202.BeneficiarySwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt202.BeneficiarySwift))
                    {
                        BuildResponse($"Beneficiary swift code ({mt202.BeneficiarySwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Account Number.
            if (!string.IsNullOrEmpty(mt202.BeneficiaryAccountNumber))
            {
                if (!mt202.BeneficiaryAccountNumber.Replace("-", "").All(char.IsNumber))
                {
                    BuildResponse($"Beneficiary account number must contain numeric values only.");
                }
            }

            //Beneficiary IBAN.
            if (!string.IsNullOrEmpty(mt202.BeneficiaryIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(mt202.BeneficiaryIBAN))
                    {
                        BuildResponse($"Beneficiary bank IBAN ({mt202.BeneficiaryIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Ccy.
            if (!string.IsNullOrEmpty(mt202.BeneficiaryCcy))
            {
                if (!currencies.Exists(c => c.CurrencyName == mt202.BeneficiaryCcy.ToUpper()))
                {
                    BuildResponse($"Invalid ben currency value {mt202.BeneficiaryCcy} used.");
                }
            }

            //Beneficiary Sub Bank Swift.
            if (!string.IsNullOrEmpty(mt202.BeneficiarySubBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt202.BeneficiarySubBankSwift))
                    {
                        BuildResponse($"Beneficiary sub bank swift code ({mt202.BeneficiarySubBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Sub Bank IBAN.
            if (!string.IsNullOrEmpty(mt202.BeneficiarySubBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(mt202.BeneficiarySubBankIBAN))
                    {
                        BuildResponse($"Beneficiary sub bank IBAN ({mt202.BeneficiarySubBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Bank Swift.
            if (!string.IsNullOrEmpty(mt202.BeneficiaryBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt202.BeneficiaryBankSwift))
                    {
                        BuildResponse($"Beneficiary Bank swift code ({mt202.BeneficiaryBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Bank IBAN.
            if (!string.IsNullOrEmpty(mt202.BeneficiaryBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(mt202.BeneficiaryBankIBAN))
                    {
                        BuildResponse($"Beneficiary bank IBAN ({mt202.BeneficiaryBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email.
            if (!string.IsNullOrEmpty(mt202.Email))
            {
                string[] emails = mt202.Email.Split(");");
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT210 Swift Request.
        /// </summary>
        /// <param name="mt210"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT210(MT210 mt210, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt210.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt210.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt210.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Beneficiary Swift Code.
            if (!string.IsNullOrEmpty(mt210.BenSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt210.BenSwiftCode))
                    {
                        BuildResponse($"Beneficiary swift code ({mt210.BenSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Beneficiary swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt210.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Related Order Ref.
            if (string.IsNullOrEmpty(mt210.RelatedOrderRef))
            {
                BuildResponse("Related Order Ref cannot be null or missing.");
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt210.SettleDate.ToString()))
            {
                BuildResponse("Settle Date cannot be null or missing.");
            }

            //Amount.
            if (string.IsNullOrEmpty(mt210.Amount.ToString()))
            {
                BuildResponse("Amount cannot be null or missing.");
            }
            else if (!IsNumericFromTryParse(mt210.Amount.ToString()))
            {
                BuildResponse($"Settle ammount {mt210.Amount} must contain numeric values only.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt210.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt210.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({mt210.OrderSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Ben Sub Bank Swift Code.
            if (!string.IsNullOrEmpty(mt210.BenSubBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt210.BenSubBankSwift))
                    {
                        BuildResponse($"Beneficiary Sub Bank swift code ({mt210.BenSubBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Ccy
            if (string.IsNullOrEmpty(mt210.Ccy) || !currencies.Exists(c => c.CurrencyName == mt210.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt210.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt210.Ccy} used.")}");
            }

            //Email
            if (!string.IsNullOrEmpty(mt210.Email))
            {
                string[] emails = mt210.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }
            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT380 Swift Request.
        /// </summary>
        /// <param name="mt380"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT380(MT380 mt380, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt380.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt380.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt380.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt380.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt380.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt380.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt380.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt380.PreviousOrderRef) && mt380.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when Trade Type set as CANC");
            }

            //Linked Message Type.
            //if (string.IsNullOrEmpty(mt380.LinkMessageType) && mt380.TradeType == "CANC")
            //{
            //    BuildResponse("Linked message type cannot be null or missing when Trade Type set as CANC");
            //}

            //Trade Type
            if (!string.IsNullOrEmpty(mt380.TradeType))
            {
                if (mt380.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Safekeeping Account
            if (string.IsNullOrEmpty(mt380.SafeKeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //Trade Date.
            if (string.IsNullOrEmpty(mt380.TradeDate.ToString()))
            {
                BuildResponse("Trade Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt380.TradeDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Trade Date ({mt380.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt380.SettlementDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt380.SettlementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt380.SettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Order Amount.
            if (!string.IsNullOrEmpty(mt380.OrderAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt380.OrderAmount.ToString()))
                {
                    BuildResponse("Order amount must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Order amount cannot be null or missing.");
            }

            //Order Ccy
            if (string.IsNullOrEmpty(mt380.OrderCcy) || !currencies.Exists(c => c.CurrencyName == mt380.OrderCcy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt380.OrderCcy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt380.OrderCcy} used.")}");
            }

            //Counter Ccy
            if (string.IsNullOrEmpty(mt380.CounterCcy) || !currencies.Exists(c => c.CurrencyName == mt380.CounterCcy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt380.CounterCcy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt380.CounterCcy} used.")}");
            }

            //Message.
            if (string.IsNullOrEmpty(mt380.Message))
            {
                BuildResponse("Message amount cannot be null or missing.");
            }

            //Email
            if (!string.IsNullOrEmpty(mt380.Email))
            {
                string[] emails = mt380.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT381 Swift Request.
        /// </summary>
        /// <param name="mt381"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT381(MT381 mt381, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt381.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt381.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt381.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt381.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt381.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt381.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt381.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt381.PreviousOrderRef) && mt381.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when Trade Type set as CANC");
            }

            //Linked Message Type.
            //if (string.IsNullOrEmpty(mt381.LinkMessageType) && mt381.TradeType == "CANC")
            //{
            //    BuildResponse("Linked message type cannot be null or missing when Trade Type set as CANC");
            //}

            //Trade Type
            if (!string.IsNullOrEmpty(mt381.TradeType))
            {
                if (mt381.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Safekeeping Account
            if (string.IsNullOrEmpty(mt381.SafeKeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //Order Date and Time.
            if (string.IsNullOrEmpty(mt381.OrderDateAndTime.ToString()))
            {
                BuildResponse("Order date and time cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt381.OrderDateAndTime.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Order date and time ({mt381.OrderDateAndTime}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Trade Date.
            if (string.IsNullOrEmpty(mt381.TradeDate.ToString()))
            {
                BuildResponse("Trade Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt381.TradeDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Trade Date ({mt381.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt381.SettlementDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt381.SettlementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt381.SettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Order Amount.
            if (!string.IsNullOrEmpty(mt381.OrderAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt381.OrderAmount.ToString()))
                {
                    BuildResponse("Order amount must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Order amount cannot be null or missing.");
            }

            //Order Ccy
            if (string.IsNullOrEmpty(mt381.OrderCcy) || !currencies.Exists(c => c.CurrencyName == mt381.OrderCcy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt381.OrderCcy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt381.OrderCcy} used.")}");
            }

            //Exchange Rate.
            if (!string.IsNullOrEmpty(mt381.ExchangeRate.ToString()))
            {
                if (!IsNumericFromTryParse(mt381.ExchangeRate.ToString()))
                {
                    BuildResponse("Exchange rate must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Exchange rate cannot be null or missing.");
            }

            //Message.
            if (string.IsNullOrEmpty(mt381.Message))
            {
                BuildResponse("Message amount cannot be null or missing.");
            }

            //Email
            if (!string.IsNullOrEmpty(mt381.Email))
            {
                string[] emails = mt381.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        ///  Validate the MT502 Swift Request.
        /// </summary>
        /// <param name="mt502"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT502(MT502 mt502, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt502.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt502.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt502.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt502.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt502.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt502.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt502.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Trade Type
            if (!string.IsNullOrEmpty(mt502.TradeType))
            {
                if (mt502.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Indicator A
            if (!string.IsNullOrEmpty(mt502.IndicatorA))
            {
                if (mt502.IndicatorA.Any(char.IsDigit))
                {
                    BuildResponse("Indicator A contains invalid characters.");
                }
            }

            //Indicator B
            if (!string.IsNullOrEmpty(mt502.IndicatorB))
            {
                if (mt502.IndicatorB.Any(char.IsDigit))
                {
                    BuildResponse("Indicator B contains invalid characters.");
                }
            }

            //Ccy
            if (string.IsNullOrEmpty(mt502.Ccy) || !currencies.Exists(c => c.CurrencyName == mt502.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt502.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt502.Ccy} used.")}");
            }

            //Quantity
            if (mt502.Quantity > 0)
            {
                if (string.IsNullOrEmpty(mt502.Quantity.ToString()))
                {
                    BuildResponse("Quantity cannot be null or missing.");
                }
                else
                {
                    if (!IsNumericFromTryParse(mt502.Quantity.ToString()))
                    {
                        BuildResponse("Quantity must contain numeric values only.");
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(mt502.Amount.ToString()))
                {
                    BuildResponse("Amount cannot be null or missing.");
                }
                else
                {
                    if (!IsNumericFromTryParse(mt502.Amount.ToString()))
                    {
                        BuildResponse("Amount must contain numeric values only.");
                    }
                }
            }

            //Cancel Order Ref
            if (mt502.TradeType != "NEWM")
            {
                if (string.IsNullOrEmpty(mt502.CancelOrderRef))
                {
                    BuildResponse("Cancel order ref cannot be null or missing.");
                }
            }

            //Broker Account Number.
            if (string.IsNullOrEmpty(mt502.BrokerAccountNumber))
            {
                BuildResponse("Broker account number cannot be null or missing.");
            }

            //Place Indicator A
            if (!string.IsNullOrEmpty(mt502.PlaceIndicatorA))
            {
                if (mt502.PlaceIndicatorA.Any(char.IsDigit))
                {
                    BuildResponse("Place indicator a contains invalid characters.");
                }
            }

            //Place Indicator A
            if (!string.IsNullOrEmpty(mt502.PlaceIndicatorB))
            {
                if (mt502.PlaceIndicatorB.Any(char.IsDigit))
                {
                    BuildResponse("Place indicator b contains invalid characters.");
                }
            }

            //Place Indicator
            if (!string.IsNullOrEmpty(mt502.PlaceIndicator))
            {
                if (mt502.PlaceIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Place indicator contains invalid characters.");
                }
            }

            //Order Type
            if (!string.IsNullOrEmpty(mt502.OrderType))
            {
                if (mt502.OrderType.Any(char.IsDigit))
                {
                    BuildResponse("Order type contains invalid characters.");
                }
            }

            //Order Time Limit.
            if (!string.IsNullOrEmpty(mt502.OrderTimeLimit))
            {
                if (mt502.OrderTimeLimit.Any(char.IsDigit))
                {
                    BuildResponse("Order time limit contains invalid characters.");
                }
            }

            //Order Pay Type.
            if (!string.IsNullOrEmpty(mt502.OrderPayType))
            {
                if (mt502.OrderPayType.Any(char.IsDigit))
                {
                    BuildResponse("Order pay type contains invalid characters.");
                }
            }

            //Order Settlement Type.
            if (!string.IsNullOrEmpty(mt502.OrderSettlementType))
            {
                if (mt502.OrderSettlementType.Any(char.IsDigit))
                {
                    BuildResponse("Order settlement type contains invalid characters.");
                }
            }

            //Order Settlement Condition.
            if (!string.IsNullOrEmpty(mt502.OrderSettlementCondition))
            {
                if (mt502.OrderSettlementCondition.Any(char.IsDigit))
                {
                    BuildResponse("Order settlement condition contains invalid characters.");
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt502.Email))
            {
                string[] emails = mt502.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT508 Swift Request.
        /// </summary>
        /// <param name="mt508"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT508(MT508 mt508, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt508.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt508.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt508.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt508.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt508.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({mt508.OrderSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt508.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt508.PreviousOrderRef) && mt508.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when Trade Type set as CANC");
            }

            //Linked Message Type.
            //if (string.IsNullOrEmpty(mt508.LinkedMessageType) && mt508.TradeType == "CANC")
            //{
            //    BuildResponse("Linked message type cannot be null or missing when Trade Type set as CANC");
            //}

            //Trade Type.
            if (!string.IsNullOrEmpty(mt508.TradeType))
            {
                if (mt508.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Instrument ISIN.
            if (string.IsNullOrEmpty(mt508.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument ISIN Description.
            if (string.IsNullOrEmpty(mt508.InstrumentDescription))
            {
                BuildResponse("Instrument description cannot be null or missing.");
            }

            //Quantity Type.
            if (!string.IsNullOrEmpty(mt508.QuantityType))
            {
                if (mt508.QuantityType.Any(char.IsDigit))
                {
                    BuildResponse("Quantity type contains invalid characters.");
                }
            }

            //Quantity.
            if (string.IsNullOrEmpty(mt508.Quantity.ToString()))
            {
                BuildResponse("Quantity cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt508.Quantity.ToString()))
                {
                    BuildResponse("Quantity must contain numeric values only.");
                }
            }

            //Transaction Safe Keeping Account Type.
            if (!string.IsNullOrEmpty(mt508.TransactionSafeKeepingAccountType))
            {
                if (mt508.TransactionSafeKeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("Quantity type contains invalid characters.");
                }
            }

            //Transaction Safe Keeping Account.
            if (string.IsNullOrEmpty(mt508.TransactionSafeKeepingAccount))
            {
                BuildResponse("Transaction safe keeping account cannot be null or missing.");
            }

            //From Balance Indicator.
            if (!string.IsNullOrEmpty(mt508.FromBalanceIndicator))
            {
                if (mt508.FromBalanceIndicator.Any(char.IsDigit))
                {
                    BuildResponse("From balance indicator contains invalid characters.");
                }
            }

            //To Balance Indicator.
            if (!string.IsNullOrEmpty(mt508.ToBalanceIndicator))
            {
                if (mt508.ToBalanceIndicator.Any(char.IsDigit))
                {
                    BuildResponse("To balance indicator contains invalid characters.");
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt508.Email))
            {
                string[] emails = mt508.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT509 Swift Request.
        /// </summary>
        /// <param name="mt509"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT509(MT509 mt509, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt509.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt509.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt509.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt509.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt509.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({mt509.OrderSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt509.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt509.PreviousOrderRef) && mt509.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when Trade Type set as CANC");
            }

            //Linked Message Type.
            //if (string.IsNullOrEmpty(mt509.LinkedMessageType) && mt509.TradeType == "CANC")
            //{
            //    BuildResponse("Linked message type cannot be null or missing when Trade Type set as CANC");
            //}

            //Trade Type.
            if (!string.IsNullOrEmpty(mt509.TradeType))
            {
                if (mt509.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Status Code Type.
            if (!string.IsNullOrEmpty(mt509.StatusCodeType))
            {
                if (mt509.StatusCodeType.Any(char.IsDigit))
                {
                    BuildResponse("Status code type contains invalid characters.");
                }
            }

            //Status Code.
            if (!string.IsNullOrEmpty(mt509.StatusCode))
            {
                if (mt509.StatusCode.Any(char.IsDigit))
                {
                    BuildResponse("Status code contains invalid characters.");
                }
            }

            //Reason Code Type.
            if (!string.IsNullOrEmpty(mt509.ReasonCodeType))
            {
                if (mt509.ReasonCodeType.Any(char.IsDigit))
                {
                    BuildResponse("Reason code type contains invalid characters.");
                }
            }

            //Reason Code.
            if (!string.IsNullOrEmpty(mt509.ReasonCode))
            {
                if (mt509.ReasonCode.Any(char.IsDigit))
                {
                    BuildResponse("Reason code contains invalid characters.");
                }
            }

            //Instrument ISIN.
            if (string.IsNullOrEmpty(mt509.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Quantity Ccy
            if (string.IsNullOrEmpty(mt509.QuantityCCY) || !currencies.Exists(c => c.CurrencyName == mt509.QuantityCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt509.QuantityCCY) ? "Currency cannot be null or missing." : $"Invalid currency value {mt509.QuantityCCY} used.")}");
            }

            //Quantity
            if (!string.IsNullOrEmpty(mt509.Quantity.ToString()))
            {
                if (!IsNumericFromTryParse(mt509.Quantity.ToString()))
                {
                    BuildResponse($"Quantity {mt509.Quantity} must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Quantity cannot be null or missing.");
            }

            //Email
            if (!string.IsNullOrEmpty(mt509.Email))
            {
                string[] emails = mt509.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT515 Swift Request.
        /// </summary>
        /// <param name="mt515"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT515(MT515 mt515, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt515.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt515.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt515.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt515.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt515.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({mt515.OrderSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt515.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt515.PreviousOrderRef) && mt515.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when Trade Type set as CANC");
            }

            //Linked Message Type.
            //if (string.IsNullOrEmpty(mt515.LinkedMessageType) && mt515.TradeType == "CANC")
            //{
            //    BuildResponse("Linked message type cannot be null or missing when Trade Type set as CANC");
            //}

            //Trade Type
            if (!string.IsNullOrEmpty(mt515.TradeType))
            {
                if (mt515.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Transaction Type Indicator
            if (!string.IsNullOrEmpty(mt515.TransactionTypeIndicator))
            {
                if (mt515.TransactionTypeIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Transaction type indicator contains invalid characters.");
                }
            }

            //Instrument ISIN.
            if (string.IsNullOrEmpty(mt515.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument ISIN Description
            if (string.IsNullOrEmpty(mt515.InstrumentDescription))
            {
                BuildResponse("Instrument description cannot be null or missing.");
            }

            //Deal Price.
            if (!string.IsNullOrEmpty(mt515.DealPrice.ToString()))
            {
                if (!IsNumericFromTryParse(mt515.DealPrice.ToString()))
                {
                    BuildResponse($"Deal price {mt515.DealPrice} must contain numeric values only.");
                }
            }

            //Deal Type
            if (!string.IsNullOrEmpty(mt515.DealType))
            {
                if (mt515.DealType.Any(char.IsDigit))
                {
                    BuildResponse("Deal type contains invalid characters.");
                }
            }

            //Deal Ccy
            if (string.IsNullOrEmpty(mt515.DealCcy) || !currencies.Exists(c => c.CurrencyName == mt515.DealCcy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt515.DealCcy) ? "Deal currency cannot be null or missing." : $"Invalid deal currency value {mt515.DealCcy} used.")}");
            }

            //Quantity Type
            if (!string.IsNullOrEmpty(mt515.QuantityType))
            {
                if (mt515.QuantityType.Any(char.IsDigit))
                {
                    BuildResponse("Quantity type contains invalid characters.");
                }
            }

            //Quantity
            if (string.IsNullOrEmpty(mt515.Quantity.ToString()))
            {
                BuildResponse("Quantity cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt515.Quantity.ToString()))
                {
                    BuildResponse("Quantity must contain numeric values only.");
                }
            }

            //Transaction Safe Keeping Account
            if (string.IsNullOrEmpty(mt515.TransactionSafeKeepingAccount))
            {
                BuildResponse("Transaction safe keeping account cannot be null or missing.");
            }

            //Transaction Indicator.
            if (!string.IsNullOrEmpty(mt515.TransactionIndicator))
            {
                if (mt515.TransactionIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Transaction indicator contains invalid characters.");
                }
            }

            //Buy-Sell Indicator.
            if (!string.IsNullOrEmpty(mt515.BuySellIndicator))
            {
                if (mt515.BuySellIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Buy sell indicator contains invalid characters.");
                }
            }

            //Payment Indicator.
            if (!string.IsNullOrEmpty(mt515.PaymentIndicator))
            {
                if (mt515.PaymentIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Payment indicator contains invalid characters.");
                }
            }

            //Trade Date.
            if (string.IsNullOrEmpty(mt515.TradeDate.ToString()))
            {
                BuildResponse("Trade date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt515.TradeDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for trade date ({mt515.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt515.SettlementDate.ToString()))
            {
                BuildResponse("Settlement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt515.SettlementDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for settlement date ({mt515.SettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Buyer Swift Code.
            if (!string.IsNullOrEmpty(mt515.BuyerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt515.BuyerSwiftCode))
                    {
                        BuildResponse($"Buyer swift code ({mt515.BuyerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Buyer swift code cannot be null or missing.");
            }

            //Seller Swift Code.
            if (!string.IsNullOrEmpty(mt515.SellerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt515.SellerSwiftCode))
                    {
                        BuildResponse($"Seller swift code ({mt515.SellerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Seller swift code cannot be null or missing.");
            }

            //Trade Amount.
            if (!string.IsNullOrEmpty(mt515.TradeAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt515.TradeAmount.ToString()))
                {
                    BuildResponse($"Trade amount {mt515.TradeAmount} must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Trade amount cannot be null or missing.");
            }

            //Trade Amount Ccy.
            if (string.IsNullOrEmpty(mt515.TradeAmountCCY) || !currencies.Exists(c => c.CurrencyName == mt515.TradeAmountCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt515.TradeAmountCCY) ? "Trade amount currency cannot be null or missing." : $"Invalid trade amount currency value {mt515.TradeAmountCCY} used.")}");
            }

            //Broker Comms Amount.
            if (!string.IsNullOrEmpty(mt515.BrokerCommsAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt515.BrokerCommsAmount.ToString()))
                {
                    BuildResponse($"Broker comms amount {mt515.BrokerCommsAmount} must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Broker comms amount cannot be null or missing.");
            }

            //Broker Comms Amount Ccy.
            if (string.IsNullOrEmpty(mt515.BrokerCommsAmountCCY) || !currencies.Exists(c => c.CurrencyName == mt515.BrokerCommsAmountCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt515.BrokerCommsAmountCCY) ? "Broker comms amount currency cannot be null or missing." : $"Invalid broker comms amount currency value {mt515.BrokerCommsAmountCCY} used.")}");
            }

            //Accrued Interest Amount.
            if (!string.IsNullOrEmpty(mt515.AccruedInterestAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt515.AccruedInterestAmount.ToString()))
                {
                    BuildResponse($"Accrued interest amount {mt515.AccruedInterestAmount} must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Accrued interest amount cannot be null or missing.");
            }

            //Accrued Interest Amount Ccy.
            if (string.IsNullOrEmpty(mt515.AccruedInterestAmountCCY) || !currencies.Exists(c => c.CurrencyName == mt515.AccruedInterestAmountCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt515.AccruedInterestAmountCCY) ? "Accrued interest amount currency cannot be null or missing." : $"Invalid accrued interest amount currency value {mt515.AccruedInterestAmountCCY} used.")}");
            }

            //Settlement Amount.
            if (!string.IsNullOrEmpty(mt515.SettlementAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt515.SettlementAmount.ToString()))
                {
                    BuildResponse($"Settlement amount {mt515.SettlementAmount} must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Settlement amount cannot be null or missing.");
            }

            //Accrued Interest Amount Ccy.
            if (string.IsNullOrEmpty(mt515.SettlementAmountCCY) || !currencies.Exists(c => c.CurrencyName == mt515.SettlementAmountCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt515.SettlementAmountCCY) ? "Settlement amount currency cannot be null or missing." : $"Invalid settlement amount currency value {mt515.SettlementAmountCCY} used.")}");
            }

            //Email
            if (!string.IsNullOrEmpty(mt515.Email))
            {
                string[] emails = mt515.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT535 Swift Request.
        /// </summary>
        /// <param name="mt535"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT535(MT535 mt535, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt535.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt535.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt535.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt535.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt535.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({mt535.OrderSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt535.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Related Ref.
            if (string.IsNullOrEmpty(mt535.RelatedRef) && mt535.TradeType == "CANC")
            {
                BuildResponse("Related ref cannot be null or missing when Trade Type set as CANC");
            }

            //Trade Type
            if (!string.IsNullOrEmpty(mt535.TradeType))
            {
                if (mt535.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Page Number.
            if (!string.IsNullOrEmpty(mt535.PageNumber.ToString()))
            {
                if (!IsNumericFromTryParse(mt535.PageNumber.ToString()))
                {
                    BuildResponse($"Page number {mt535.PageNumber} must contain numeric values only.");
                }
            }

            //Statement Sequence Type
            if (!string.IsNullOrEmpty(mt535.StatementSequenceType))
            {
                if (mt535.StatementSequenceType.Any(char.IsDigit))
                {
                    BuildResponse("Statement sequence type contains invalid characters.");
                }
            }

            //Statement Date.
            if (string.IsNullOrEmpty(mt535.StatementDate.ToString()))
            {
                BuildResponse("Statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt535.StatementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for statement date ({mt535.StatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Statement Frequency Indicator Code.
            if (!string.IsNullOrEmpty(mt535.StatementFrequencyIndicatorCode))
            {
                if (mt535.StatementFrequencyIndicatorCode.Any(char.IsDigit))
                {
                    BuildResponse("Statement frequency indicator code contains invalid characters.");
                }
            }

            //Updates Indicator Code.
            if (!string.IsNullOrEmpty(mt535.UpdatesIndicatorCode))
            {
                if (mt535.UpdatesIndicatorCode.Any(char.IsDigit))
                {
                    BuildResponse("Updates indicator code contains invalid characters.");
                }
            }

            //Statement Type Code.
            if (!string.IsNullOrEmpty(mt535.StatementTypeCode))
            {
                if (mt535.StatementTypeCode.Any(char.IsDigit))
                {
                    BuildResponse("Statement type code contains invalid characters.");
                }
            }

            //Statement Basis Code.
            if (!string.IsNullOrEmpty(mt535.StatementBasisCode))
            {
                if (mt535.StatementBasisCode.Any(char.IsDigit))
                {
                    BuildResponse("Statement basis code contains invalid characters.");
                }
            }

            //Safekeeping Account Type.
            if (!string.IsNullOrEmpty(mt535.SafekeepingAccountType))
            {
                if (mt535.SafekeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("Safekeeping account type contains invalid characters.");
                }
            }

            //Safekeeping Account.
            if (string.IsNullOrEmpty(mt535.SafekeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //Activity Flag
            if (!string.IsNullOrEmpty(mt535.ActivityFlag))
            {
                if (new string[] { "Y", "N" }.Any(s => mt535.ActivityFlag.Contains(s)))
                {
                }
                else
                {
                    BuildResponse("Activity flag can only be y or n.");
                }
            }

            //Sub Safekeeping Statement Flag.
            if (!string.IsNullOrEmpty(mt535.SubSafekeepingStatementFlag))
            {
                if (new string[] { "Y", "N" }.Any(s => mt535.SubSafekeepingStatementFlag.Contains(s)))
                {
                }
                else
                {
                    BuildResponse("Sub safekeeping activity flag can only be y or n.");
                }
            }

            //Instrument ISIN.
            if (string.IsNullOrEmpty(mt535.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument ISIN Description
            if (string.IsNullOrEmpty(mt535.InstrumentDescription))
            {
                BuildResponse("Instrument description cannot be null or missing.");
            }

            //Deal Price.
            if (!string.IsNullOrEmpty(mt535.DealPrice.ToString()))
            {
                if (!IsNumericFromTryParse(mt535.DealPrice.ToString()))
                {
                    BuildResponse($"Deal price {mt535.DealPrice} must contain numeric values only.");
                }
            }

            //Deal Type.
            if (!string.IsNullOrEmpty(mt535.DealType))
            {
                if (mt535.DealType.Any(char.IsDigit))
                {
                    BuildResponse("Deal type contains invalid characters.");
                }
            }

            //Deal Ccy
            if (string.IsNullOrEmpty(mt535.DealCcy) || !currencies.Exists(c => c.CurrencyName == mt535.DealCcy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt535.DealCcy) ? "Deal currency cannot be null or missing." : $"Invalid deal currency value {mt535.DealCcy} used.")}");
            }

            //Aggregate Balance Type.
            if (!string.IsNullOrEmpty(mt535.AggregateBalanceType))
            {
                if (mt535.AggregateBalanceType.Any(char.IsDigit))
                {
                    BuildResponse("Aggregate balance type contains invalid characters.");
                }
            }

            //Aggregate Balance.
            if (string.IsNullOrEmpty(mt535.AggregateBalance.ToString()))
            {
                BuildResponse("Aggregate balance cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt535.AggregateBalance.ToString()))
                {
                    BuildResponse($"Aggregate balance {mt535.AggregateBalance} must contain numeric values only.");
                }
            }

            //Aggregate Balance Safgekeeping Swift Code
            if (!string.IsNullOrEmpty(mt535.AggregateBalanceSafeKeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt535.AggregateBalanceSafeKeepingSwiftCode))
                    {
                        BuildResponse($"Aggregate balance safekeeping swift code ({mt535.AggregateBalanceSafeKeepingSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Holding Amount.
            if (!string.IsNullOrEmpty(mt535.HoldingAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt535.HoldingAmount.ToString()))
                {
                    BuildResponse($"Holding amount {mt535.HoldingAmount} must contain numeric values only.");
                }

                if (string.IsNullOrEmpty(mt535.HoldingAmountCCY))
                {
                    BuildResponse($"A holding amount currency value cannot be null or missing when a holding amount is present.");
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(mt535.HoldingAmountCCY))
                {
                    BuildResponse($"The holding amount value cannot be null or missing when a holding amount currency is present.");
                }
            }

            //Holding Amount Ccy
            if (!string.IsNullOrEmpty(mt535.HoldingAmountCCY))
            {
                if (!currencies.Exists(c => c.CurrencyName == mt535.DealCcy.ToUpper()))
                {
                    BuildResponse($"Invalid holding amount currency value {mt535.HoldingAmountCCY} used.");
                }
            }
            //else
            //{
            //    if (string.IsNullOrEmpty(mt535.HoldingAmountCCY) && !string.IsNullOrEmpty(mt535.HoldingAmount.ToString()))
            //    {
            //        BuildResponse($"A holding amount currency value cannot be null or missing when a holding amount is present.");
            //    }
            //}

            //Exchange Rate.
            if (!string.IsNullOrEmpty(mt535.ExchangeRate.ToString()))
            {
                if (!IsNumericFromTryParse(mt535.ExchangeRate.ToString()))
                {
                    BuildResponse($"Exchange rate {mt535.ExchangeRate} must contain numeric values only.");
                }
            }

            //Total Holdings Value of Page.
            if (!string.IsNullOrEmpty(mt535.TotalHoldingsValueofPage.ToString()))
            {
                if (!IsNumericFromTryParse(mt535.TotalHoldingsValueofPage.ToString()))
                {
                    BuildResponse($"Total holdings value of page {mt535.TotalHoldingsValueofPage} must contain numeric values only.");
                }
            }

            //Holding Amount Ccy
            if (!string.IsNullOrEmpty(mt535.TotalHoldingsValueofPageCCY))
            {
                if (!currencies.Exists(c => c.CurrencyName == mt535.TotalHoldingsValueofPageCCY.ToUpper()))
                {
                    BuildResponse($"Total holdings value of page ccy {mt535.TotalHoldingsValueofPageCCY} used.");
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt535.Email))
            {
                string[] emails = mt535.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT536 Swift Request.
        /// </summary>
        /// <param name="mt536"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT536(MT536 mt536, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt536.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt536.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt536.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt536.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt536.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({mt536.OrderSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt536.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt536.PreviousOrderRef) && mt536.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when Trade Type set as CANC");
            }

            //Linked Message Type.
            //if (string.IsNullOrEmpty(mt536.LinkedMessageType) && mt536.TradeType == "CANC")
            //{
            //    BuildResponse("Linked message type cannot be null or missing when Trade Type set as CANC");
            //}

            //Trade Type
            if (!string.IsNullOrEmpty(mt536.TradeType))
            {
                if (mt536.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Page Number.
            if (!string.IsNullOrEmpty(mt536.PageNumber.ToString()))
            {
                if (!IsNumericFromTryParse(mt536.PageNumber.ToString()))
                {
                    BuildResponse($"Page number {mt536.PageNumber} must contain numeric values only.");
                }
            }

            //Statement Sequence Type.
            if (!string.IsNullOrEmpty(mt536.StatementSequenceType))
            {
                if (mt536.StatementSequenceType.Any(char.IsDigit))
                {
                    BuildResponse("Statement sequence type contains invalid characters.");
                }
            }

            //Statement Date.
            if (string.IsNullOrEmpty(mt536.StatementDate.ToString()))
            {
                BuildResponse("Statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt536.StatementDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for statement date ({mt536.StatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Statement Frequency Indicator Code.
            if (!string.IsNullOrEmpty(mt536.StatementFrequencyIndicatorCode))
            {
                if (mt536.StatementFrequencyIndicatorCode.Any(char.IsDigit))
                {
                    BuildResponse("Statement frequency indicator code contains invalid characters.");
                }
            }

            //Updates Indicator Code.
            if (!string.IsNullOrEmpty(mt536.UpdatesIndicatorCode))
            {
                if (mt536.UpdatesIndicatorCode.Any(char.IsDigit))
                {
                    BuildResponse("Updates indicator code contains invalid characters.");
                }
            }

            //Statement Basis Code.
            if (!string.IsNullOrEmpty(mt536.StatementBasisCode))
            {
                if (mt536.StatementBasisCode.Any(char.IsDigit))
                {
                    BuildResponse("Statement basis code contains invalid characters.");
                }
            }

            //Safekeeping Account Type.
            if (!string.IsNullOrEmpty(mt536.SafekeepingAccountType))
            {
                if (mt536.SafekeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("Safekeeping account type contains invalid characters.");
                }
            }

            //Safekeeping Account.
            if (string.IsNullOrEmpty(mt536.SafekeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //Activity Flag
            if (!string.IsNullOrEmpty(mt536.ActivityFlag))
            {
                if (new string[] { "Y", "N" }.Any(s => mt536.ActivityFlag.Contains(s)))
                {
                }
                else
                {
                    BuildResponse("Activity flag can only be y or n.");
                }
            }

            //Sub Safekeeping Statement Flag.
            if (!string.IsNullOrEmpty(mt536.SubSafekeepingStatementFlag))
            {
                if (new string[] { "Y", "N" }.Any(s => mt536.SubSafekeepingStatementFlag.Contains(s)))
                {
                }
                else
                {
                    BuildResponse("Sub safekeeping activity flag can only be y or n.");
                }
            }

            //Instrument ISIN.
            if (string.IsNullOrEmpty(mt536.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument ISIN Description
            if (string.IsNullOrEmpty(mt536.InstrumentDescription))
            {
                BuildResponse("Instrument description cannot be null or missing.");
            }

            //Deal Price.
            if (!string.IsNullOrEmpty(mt536.DealPrice.ToString()))
            {
                if (!IsNumericFromTryParse(mt536.DealPrice.ToString()))
                {
                    BuildResponse($"Deal price {mt536.DealPrice} must contain numeric values only.");
                }
            }

            //Deal Type.
            if (!string.IsNullOrEmpty(mt536.DealType))
            {
                if (mt536.DealType.Any(char.IsDigit))
                {
                    BuildResponse("Deal type contains invalid characters.");
                }
            }

            //Deal Ccy
            if (!string.IsNullOrEmpty(mt536.DealCcy))
            {
                if (!currencies.Exists(c => c.CurrencyName == mt536.DealCcy.ToUpper()))
                {
                    BuildResponse($"Invalid deal currency value {mt536.DealCcy} used.");
                }
            }

            //Posting Quantity Type.
            if (!string.IsNullOrEmpty(mt536.PostingQuantityType))
            {
                if (mt536.PostingQuantityType.Any(char.IsDigit))
                {
                    BuildResponse("Posting quantity type contains invalid characters.");
                }
            }

            //Posting Quantity.
            if (!string.IsNullOrEmpty(mt536.PostingQuantity.ToString()))
            {
                if (!IsNumericFromTryParse(mt536.PostingQuantity.ToString()))
                {
                    BuildResponse($"Posting quantity {mt536.PostingQuantity} must contain numeric values only.");
                }
            }

            //Transaction SafeKeeping Account Type.
            if (!string.IsNullOrEmpty(mt536.TransactionSafeKeepingAccountType))
            {
                if (mt536.TransactionSafeKeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("Transaction safeKeeping account type contains invalid characters.");
                }
            }

            //Transaction SafeKeeping Swift Code.
            if (!string.IsNullOrEmpty(mt536.TransactionSafeKeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt536.TransactionSafeKeepingSwiftCode))
                    {
                        BuildResponse($"Transaction safeKeeping swift code ({mt536.TransactionSafeKeepingSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Transaction Indicator.
            if (!string.IsNullOrEmpty(mt536.TransactionIndicator))
            {
                if (mt536.TransactionIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Transaction indicator contains invalid characters.");
                }
            }

            //Delivery Indicator.
            if (!string.IsNullOrEmpty(mt536.DeliveryIndicator))
            {
                if (mt536.DeliveryIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Delivery indicator contains invalid characters.");
                }
            }

            //Payment Indicator.
            if (!string.IsNullOrEmpty(mt536.PaymentIndicator))
            {
                if (mt536.PaymentIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Payment indicator contains invalid characters.");
                }
            }

            //Trade Date.
            if (string.IsNullOrEmpty(mt536.TradeDate.ToString()))
            {
                BuildResponse("Trade date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt536.TradeDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for trade date ({mt536.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt536.SettlementDate.ToString()))
            {
                BuildResponse("Settlement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt536.SettlementDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for settlement date ({mt536.SettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Effective Settlement Date.
            if (string.IsNullOrEmpty(mt536.EffectiveSettlementDate.ToString()))
            {
                BuildResponse("Effective settlement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt536.EffectiveSettlementDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for effective settlement date ({mt536.EffectiveSettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Agent Swift Code.
            if (!string.IsNullOrEmpty(mt536.AgentSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt536.AgentSwiftCode))
                    {
                        BuildResponse($"Agent swift code ({mt536.AgentSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Agent swift code cannot be null or missing.");
            }

            //Buyer Seller Swift Code.
            if (!string.IsNullOrEmpty(mt536.BuyerSellerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt536.BuyerSellerSwiftCode))
                    {
                        BuildResponse($"Buyer seller swift code ({mt536.BuyerSellerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Buyer seller swift code cannot be null or missing.");
            }

            //Settlement Swift Code.
            if (!string.IsNullOrEmpty(mt536.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt536.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({mt536.SettlementSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt536.Email))
            {
                string[] emails = mt536.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT537 Swift Request.
        /// </summary>
        /// <param name="mt537"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT537(MT537 mt537, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt537.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt537.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt537.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt537.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt537.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({mt537.OrderSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt537.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt537.PreviousOrderRef) && mt537.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when Trade Type set as CANC");
            }

            //Linked Message Type.
            //if (string.IsNullOrEmpty(mt537.LinkedMessageType) && mt537.TradeType == "CANC")
            //{
            //    BuildResponse("Linked message type cannot be null or missing when Trade Type set as CANC");
            //}

            //Trade Type
            if (!string.IsNullOrEmpty(mt537.TradeType))
            {
                if (mt537.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Page Number.
            if (!string.IsNullOrEmpty(mt537.PageNumber.ToString()))
            {
                if (!IsNumericFromTryParse(mt537.PageNumber.ToString()))
                {
                    BuildResponse($"Page number {mt537.PageNumber} must contain numeric values only.");
                }
            }

            //Statement Sequence Type.
            if (!string.IsNullOrEmpty(mt537.StatementSequenceType))
            {
                if (mt537.StatementSequenceType.Any(char.IsDigit))
                {
                    BuildResponse("Statement sequence type contains invalid characters.");
                }
            }

            //Statement Date.
            if (string.IsNullOrEmpty(mt537.StatementDate.ToString()))
            {
                BuildResponse("Statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt537.StatementDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for statement date ({mt537.StatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Statement Frequency Indicator Code.
            if (!string.IsNullOrEmpty(mt537.StatementFrequencyIndicatorCode))
            {
                if (mt537.StatementFrequencyIndicatorCode.Any(char.IsDigit))
                {
                    BuildResponse("Statement frequency indicator code contains invalid characters.");
                }
            }

            //Updates Indicator Code.
            if (!string.IsNullOrEmpty(mt537.UpdatesIndicatorCode))
            {
                if (mt537.UpdatesIndicatorCode.Any(char.IsDigit))
                {
                    BuildResponse("Updates indicator code contains invalid characters.");
                }
            }

            //Statement Structure Code.
            if (!string.IsNullOrEmpty(mt537.StatementStructureCode))
            {
                if (mt537.StatementStructureCode.Any(char.IsDigit))
                {
                    BuildResponse("Statement structure code contains invalid characters.");
                }
            }

            //Safekeeping Account Type.
            if (!string.IsNullOrEmpty(mt537.SafekeepingAccountType))
            {
                if (mt537.SafekeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("Safekeeping account type contains invalid characters.");
                }
            }

            //Safekeeping Account.
            if (string.IsNullOrEmpty(mt537.SafekeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //Activity Flag
            if (!string.IsNullOrEmpty(mt537.ActivityFlag))
            {
                if (new string[] { "Y", "N" }.Any(s => mt537.ActivityFlag.Contains(s)))
                {
                }
                else
                {
                    BuildResponse("Activity flag can only be y or n.");
                }
            }

            //Status Code Type.
            if (!string.IsNullOrEmpty(mt537.StatusCodeType))
            {
                if (mt537.StatusCodeType.Any(char.IsDigit))
                {
                    BuildResponse("Status code type contains invalid characters.");
                }
            }

            //Status Code.
            if (!string.IsNullOrEmpty(mt537.StatusCode))
            {
                if (mt537.StatusCode.Any(char.IsDigit))
                {
                    BuildResponse("Status code contains invalid characters.");
                }
            }

            //Reason Code Type.
            if (!string.IsNullOrEmpty(mt537.ReasonCodeType))
            {
                if (mt537.ReasonCodeType.Any(char.IsDigit))
                {
                    BuildResponse("Reason code type contains invalid characters.");
                }
            }

            //Reason Code.
            if (!string.IsNullOrEmpty(mt537.ReasonCode))
            {
                if (mt537.ReasonCode.Any(char.IsDigit))
                {
                    BuildResponse("Reason code contains invalid characters.");
                }
            }

            //Instrument ISIN.
            if (string.IsNullOrEmpty(mt537.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument ISIN Description
            if (string.IsNullOrEmpty(mt537.InstrumentDescription))
            {
                BuildResponse("Instrument description cannot be null or missing.");
            }

            //Posting Quantity Type.
            if (!string.IsNullOrEmpty(mt537.PostingQuantityType))
            {
                if (mt537.PostingQuantityType.Any(char.IsDigit))
                {
                    BuildResponse("Posting quantity type contains invalid characters.");
                }
            }

            //Posting Quantity.
            if (!string.IsNullOrEmpty(mt537.PostingQuantity.ToString()))
            {
                if (!IsNumericFromTryParse(mt537.PostingQuantity.ToString()))
                {
                    BuildResponse($"Posting quantity {mt537.PostingQuantity} must contain numeric values only.");
                }
            }
            else
            {
                BuildResponse("Posting quantity cannot be null or missing.");
            }

            //Transaction SafeKeeping Account Type.
            if (!string.IsNullOrEmpty(mt537.TransactionSafeKeepingAccountType))
            {
                if (mt537.TransactionSafeKeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("Transaction safeKeeping account type contains invalid characters.");
                }
            }

            //Transaction SafeKeeping Account Code.
            if (!string.IsNullOrEmpty(mt537.TransactionSafeKeepingCode))
            {
                if (mt537.TransactionSafeKeepingCode.Any(char.IsDigit))
                {
                    BuildResponse("Transaction safeKeeping account code contains invalid characters.");
                }
            }

            //Transaction SafeKeeping Swift Code.
            if (!string.IsNullOrEmpty(mt537.TransactionSafeKeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt537.TransactionSafeKeepingSwiftCode))
                    {
                        BuildResponse($"Transaction safeKeeping swift code ({mt537.OrderSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Transaction Indicator.
            if (!string.IsNullOrEmpty(mt537.TransactionIndicator))
            {
                if (mt537.TransactionIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Transaction indicator contains invalid characters.");
                }
            }

            //Delivery Indicator.
            if (!string.IsNullOrEmpty(mt537.DeliveryIndicator))
            {
                if (mt537.DeliveryIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Delivery indicator contains invalid characters.");
                }
            }

            //Payment Indicator.
            if (!string.IsNullOrEmpty(mt537.PaymentIndicator))
            {
                if (mt537.PaymentIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Payment indicator contains invalid characters.");
                }
            }

            //Trade Date.
            if (string.IsNullOrEmpty(mt537.TradeDate.ToString()))
            {
                BuildResponse("Trade date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt537.TradeDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for trade date ({mt537.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt537.SettlementDate.ToString()))
            {
                BuildResponse("Settlement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt537.SettlementDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for settlement date ({mt537.SettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Agent Swift Code.
            if (!string.IsNullOrEmpty(mt537.AgentSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt537.AgentSwiftCode))
                    {
                        BuildResponse($"Agent swift code ({mt537.AgentSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Agent Code And ID.
            if (string.IsNullOrEmpty(mt537.AgentCodeAndID))
            {
                BuildResponse($"Agent code and id cannot be null or missing.");
            }

            //Buyer Seller Swift Code.
            if (!string.IsNullOrEmpty(mt537.BuyerSellerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt537.BuyerSellerSwiftCode))
                    {
                        BuildResponse($"Buyer seller swift code ({mt537.BuyerSellerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Settlement Swift Code.
            if (!string.IsNullOrEmpty(mt537.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt537.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({mt537.SettlementSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt537.Email))
            {
                string[] emails = mt537.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT540 Swift Request.
        /// </summary>
        /// <param name="mt540"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT540(MT540 mt540, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt540.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt540.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt540.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt540.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt540.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt540.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Trade Type
            if (!string.IsNullOrEmpty(mt540.TradeType))
            {
                if (mt540.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt540.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Cancel Order Ref.
            if (string.IsNullOrEmpty(mt540.CancelOrderRef) && mt540.TradeType == "CANC")
            {
                BuildResponse("Cancel order ref cannot be null or missing when the trade type is set to CANC.");
            }

            //Trade Date.
            if (string.IsNullOrEmpty(mt540.TradeDate.ToString()))
            {
                BuildResponse("Trade Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt540.TradeDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for Trade Date ({mt540.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt540.SettleDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt540.SettleDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt540.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount.
            if (string.IsNullOrEmpty(mt540.Amount.ToString()))
            {
                BuildResponse("Amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt540.Amount.ToString()))
                {
                    BuildResponse("Amount must contain numeric values only.");
                }
            }

            //Instrument ISIN.
            if (string.IsNullOrEmpty(mt540.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //SafeKeeping Place Code.
            if (!string.IsNullOrEmpty(mt540.SafeKeepingPlaceCode))
            {
                if (mt540.SafeKeepingPlaceCode.Any(char.IsDigit))
                {
                    BuildResponse("SafeKeeping place code contains invalid characters.");
                }
            }

            //Safekeeping Swift Code.
            if (!string.IsNullOrEmpty(mt540.SafeKeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt540.SafeKeepingSwiftCode))
                    {
                        BuildResponse($"Safekeeping swift code ({mt540.SafeKeepingSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Agent Swift Code.
            if (!string.IsNullOrEmpty(mt540.AgentSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt540.AgentSwiftCode))
                    {
                        BuildResponse($"Agent swift code ({mt540.AgentID}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Settlement Swift Code.
            if (!string.IsNullOrEmpty(mt540.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt540.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({mt540.SettlementSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt540.Email))
            {
                string[] emails = mt540.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT541 Swift Request.
        /// </summary>
        /// <param name="mt541"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT541(MT541 mt541, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt541.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt541.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt541.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt541.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt541.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt541.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt541.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt541.PreviousOrderRef) && mt541.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when the trade type is set to CANC.");
            }

            //Linked Message Type.
            if (string.IsNullOrEmpty(mt541.LinkedMessageType) && mt541.TradeType == "CANC")
            {
                BuildResponse("Linked message type cannot be null or missing when the trade type is set to CANC.");
            }

            //Trade Type.
            if (!string.IsNullOrEmpty(mt541.TradeType))
            {
                if (mt541.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Preperation Date.
            if (!string.IsNullOrEmpty(mt541.PreparationDate.ToString()))
            {
                if (DateTime.TryParse(mt541.PreparationDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Preperation Date ({mt541.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Trade Date.
            if (string.IsNullOrEmpty(mt541.TradeDate.ToString()))
            {
                BuildResponse("Trade Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt541.TradeDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Trade Date ({mt541.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt541.SettleDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt541.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt541.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //DealPrice - DealPerc
            if (string.IsNullOrEmpty(mt541.DealPrice.ToString()) && string.IsNullOrEmpty(mt541.DealPerc.ToString()))
            {
                BuildResponse("Either a Deal Price or Deal Perc must be entered");
            }

            if (!string.IsNullOrEmpty(mt541.DealPrice.ToString())
                && string.IsNullOrEmpty(mt541.DealPerc.ToString()))
            {
                bool parsed = double.TryParse(mt541.DealPrice.ToString(), out double val);
                if (!parsed)
                {
                    BuildResponse("Deal Price must contain numeric values only.");
                }
                else if (!string.IsNullOrEmpty(mt541.DealPerc.ToString())
                    && string.IsNullOrEmpty(mt541.DealPrice.ToString()))
                {
                    bool parsedPerc = double.TryParse(mt541.DealPerc.ToString(), out double valPerc);
                    if (!parsedPerc)
                    {
                        BuildResponse("Deal Perc must contain numeric values only.");
                    }
                }
            }

            //Deal Currency.
            if (!string.IsNullOrEmpty(mt541.DealCcy))
            {
                if (!currencies.Exists(c => c.CurrencyName == mt541.DealCcy.ToUpper()))
                {
                    BuildResponse($"Invalid deal currency value {mt541.DealCcy} used.");
                }
            }

            //Quantity Type.
            if (!string.IsNullOrEmpty(mt541.QuantityType))
            {
                if (mt541.QuantityType.Any(char.IsDigit))
                {
                    BuildResponse("Quantity type contains invalid characters.");
                }
            }

            //Quantity.
            if (string.IsNullOrEmpty(mt541.Quantity.ToString()))
            {
                BuildResponse("Quantity cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt541.Quantity.ToString()))
                {
                    BuildResponse("Quantity must contain numeric values only.");
                }
            }

            //SafeKeeping Account Type.
            if (!string.IsNullOrEmpty(mt541.SafeKeepingAccountType))
            {
                if (mt541.SafeKeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("SafeKeeping Account type contains invalid characters.");
                }
            }

            //Safe Keeping Account.
            if (string.IsNullOrEmpty(mt541.SafeKeepingAccount))
            {
                BuildResponse("Safe Keeping Account cannot be null or missing.");
            }

            //SafeKeeping Place Code.
            if (!string.IsNullOrEmpty(mt541.SafeKeepingPlaceCode))
            {
                if (mt541.SafeKeepingPlaceCode.Any(char.IsDigit))
                {
                    BuildResponse("SafeKeeping place code contains invalid characters.");
                }
            }

            //Safekeeping Swift Code.
            if (!string.IsNullOrEmpty(mt541.SafeKeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt541.SafeKeepingSwiftCode))
                    {
                        BuildResponse($"Safekeeping swift code ({mt541.SafeKeepingSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Seller/Buyer Swift Code/Name.
            if ((string.IsNullOrEmpty(mt541.BuyerSwiftCode) && string.IsNullOrEmpty(mt541.BuyerName)) &&
                (string.IsNullOrEmpty(mt541.SellerSwiftCode) && string.IsNullOrEmpty(mt541.SellerName)))
            {
                BuildResponse("Either a Buyer swift code, Buyer Name or Seller Swift Code, Seller Name are required.");
            }
            else if (!string.IsNullOrEmpty(mt541.BuyerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt541.BuyerSwiftCode))
                    {
                        BuildResponse($"Buyer swift code ({mt541.BuyerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else if (!string.IsNullOrEmpty(mt541.SellerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt541.SellerSwiftCode))
                    {
                        BuildResponse($"Seller swift code ({mt541.SellerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Settlement Swift Code.
            if (!string.IsNullOrEmpty(mt541.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt541.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({mt541.SettlementSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Settlement swift code cannot be null or missing.");
            }

            //Settlement Transaction Type.
            if (!string.IsNullOrEmpty(mt541.SettlementTransactionType))
            {
                if (mt541.SettlementTransactionType.Any(char.IsDigit))
                {
                    BuildResponse("Settlement transaction type contains invalid characters.");
                }
            }

            //Settlement Transaction Indicator.
            if (!string.IsNullOrEmpty(mt541.SettlementTransactionIndicator))
            {
                if (mt541.SettlementTransactionIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Settlement transaction indicator contains invalid characters.");
                }
            }

            //Agent Code and ID.
            if (string.IsNullOrEmpty(mt541.AgentCodeAndID))
            {
                BuildResponse("Agent code and id cannot be null or missing.");
            }

            //AgentID.
            if (string.IsNullOrEmpty(mt541.AgentID))
            {
                BuildResponse("Agentid cannot be null or missing.");
            }

            //Settlement Amount Type.
            if (!string.IsNullOrEmpty(mt541.SettlementAmountType))
            {
                if (mt541.SettlementAmountType.Any(char.IsDigit))
                {
                    BuildResponse("Settlement amount type contains invalid characters.");
                }
            }

            //Settlement Amount.
            if (!string.IsNullOrEmpty(mt541.SettlementAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt541.SettlementAmount.ToString()))
                {
                    BuildResponse("Settlement amount must contain numeric values only.");
                }
            }

            //Settlement Currency.
            if (string.IsNullOrEmpty(mt541.SettlementCcy) || !currencies.Exists(c => c.CurrencyName == mt541.SettlementCcy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt541.SettlementCcy) ? "Settlement currency cannot be null or missing." : $"Invalid settlement currency value {mt541.SettlementCcy} used.")}");
            }

            //Email.
            if (!string.IsNullOrEmpty(mt541.Email))
            {
                string[] emails = mt541.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }
            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT542 Swift Request.
        /// </summary>
        /// <param name="mt542"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT542(MT542 mt542, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt542.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt542.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt542.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt542.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt542.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt542.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Trade Type.
            if (!string.IsNullOrEmpty(mt542.TradeType))
            {
                if (mt542.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt542.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Cancel Order Ref.
            if (string.IsNullOrEmpty(mt542.CancelOrderRef) && mt542.TradeType == "CANC")
            {
                BuildResponse("Cancel Order Ref cannot be null or missing when Trade Type set as CANC");
            }

            //Trade Date.
            if (string.IsNullOrEmpty(mt542.TradeDate.ToString()))
            {
                BuildResponse("Trade Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt542.TradeDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for Trade Date ({mt542.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt542.SettleDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt542.SettleDate.ToString(), out _) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt542.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount
            if (string.IsNullOrEmpty(mt542.Amount.ToString()))
            {
                BuildResponse("Amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt542.Amount.ToString()))
                {
                    BuildResponse("Amount must contain numeric values only.");
                }
            }

            //Instrument ISIN
            if (string.IsNullOrEmpty(mt542.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Safekeeping Place Code.
            if (!string.IsNullOrEmpty(mt542.SafekeepingPlaceCode))
            {
                if (mt542.SafekeepingPlaceCode.Any(char.IsDigit))
                {
                    BuildResponse("Safekeeping place contains invalid characters.");
                }
            }

            //Safekeeping Swift Code.
            if (!string.IsNullOrEmpty(mt542.SafekeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt542.SafekeepingSwiftCode))
                    {
                        BuildResponse($"Safekeeping swift code ({mt542.SafekeepingSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Agent Swift Code.
            if (!string.IsNullOrEmpty(mt542.AgentSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt542.AgentSwiftCode))
                    {
                        BuildResponse($"Agent swift code ({mt542.AgentSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Settlement Swift Code.
            if (!string.IsNullOrEmpty(mt542.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt542.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({mt542.SettlementSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT543 Swift Request.
        /// </summary>
        /// <param name="mt543"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT543(MT543 mt543, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt543.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt543.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt543.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt543.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt543.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt543.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt543.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Previous Order Ref.
            if (string.IsNullOrEmpty(mt543.PreviousOrderRef) && mt543.TradeType == "CANC")
            {
                BuildResponse("Previous order ref cannot be null or missing when the trade type is set to CANC.");
            }

            //Trade Type.
            if (!string.IsNullOrEmpty(mt543.TradeType))
            {
                if (mt543.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Preperation Date.
            if (!string.IsNullOrEmpty(mt543.PreparationDate.ToString()))
            {
                if (DateTime.TryParse(mt543.PreparationDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Preperation Date ({mt543.SettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }
            else
            {
                BuildResponse("Preperation Date cannot be null or missing.");
            }

            //Trade Date.
            if (!string.IsNullOrEmpty(mt543.TradeDate.ToString()))
            {
                if (DateTime.TryParse(mt543.TradeDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Trade Date ({mt543.SettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }
            else
            {
                BuildResponse("Trade Date cannot be null or missing.");
            }

            //Settlement Date.
            if (string.IsNullOrEmpty(mt543.SettlementDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt543.SettlementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({mt543.SettlementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //DealPrice - DealPerc
            if (string.IsNullOrEmpty(mt543.DealPrice.ToString()) && string.IsNullOrEmpty(mt543.DealPerc.ToString()))
            {
                BuildResponse("Either a Deal Price or Deal Perc must be entered");
            }

            if (!string.IsNullOrEmpty(mt543.DealPrice.ToString())
                && string.IsNullOrEmpty(mt543.DealPerc.ToString()))
            {
                bool parsed = double.TryParse(mt543.DealPrice.ToString(), out double val);
                if (!parsed)
                {
                    BuildResponse("Deal Price must contain numeric values only.");
                }
            }
            else if (!string.IsNullOrEmpty(mt543.DealPerc.ToString())
                    && string.IsNullOrEmpty(mt543.DealPrice.ToString()))
            {
                bool parsedPerc = double.TryParse(mt543.DealPerc.ToString(), out double valPerc);
                if (!parsedPerc)
                {
                    BuildResponse("Deal Perc must contain numeric values only.");
                }
            }

            //Deal Currency
            if (!string.IsNullOrEmpty(mt543.DealCcy))
            {
                if (!currencies.Exists(c => c.CurrencyName == mt543.DealCcy.ToUpper()))
                {
                    BuildResponse($"Invalid deal currency value {mt543.DealCcy} used.");
                }
            }

            //Quantity Type.
            if (!string.IsNullOrEmpty(mt543.QuantityType))
            {
                if (mt543.QuantityType.Any(char.IsDigit))
                {
                    BuildResponse("Quantity type contains invalid characters.");
                }
            }

            //Quantity
            if (string.IsNullOrEmpty(mt543.Quantity.ToString()))
            {
                BuildResponse("Quantity cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt543.Quantity.ToString()))
                {
                    BuildResponse("Quantity must contain numeric values only.");
                }
            }

            //Safekeeping Swift Code.
            if (!string.IsNullOrEmpty(mt543.SafeKeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt543.SafeKeepingSwiftCode))
                    {
                        BuildResponse($"Safekeeping swift code ({mt543.SafeKeepingSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Buyer Swift Code.
            if (!string.IsNullOrEmpty(mt543.BuyerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt543.BuyerSwiftCode))
                    {
                        BuildResponse($"Buyer swift code ({mt543.BuyerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Seller Swift Code.
            if (!string.IsNullOrEmpty(mt543.BuyerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt543.SellerSwiftCode))
                    {
                        BuildResponse($"Seller swift code ({mt543.SellerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Settlement Swift Code.
            if (!string.IsNullOrEmpty(mt543.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt543.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({mt543.SettlementSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Settlement swift code cannot be null or missing.");
            }

            //Settlement Transaction Indicator.
            if (!string.IsNullOrEmpty(mt543.TransactionIndicator))
            {
                if (mt543.TransactionIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Settlement transaction indicator contains invalid characters.");
                }
            }

            //Settlement Amount Type.
            if (!string.IsNullOrEmpty(mt543.SettlementAmountType))
            {
                if (mt543.SettlementAmountType.Any(char.IsDigit))
                {
                    BuildResponse("Settlement amount type contains invalid characters.");
                }
            }

            //Settlement Amount
            if (!string.IsNullOrEmpty(mt543.SettlementAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt543.SettlementAmount.ToString()))
                {
                    BuildResponse("Settlement amount must contain numeric values only.");
                }
            }

            //Settlement Currency
            if (!string.IsNullOrEmpty(mt543.SettlementCcy))
            {
                if (!currencies.Exists(c => c.CurrencyName == mt543.SettlementCcy.ToUpper()))
                {
                    BuildResponse($"Invalid settlement currency value {mt543.SettlementCcy} used.");
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt543.Email))
            {
                string[] emails = mt543.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT545 Swift Request.
        /// </summary>
        /// <param name="mt545"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT545(MT545 mt545, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code
            if (!string.IsNullOrEmpty(mt545.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt545.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt545.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Order Swift Code
            if (!string.IsNullOrEmpty(mt545.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt545.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({(mt545.OrderSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref
            if (string.IsNullOrEmpty(mt545.OrderRef))
            {
                BuildResponse("Order ref cannot be null or missing.");
            }

            //Related Ref
            if (string.IsNullOrEmpty(mt545.RelatedRef))
            {
                BuildResponse("Related ref cannot be null or missing.");
            }

            //Trade Date
            if (string.IsNullOrEmpty(mt545.TradeDate.ToString()))
            {
                BuildResponse("Trade date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt545.TradeDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Trade Date ({mt545.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settle Date
            if (string.IsNullOrEmpty(mt545.SettleDate.ToString()))
            {
                BuildResponse("Settle date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt545.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settle Date ({mt545.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Effective Settlement Date
            if (string.IsNullOrEmpty(mt545.EffectiveSettleDate.ToString()))
            {
                BuildResponse("Effective settle date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt545.EffectiveSettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for effective settle date ({mt545.EffectiveSettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //DealPrice - DealPerc
            if (string.IsNullOrEmpty(mt545.DealPrice.ToString()) && string.IsNullOrEmpty(mt545.DealPerc.ToString()))
            {
                BuildResponse("Either a Deal Price or Deal Perc must be entered");
            }

            if (!string.IsNullOrEmpty(mt545.DealPrice.ToString())
                && string.IsNullOrEmpty(mt545.DealPerc.ToString()))
            {
                bool parsed = double.TryParse(mt545.DealPrice.ToString(), out double val);
                if (!parsed)
                {
                    BuildResponse("Deal Price must contain numeric values only.");
                }
            }
            else if (!string.IsNullOrEmpty(mt545.DealPerc.ToString())
                    && string.IsNullOrEmpty(mt545.DealPrice.ToString()))
            {
                bool parsedPerc = double.TryParse(mt545.DealPerc.ToString(), out double valPerc);
                if (!parsedPerc)
                {
                    BuildResponse("Deal Perc must contain numeric values only.");
                }
            }

            //Deal Ccy
            if (string.IsNullOrEmpty(mt545.DealCcy) || !currencies.Exists(c => c.CurrencyName == mt545.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt545.DealCcy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt545.DealCcy} used.")}");
            }

            //Instrument ISIN
            if (string.IsNullOrEmpty(mt545.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument Description
            if (string.IsNullOrEmpty(mt545.InstrumentDescription))
            {
                BuildResponse("Instrument description cannot be null or missing.");
            }

            //Quantity Type.
            if (!string.IsNullOrEmpty(mt545.QuantityType))
            {
                if (mt545.QuantityType.Any(char.IsDigit))
                {
                    BuildResponse("Quantity type contains invalid characters.");
                }
            }

            //Quantity.
            if (string.IsNullOrEmpty(mt545.Quantity.ToString()))
            {
                BuildResponse("Quantity cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt545.Quantity.ToString()))
                {
                    BuildResponse("Quantity must contain numeric values only.");
                }
            }

            //Settlement Amount.
            if (!string.IsNullOrEmpty(mt545.SettlementAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt545.SettlementAmount.ToString()))
                {
                    BuildResponse("Settlement amount must contain numeric values only.");
                }
            }

            //Ccy
            if (string.IsNullOrEmpty(mt545.Ccy) || !currencies.Exists(c => c.CurrencyName == mt545.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt545.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt545.Ccy} used.")}");
            }

            //SafeKeeping Account Type.
            if (!string.IsNullOrEmpty(mt545.SafekeepingAccountType))
            {
                if (mt545.SafekeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("SafeKeeping Account type contains invalid characters.");
                }
            }

            //Safekeeping Account.
            if (string.IsNullOrEmpty(mt545.SafekeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //SafeKeeping Place Code.
            if (!string.IsNullOrEmpty(mt545.SafeKeepingPlaceCode))
            {
                if (mt545.SafeKeepingPlaceCode.Any(char.IsDigit))
                {
                    BuildResponse("SafeKeeping place code contains invalid characters.");
                }
            }

            //Safekeeping Swift Code
            if (!string.IsNullOrEmpty(mt545.SafeKeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt545.SafeKeepingSwiftCode))
                    {
                        BuildResponse($"Safekeeping swift code ({(mt545.SafeKeepingSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Transaction Indicator.
            if (!string.IsNullOrEmpty(mt545.TransactionIndicator))
            {
                if (mt545.TransactionIndicator.Any(char.IsDigit))
                {
                    BuildResponse("Transaction indicator contains invalid characters.");
                }
            }

            //Agent Swift Code
            if (!string.IsNullOrEmpty(mt545.AgentSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt545.AgentSwiftCode))
                    {
                        BuildResponse($"Agent swift code ({(mt545.AgentSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Agent Code and ID
            if (string.IsNullOrEmpty(mt545.AgentCodeAndID))
            {
                BuildResponse("Agent code and id cannot be null or missing.");
            }

            //AgentID
            if (string.IsNullOrEmpty(mt545.AgentID))
            {
                BuildResponse("Agentid cannot be null or missing.");
            }

            //Counter Party Swift Code
            if (!string.IsNullOrEmpty(mt545.CptySwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt545.CptySwiftCode))
                    {
                        BuildResponse($"Counter party swift code ({(mt545.CptySwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Counter party swift code cannot be null or missing.");
            }

            //Settlement Swift Code
            if (!string.IsNullOrEmpty(mt545.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt545.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({(mt545.SettlementSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Settlement swift code cannot be null or missing.");
            }

            //Email
            if (!string.IsNullOrEmpty(mt545.Email))
            {
                string[] emails = mt545.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT547 Swift Request.
        /// </summary>
        /// <param name="mt547"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT547(MT547 mt547, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code
            if (!string.IsNullOrEmpty(mt547.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt547.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt547.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Order Swift Code
            if (!string.IsNullOrEmpty(mt547.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt547.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({(mt547.OrderSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref
            if (string.IsNullOrEmpty(mt547.OrderRef))
            {
                BuildResponse("Order ref cannot be null or missing.");
            }

            //Related Ref
            if (string.IsNullOrEmpty(mt547.RelatedRef))
            {
                BuildResponse("Related ref cannot be null or missing.");
            }

            //Trade Date
            if (string.IsNullOrEmpty(mt547.TradeDate.ToString()))
            {
                BuildResponse("Trade date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt547.TradeDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Trade Date ({mt547.TradeDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Settle Date
            if (string.IsNullOrEmpty(mt547.SettleDate.ToString()))
            {
                BuildResponse("Settlement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt547.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for settlement date ({mt547.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Effective Settlement Date
            if (string.IsNullOrEmpty(mt547.EffectiveSettleDate.ToString()))
            {
                BuildResponse("Effective settle date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt547.EffectiveSettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for effective settle date ({mt547.EffectiveSettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //DealPrice - DealPerc
            if (string.IsNullOrEmpty(mt547.DealPrice.ToString()) && string.IsNullOrEmpty(mt547.DealPerc.ToString()))
            {
                BuildResponse("Either a Deal Price or Deal Perc must be entered");
            }

            if (!string.IsNullOrEmpty(mt547.DealPrice.ToString())
                && string.IsNullOrEmpty(mt547.DealPerc.ToString()))
            {
                bool parsed = double.TryParse(mt547.DealPrice.ToString(), out double val);
                if (!parsed)
                {
                    BuildResponse("Deal Price must contain numeric values only.");
                }
            }
            else if (!string.IsNullOrEmpty(mt547.DealPerc.ToString())
                    && string.IsNullOrEmpty(mt547.DealPrice.ToString()))
            {
                bool parsedPerc = double.TryParse(mt547.DealPerc.ToString(), out double valPerc);
                if (!parsedPerc)
                {
                    BuildResponse("Deal Perc must contain numeric values only.");
                }
            }

            //Deal Ccy
            if (string.IsNullOrEmpty(mt547.DealCcy) || !currencies.Exists(c => c.CurrencyName == mt547.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt547.DealCcy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt547.DealCcy} used.")}");
            }

            //Instrument ISIN
            if (string.IsNullOrEmpty(mt547.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument Description
            if (string.IsNullOrEmpty(mt547.InstrumentDescription))
            {
                BuildResponse("Instrument description cannot be null or missing.");
            }

            //Quantity Type
            if (string.IsNullOrEmpty(mt547.QuantityType))
            {
                BuildResponse("Quantity type cannot be null or missing.");
            }

            //Quantity.
            if (string.IsNullOrEmpty(mt547.Quantity.ToString()))
            {
                BuildResponse("Quantity cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt547.Quantity.ToString()))
                {
                    BuildResponse("Quantity must contain numeric values only.");
                }
            }

            //Settlement Amount.
            if (!string.IsNullOrEmpty(mt547.SettlementAmount.ToString()))
            {
                if (!IsNumericFromTryParse(mt547.SettlementAmount.ToString()))
                {
                    BuildResponse("Settlement amount must contain numeric values only.");
                }
            }

            //Ccy
            if (string.IsNullOrEmpty(mt547.Ccy) || !currencies.Exists(c => c.CurrencyName == mt547.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt547.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt547.Ccy} used.")}");
            }

            //Safekeeping Account.
            if (string.IsNullOrEmpty(mt547.SafekeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //Safekeeping Swift Code
            if (!string.IsNullOrEmpty(mt547.SafeKeepingSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt547.SafeKeepingSwiftCode))
                    {
                        BuildResponse($"Safekeeping swift code ({(mt547.SafeKeepingSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Agent Swift Code
            if (!string.IsNullOrEmpty(mt547.AgentSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt547.AgentSwiftCode))
                    {
                        BuildResponse($"Agent swift code ({(mt547.AgentSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Agent Code and ID
            if (string.IsNullOrEmpty(mt547.AgentCodeAndID))
            {
                BuildResponse("Agent code and id cannot be null or missing.");
            }

            //AgentID
            if (string.IsNullOrEmpty(mt547.AgentID))
            {
                BuildResponse("Agentid cannot be null or missing.");
            }

            //Counter Party Swift Code
            if (!string.IsNullOrEmpty(mt547.CptySwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt547.CptySwiftCode))
                    {
                        BuildResponse($"Counter party swift code ({(mt547.CptySwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Counter party swift code cannot be null or missing.");
            }

            //Settlement Swift Code
            if (!string.IsNullOrEmpty(mt547.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt547.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({(mt547.SettlementSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt547.Email))
            {
                string[] emails = mt547.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT548 Swift request.
        /// </summary>
        /// <param name="mt548"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT548(MT548 mt548, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code
            if (!string.IsNullOrEmpty(mt548.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt548.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt548.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Order Swift Code
            if (!string.IsNullOrEmpty(mt548.OrderSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt548.OrderSwiftCode))
                    {
                        BuildResponse($"Order swift code ({(mt548.OrderSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref
            if (string.IsNullOrEmpty(mt548.OrderRef))
            {
                BuildResponse("Order ref cannot be null or missing.");
            }

            //Linked Message Type
            if (string.IsNullOrEmpty(mt548.LinkedMessageType))
            {
                BuildResponse("Linked message type cannot be null or missing.");
            }

            //Settlement Date
            if (string.IsNullOrEmpty(mt548.SettleDate.ToString()))
            {
                BuildResponse("Settlement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt548.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for settlement date ({mt548.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Instrument ISIN
            if (string.IsNullOrEmpty(mt548.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument Description
            if (string.IsNullOrEmpty(mt548.InstrumentDescription))
            {
                BuildResponse("Instrument description cannot be null or missing.");
            }

            //Quantity Type.
            if (string.IsNullOrEmpty(mt548.QuantityType))
            {
                BuildResponse("Quantity type cannot be null or missing.");
            }

            //Quantity.
            if (string.IsNullOrEmpty(mt548.Quantity.ToString()))
            {
                BuildResponse("Quantity cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt548.Quantity.ToString()))
                {
                    BuildResponse("Quantity must contain numeric values only.");
                }
            }

            //Settlement Amount.
            if (string.IsNullOrEmpty(mt548.SettlementAmount.ToString()))
            {
                BuildResponse("Settlement amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt548.SettlementAmount.ToString()))
                {
                    BuildResponse("Settlement amount must contain numeric values only.");
                }
            }

            //Ccy
            if (string.IsNullOrEmpty(mt548.Ccy) || !currencies.Exists(c => c.CurrencyName == mt548.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt548.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt548.Ccy} used.")}");
            }

            //Safekeeping Account Type.
            if (string.IsNullOrEmpty(mt548.SafekeepingAccountType))
            {
                BuildResponse("Safekeeping account type cannot be null or missing.");
            }

            //Safekeeping Account.
            if (string.IsNullOrEmpty(mt548.SafekeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //Agent Swift Code
            if (!string.IsNullOrEmpty(mt548.AgentSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt548.AgentSwiftCode))
                    {
                        BuildResponse($"Agent code ({(mt548.AgentSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Settlement Swift Code
            if (!string.IsNullOrEmpty(mt548.SettlementSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt548.SettlementSwiftCode))
                    {
                        BuildResponse($"Settlement swift code ({(mt548.SettlementSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt548.Email))
            {
                string[] emails = mt548.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT565 Swift request.
        /// </summary>
        /// <param name="mt565"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT565(MT565 mt565, bool validateSwift)
        {
            //Client Swift Code
            if (!string.IsNullOrEmpty(mt565.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt565.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt565.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code
            if (!string.IsNullOrEmpty(mt565.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt565.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({(mt565.BrokerSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref
            if (string.IsNullOrEmpty(mt565.OrderRef))
            {
                BuildResponse("Order ref cannot be null or missing.");
            }

            //Corporate Action Ref
            if (string.IsNullOrEmpty(mt565.CorporateActionRef))
            {
                BuildResponse("Corporate action ref cannot be null or missing.");
            }

            //Trade Type.
            if (string.IsNullOrEmpty(mt565.TradeType))
            {
                BuildResponse("Trade type cannot be null or missing.");
            }
            else
            {
                if (mt565.TradeType.Any(char.IsDigit))
                {
                    BuildResponse("Trade type contains invalid characters.");
                }
            }

            //Corporate Action Event Indicator
            if (string.IsNullOrEmpty(mt565.CorporateActionEventIndicator))
            {
                BuildResponse("Corporate action event indicator cannot be null or missing.");
            }

            //Link fields.
            if (mt565.TradeType == "CANC")
            {
                if (string.IsNullOrEmpty(mt565.RelatedOrderRef))
                {
                    BuildResponse("A related order ref is required when the trade type is CANC.");
                }
                else
                {
                    if (string.IsNullOrEmpty(mt565.LinkIndicatorType))
                    {
                        BuildResponse("Link indicator type cannot be null or missing when a related ref is entered.");
                    }

                    if (string.IsNullOrEmpty(mt565.LinkMessageType))
                    {
                        BuildResponse("Link message type cannot be null or missing when a related ref is entered.");
                    }
                }
            }

            //Instrument ISIN.
            if (string.IsNullOrEmpty(mt565.InstrumentISIN))
            {
                BuildResponse("Instrument ISIN cannot be null or missing.");
            }

            //Instrument Quanity Type.
            if (string.IsNullOrEmpty(mt565.InstrumentQtyType))
            {
                BuildResponse("Instrument quanity type cannot be null or missing.");
            }
            else
            {
                if (mt565.InstrumentQtyType.Any(char.IsDigit))
                {
                    BuildResponse("Instrument quanity type contains invalid characters.");
                }
            }

            //Instrument Quanity.
            if (!string.IsNullOrEmpty(mt565.InstrumentQty.ToString()))
            {
                if (!IsNumericFromTryParse(mt565.InstrumentQty.ToString()))
                {
                    BuildResponse("Instrument quantity must contain numeric values only.");
                }
            }

            //Safekeeping Account Type.
            if (string.IsNullOrEmpty(mt565.SafeKeepingAccountType))
            {
                BuildResponse("Safekeeping account type cannot be null or missing.");
            }
            else
            {
                if (mt565.SafeKeepingAccountType.Any(char.IsDigit))
                {
                    BuildResponse("Safekeeping account type contains invalid characters.");
                }
            }

            //Safekeeping Account.
            if (string.IsNullOrEmpty(mt565.SafekeepingAccount))
            {
                BuildResponse("Safekeeping account cannot be null or missing.");
            }

            //Beneficiary Swift Code
            if (!string.IsNullOrEmpty(mt565.BeneficiarySwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt565.BeneficiarySwiftCode))
                    {
                        BuildResponse($"Beneficiary swift code ({(mt565.BeneficiarySwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Beneficiary swift code cannot be null or missing.");
            }

            //Corporate Action Number.
            if (mt565.PreviousActionNotification)
            {
                if (string.IsNullOrEmpty(mt565.CorporateActionNumber))
                {
                    BuildResponse("Corporate action number cannot be null or missing when previous action notification is true");
                }
                else
                {
                    if (!IsNumericFromTryParse(mt565.CorporateActionNumber.ToString()))
                    {
                        BuildResponse("Corporate action number must contain numeric values only.");
                    }
                    else
                    {
                        if (Convert.ToInt32(mt565.CorporateActionNumber) > 999 || Convert.ToInt32(mt565.CorporateActionNumber) < 1)
                        {
                            BuildResponse("Corporate action number must be between 001 and 999 only.");
                        }
                    }
                }
            }

            //Corporate Action Indicator.
            if (string.IsNullOrEmpty(mt565.CorporateActionIndicator))
            {
                BuildResponse("Corporate action indicator cannot be null or missing.");
            }

            //Email
            if (!string.IsNullOrEmpty(mt565.Email))
            {
                string[] emails = mt565.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT900 Swift request.
        /// </summary>
        /// <param name="mT900"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT900(MT900 mT900, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code
            if (!string.IsNullOrEmpty(mT900.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mT900.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mT900.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code
            if (!string.IsNullOrEmpty(mT900.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mT900.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({(mT900.BrokerSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mT900.OrderRef))
            {
                BuildResponse("Order ref cannot be null or missing.");
            }

            //Related Order Ref.
            if (string.IsNullOrEmpty(mT900.RelatedOrderRef))
            {
                BuildResponse("Related order ref cannot be null or missing.");
            }

            //Settle Date.
            if (string.IsNullOrEmpty(mT900.SettleDate.ToString()))
            {
                BuildResponse("Settle Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mT900.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for settle date ({mT900.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount
            if (string.IsNullOrEmpty(mT900.Amount.ToString()))
            {
                BuildResponse("Amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mT900.Amount.ToString()))
                {
                    BuildResponse("Amount must contain numeric values only.");
                }
            }

            //CCY
            if (string.IsNullOrEmpty(mT900.Ccy) || !currencies.Exists(c => c.CurrencyName == mT900.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mT900.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mT900.Ccy} used.")}");
            }

            //Email
            if (!string.IsNullOrEmpty(mT900.Email))
            {
                string[] emails = mT900.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT910 Swift request.
        /// </summary>
        /// <param name="mT910"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT910(MT910 mT910, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code
            if (!string.IsNullOrEmpty(mT910.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mT910.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mT910.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Broker Swift Code
            if (!string.IsNullOrEmpty(mT910.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mT910.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({(mT910.BrokerSwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mT910.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Settle Date.
            if (string.IsNullOrEmpty(mT910.SettleDate.ToString()))
            {
                BuildResponse("Settle Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mT910.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for settle date ({mT910.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount
            if (string.IsNullOrEmpty(mT910.Amount.ToString()))
            {
                BuildResponse("Amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mT910.Amount.ToString()))
                {
                    BuildResponse("Amount must contain numeric values only.");
                }
            }

            //CCY
            if (string.IsNullOrEmpty(mT910.Ccy) || !currencies.Exists(c => c.CurrencyName == mT910.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mT910.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {mT910.Ccy} used.")}");
            }

            //Broker Swift Code
            if (!string.IsNullOrEmpty(mT910.IntermediarySwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mT910.IntermediarySwiftCode))
                    {
                        BuildResponse($"Intermediary swift code ({(mT910.IntermediarySwiftCode)}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mT910.Email))
            {
                string[] emails = mT910.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT920 Swift Request.
        /// </summary>
        /// <param name="mt920"></param>
        /// <param name="currencies"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string ValidateMT920(MT920 mt920, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt920.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt920.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt920.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Client swift code cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(mt920.OrderSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt920.OrderSwift))
                    {
                        BuildResponse($"Order swift code ({mt920.OrderSwift}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Order swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt920.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Ccy.
            if (string.IsNullOrEmpty(mt920.OrderCcy) || !currencies.Exists(c => c.CurrencyName == mt920.OrderCcy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt920.OrderCcy) ? "Currency cannot be null or missing." : $"Invalid currency value {mt920.OrderCcy} used.")}");
            }

            //Order Type
            if (string.IsNullOrEmpty(mt920.OrderType))
            {
                BuildResponse("Order type cannot be null or missing.");
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        ///  Validate the MT94n Swift Request.
        /// </summary>
        /// <param name="mt94"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT94(MT94 mt94, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt94.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt94.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt94.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt94.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt94.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt94.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt94.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Account Number.
            if (string.IsNullOrEmpty(mt94.AccountNumber))
            {
                BuildResponse("Account number cannot be null or missing.");
            }

            //Statement Number.
            if (string.IsNullOrEmpty(mt94.StatementNumber.ToString()))
            {
                BuildResponse("Statement number cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt94.StatementNumber.ToString()))
                {
                    BuildResponse("Statement number must contain numeric values only.");
                }
            }

            //Statement Sequence Number.
            if (string.IsNullOrEmpty(mt94.StatementSequenceNumber.ToString()))
            {
                BuildResponse("Statement sequence number cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt94.StatementSequenceNumber.ToString()))
                {
                    BuildResponse("Statement sequence number must contain numeric values only.");
                }
            }

            //Opening Balance Statement Date.
            if (string.IsNullOrEmpty(mt94.OpeningBalanceStatementDate.ToString()))
            {
                BuildResponse("Opening balance statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt94.OpeningBalanceStatementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for opening balance statement date ({mt94.OpeningBalanceStatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Opening Balance Ccy.
            if (string.IsNullOrEmpty(mt94.OpeningBalanceCCY) || !currencies.Exists(c => c.CurrencyName == mt94.OpeningBalanceCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt94.OpeningBalanceCCY) ? "Opening balance currency cannot be null or missing." : $"Invalid opening balance currency value {mt94.OpeningBalanceCCY} used.")}");
            }

            //Opening Balance Amount.
            if (string.IsNullOrEmpty(mt94.OpeningBalanceAmount.ToString()))
            {
                BuildResponse("Opening balance amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt94.OpeningBalanceAmount.ToString()))
                {
                    BuildResponse("Opening balance amount must contain numeric values only.");
                }
            }

            //Closing Balance Statement Date.
            if (string.IsNullOrEmpty(mt94.ClosingBalanceStatementDate.ToString()))
            {
                BuildResponse("Closing balance statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt94.ClosingBalanceStatementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for closing balance statement date ({mt94.ClosingBalanceStatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Closing Balance Ccy.
            if (string.IsNullOrEmpty(mt94.ClosingBalanceCCY) || !currencies.Exists(c => c.CurrencyName == mt94.ClosingBalanceCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt94.ClosingBalanceCCY) ? "Closing balance currency cannot be null or missing." : $"Invalid closing balance currency value {mt94.ClosingBalanceCCY} used.")}");
            }

            //Closing Balance Amount.
            if (string.IsNullOrEmpty(mt94.ClosingBalanceAmount.ToString()))
            {
                BuildResponse("Closing balance amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt94.ClosingBalanceAmount.ToString()))
                {
                    BuildResponse("Closing balance amount must contain numeric values only.");
                }
            }

            //Closing Available Balance Statement Date.
            if (string.IsNullOrEmpty(mt94.ClosingAvailableBalanceStatementDate.ToString()))
            {
                BuildResponse("Closing available balance statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt94.ClosingAvailableBalanceStatementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for closing available balance statement date ({mt94.ClosingAvailableBalanceStatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Closing Available Balance Ccy.
            if (string.IsNullOrEmpty(mt94.ClosingAvailableBalanceCCY) || !currencies.Exists(c => c.CurrencyName == mt94.ClosingAvailableBalanceCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt94.ClosingAvailableBalanceCCY) ? "Closing balance currency cannot be null or missing." : $"Invalid closing balance currency value {mt94.ClosingAvailableBalanceCCY} used.")}");
            }

            //Closing Available Balance Amount.
            if (string.IsNullOrEmpty(mt94.ClosingAvailableBalanceAmount.ToString()))
            {
                BuildResponse("Closing available balance amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt94.ClosingAvailableBalanceAmount.ToString()))
                {
                    BuildResponse("Closing available balance amount must contain numeric values only.");
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt94.Email))
            {
                string[] emails = mt94.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        ///  Validate the MT950, MT970 and MT972 Swift Requests.
        /// </summary>
        /// <param name="mt95"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT95(MT95 mt95, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt95.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt95.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt95.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt95.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt95.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt95.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt95.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Account Number.
            if (string.IsNullOrEmpty(mt95.AccountNumber))
            {
                BuildResponse("Account number cannot be null or missing.");
            }

            //Statement Number.
            if (string.IsNullOrEmpty(mt95.StatementNumber.ToString()))
            {
                BuildResponse("Statement number cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt95.StatementNumber.ToString()))
                {
                    BuildResponse("Statement number must contain numeric values only.");
                }
            }

            //Statement Sequence Number.
            if (string.IsNullOrEmpty(mt95.StatementSequenceNumber.ToString()))
            {
                BuildResponse("Statement sequence number cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt95.StatementSequenceNumber.ToString()))
                {
                    BuildResponse("Statement sequence number must contain numeric values only.");
                }
            }

            //Opening Balance Statement Date.
            if (string.IsNullOrEmpty(mt95.OpeningBalanceStatementDate.ToString()))
            {
                BuildResponse("Opening balance statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt95.OpeningBalanceStatementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for opening balance statement date ({mt95.OpeningBalanceStatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Opening Balance Ccy.
            if (string.IsNullOrEmpty(mt95.OpeningBalanceCCY) || !currencies.Exists(c => c.CurrencyName == mt95.OpeningBalanceCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt95.OpeningBalanceCCY) ? "Opening balance currency cannot be null or missing." : $"Invalid opening balance currency value {mt95.OpeningBalanceCCY} used.")}");
            }

            //Opening Balance Amount.
            if (string.IsNullOrEmpty(mt95.OpeningBalanceAmount.ToString()))
            {
                BuildResponse("Opening balance amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt95.OpeningBalanceAmount.ToString()))
                {
                    BuildResponse("Opening balance amount must contain numeric values only.");
                }
            }

            //Closing Balance Statement Date.
            if (string.IsNullOrEmpty(mt95.ClosingBalanceStatementDate.ToString()))
            {
                BuildResponse("Closing balance statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt95.ClosingBalanceStatementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for closing balance statement date ({mt95.ClosingBalanceStatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Closing Balance Ccy.
            if (string.IsNullOrEmpty(mt95.ClosingBalanceCCY) || !currencies.Exists(c => c.CurrencyName == mt95.ClosingBalanceCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt95.ClosingBalanceCCY) ? "Closing balance currency cannot be null or missing." : $"Invalid closing balance currency value {mt95.ClosingBalanceCCY} used.")}");
            }

            //Closing Balance Amount.
            if (string.IsNullOrEmpty(mt95.ClosingBalanceAmount.ToString()))
            {
                BuildResponse("Closing balance amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt95.ClosingBalanceAmount.ToString()))
                {
                    BuildResponse("Closing balance amount must contain numeric values only.");
                }
            }

            //Closing Available Balance Statement Date.
            if (string.IsNullOrEmpty(mt95.ClosingAvailableBalanceStatementDate.ToString()))
            {
                BuildResponse("Closing available balance statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt95.ClosingAvailableBalanceStatementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for closing available balance statement date ({mt95.ClosingAvailableBalanceStatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Closing Available Balance Ccy.
            if (string.IsNullOrEmpty(mt95.ClosingAvailableBalanceCCY) || !currencies.Exists(c => c.CurrencyName == mt95.ClosingAvailableBalanceCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt95.ClosingAvailableBalanceCCY) ? "Closing balance currency cannot be null or missing." : $"Invalid closing balance currency value {mt95.ClosingAvailableBalanceCCY} used.")}");
            }

            //Closing Available Balance Amount.
            if (string.IsNullOrEmpty(mt95.ClosingAvailableBalanceAmount.ToString()))
            {
                BuildResponse("Closing available balance amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt95.ClosingAvailableBalanceAmount.ToString()))
                {
                    BuildResponse("Closing available balance amount must contain numeric values only.");
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt95.Email))
            {
                string[] emails = mt95.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT971 Swift Request.
        /// </summary>
        /// <param name="mt971"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT971(MT971 mt971, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt971.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt971.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt971.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt971.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt971.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt971.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt971.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Account Number.
            if (string.IsNullOrEmpty(mt971.AccountNumber))
            {
                BuildResponse("Account number cannot be null or missing.");
            }

            //Closing Balance Statement Date.
            if (string.IsNullOrEmpty(mt971.ClosingBalanceStatementDate.ToString()))
            {
                BuildResponse("Closing balance statement date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(mt971.ClosingBalanceStatementDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for closing balance statement date ({mt971.ClosingBalanceStatementDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Closing Balance Ccy.
            if (string.IsNullOrEmpty(mt971.ClosingBalanceCCY) || !currencies.Exists(c => c.CurrencyName == mt971.ClosingBalanceCCY.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(mt971.ClosingBalanceCCY) ? "Closing balance currency cannot be null or missing." : $"Invalid closing balance currency value {mt971.ClosingBalanceCCY} used.")}");
            }

            //Closing Balance Amount.
            if (string.IsNullOrEmpty(mt971.ClosingBalanceAmount.ToString()))
            {
                BuildResponse("Closing balance amount cannot be null or missing.");
            }
            else
            {
                if (!IsNumericFromTryParse(mt971.ClosingBalanceAmount.ToString()))
                {
                    BuildResponse("Closing balance amount must contain numeric values only.");
                }
            }

            //Email
            if (!string.IsNullOrEmpty(mt971.Email))
            {
                string[] emails = mt971.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the MT973 Swift Request.
        /// </summary>
        /// <param name="mt973"></param>
        /// <param name="currencies"></param>
        /// <param name="validateSwift"></param>
        /// <returns></returns>
        public string ValidateMT973(MT973 mt973, List<Currencies> currencies, bool validateSwift)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(mt973.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt973.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({mt973.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Client swift code cannot be null or missing.");
            }

            //Broker Swift Code.
            if (!string.IsNullOrEmpty(mt973.BrokerSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(mt973.BrokerSwiftCode))
                    {
                        BuildResponse($"Broker swift code ({mt973.BrokerSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Broker swift code cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(mt973.OrderRef))
            {
                BuildResponse("Order Ref cannot be null or missing.");
            }

            //Message.
            if (string.IsNullOrEmpty(mt973.Message))
            {
                BuildResponse("Message cannot be null or missing.");
            }

            //Account Number.
            if (string.IsNullOrEmpty(mt973.AccountNumber))
            {
                BuildResponse("Account number cannot be null or missing.");
            }

            //Email
            if (!string.IsNullOrEmpty(mt973.Email))
            {
                string[] emails = mt973.Email.Split(';');
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        public string ValidatePain001<T>(T pain001S, List<Currencies> currencies, List<Countries> countries,
            bool validateSwift, bool validateIban)
        {
            dynamic pain001 = pain001S;
            //Client Swift Code.
            if (!string.IsNullOrEmpty(pain001.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(pain001.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({pain001.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Settle Date.
            if (string.IsNullOrEmpty(pain001.SettleDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(pain001.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({pain001.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount.
            if (pain001.Amount < 0 || !IsNumericFromTryParse(pain001.Amount.ToString()))
            {
                BuildResponse($"{(!IsNumericFromTryParse(pain001.Amount.ToString()) ? "Settle ammount must contain numeric values only." : "Settle ammount must be 0 or greater.")}");
            }

            //Ccy.
            if (string.IsNullOrEmpty(pain001.Ccy) || !currencies.Exists(c => c.CurrencyName == pain001.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(pain001.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {pain001.Ccy} used.")}");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(pain001.OrderRef))
            {
                BuildResponse($"Order ref cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(pain001.OrderSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(pain001.OrderSwift))
                    {
                        BuildResponse($"Order swift code ({pain001.OrderSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Order IBAN/Account Number.
            if (!string.IsNullOrEmpty(pain001.OrderIbanAccountNumber))
            {
                //if (!pain001.OrderIbanAccountNumber.All(Char.IsDigit))
                if (!((string)pain001.OrderIbanAccountNumber).All(Char.IsDigit))
                {
                    if (validateIban)
                    {
                        if (!ValidIBAN(pain001.OrderIbanAccountNumber))
                        {
                            BuildResponse($"Order IBAN ({pain001.OrderIbanAccountNumber}) appears to be incorrectly formatted.");
                        }
                    }
                }
            }

            //Order Country Code
            if (!string.IsNullOrEmpty(pain001.OrderCountryCode))
            {
                if (!countries.Exists(x => x.CountryCode == pain001.OrderCountryCode))
                {
                    BuildResponse($"Invalid order country code {pain001.OrderCountryCode} used.");
                }
            }

            //Beneficiary Ccy.
            if (!string.IsNullOrEmpty(pain001.BeneficiaryCcy))
            {
                if (!currencies.Exists(c => c.CurrencyName == pain001.BeneficiaryCcy.ToUpper()))
                {
                    BuildResponse($"Invalid benficiary currency value {pain001.BeneficiaryCcy} used.");
                }
            }

            //Beneficiary Country Code
            if (!string.IsNullOrEmpty(pain001.BenCountryCode))
            {
                if (!countries.Exists(x => x.CountryCode == pain001.BenCountryCode))
                {
                    BuildResponse($"Invalid beneficiary country code {pain001.BenCountryCode} used.");
                }
            }

            //Beneficiary Bank Swift Code.
            if (!string.IsNullOrEmpty(pain001.BeneficiaryBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(pain001.BeneficiaryBankSwift))
                    {
                        BuildResponse($"Beneficiary bank swift code ({pain001.BeneficiaryBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Bank IBAN.
            if (!string.IsNullOrEmpty(pain001.BeneficiaryBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(pain001.BeneficiaryBankIBAN))
                    {
                        BuildResponse($"Beneficiary bank IBAN ({pain001.BeneficiaryBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Bank Country Code
            if (!string.IsNullOrEmpty(pain001.BeneficiaryBankCountryCode))
            {
                if (!countries.Exists(x => x.CountryCode == pain001.BeneficiaryBankCountryCode))
                {
                    BuildResponse($"Invalid beneficiary bank country code {pain001.BeneficiaryBankCountryCode} used.");
                }
            }

            //Beneficiary Sub Bank Swift Code.
            if (!string.IsNullOrEmpty(pain001.BeneficiarySubBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(pain001.BeneficiarySubBankSwift))
                    {
                        BuildResponse($"Beneficiary sub bank swift code ({pain001.BeneficiarySubBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Sub Bank IBAN.
            if (!string.IsNullOrEmpty(pain001.BeneficiarySubBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(pain001.BeneficiarySubBankIBAN))
                    {
                        BuildResponse($"Beneficiary sub bank IBAN ({pain001.BeneficiarySubBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Sub Bank Country Code
            if (!string.IsNullOrEmpty(pain001.BenSubBankCountryCode))
            {
                if (!countries.Exists(x => x.CountryCode == pain001.BenSubBankCountryCode))
                {
                    BuildResponse($"Invalid beneficiary sub bank country code {pain001.BenSubBankCountryCode} used.");
                }
            }

            //Email.
            if (!string.IsNullOrEmpty(pain001.Email))
            {
                string[] emails = pain001.Email.Split(";");
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        public string ValidatePacs008(Pacs008 pacs008, List<Currencies> currencies, List<Countries> countries,
            bool validateSwift, bool validateIban)
        {
            //Client Swift Code.
            if (!string.IsNullOrEmpty(pacs008.ClientSwiftCode))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(pacs008.ClientSwiftCode))
                    {
                        BuildResponse($"Client swift code ({pacs008.ClientSwiftCode}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Client swift code cannot be null or missing.");
            }

            //Settle Date.
            if (string.IsNullOrEmpty(pacs008.SettleDate.ToString()))
            {
                BuildResponse("Settlement Date cannot be null or missing.");
            }
            else
            {
                if (DateTime.TryParse(pacs008.SettleDate.ToString(), out DateTime tempTo) == false)
                {
                    BuildResponse($"Invalid format for Settlement Date ({pacs008.SettleDate}), this must be yyyy-mm-dd and be a valid date");
                }
            }

            //Amount.
            if (pacs008.Amount < 0 || !IsNumericFromTryParse(pacs008.Amount.ToString()))
            {
                BuildResponse($"{(!IsNumericFromTryParse(pacs008.Amount.ToString()) ? "Settle ammount must contain numeric values only." : "Settle ammount must be 0 or greater.")}");
            }

            //Ccy.
            if (string.IsNullOrEmpty(pacs008.Ccy) || !currencies.Exists(c => c.CurrencyName == pacs008.Ccy.ToUpper()))
            {
                BuildResponse($"{(string.IsNullOrEmpty(pacs008.Ccy) ? "Currency cannot be null or missing." : $"Invalid currency value {pacs008.Ccy} used.")}");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(pacs008.OrderRef))
            {
                BuildResponse($"Order ref cannot be null or missing.");
            }

            //Order Swift Code.
            if (!string.IsNullOrEmpty(pacs008.OrderSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(pacs008.OrderSwift))
                    {
                        BuildResponse($"Order swift code ({pacs008.OrderSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Order IBAN/Account Number.
            if (!string.IsNullOrEmpty(pacs008.OrderIbanAccountNumber))
            {
                if (!pacs008.OrderIbanAccountNumber.All(Char.IsDigit))
                {
                    if (validateIban)
                    {
                        if (!ValidIBAN(pacs008.OrderIbanAccountNumber))
                        {
                            BuildResponse($"Order IBAN ({pacs008.OrderIbanAccountNumber}) appears to be incorrectly formatted.");
                        }
                    }
                }
            }

            //Order Country Code
            if (!string.IsNullOrEmpty(pacs008.OrderCountryCode))
            {
                if (!countries.Exists(x => x.CountryCode == pacs008.OrderCountryCode))
                {
                    BuildResponse($"Invalid order country code {pacs008.OrderCountryCode} used.");
                }
            }

            //Beneficiary Ccy.
            if (!string.IsNullOrEmpty(pacs008.BeneficiaryCcy))
            {
                if (!currencies.Exists(c => c.CurrencyName == pacs008.Ccy.ToUpper()))
                {
                    BuildResponse($"Invalid benficiary currency value {pacs008.BeneficiaryCcy} used.");
                }
            }

            //Beneficiary Country Code
            if (!string.IsNullOrEmpty(pacs008.BenCountryCode))
            {
                if (!countries.Exists(x => x.CountryCode == pacs008.BenCountryCode))
                {
                    BuildResponse($"Invalid beneficiary country code {pacs008.BenCountryCode} used.");
                }
            }

            //Beneficiary Bank Swift Code.
            if (!string.IsNullOrEmpty(pacs008.BeneficiaryBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(pacs008.BeneficiaryBankSwift))
                    {
                        BuildResponse($"Beneficiary bank swift code ({pacs008.BeneficiaryBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Bank IBAN.
            if (!string.IsNullOrEmpty(pacs008.BeneficiaryBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(pacs008.BeneficiaryBankIBAN))
                    {
                        BuildResponse($"Beneficiary bank IBAN ({pacs008.BeneficiaryBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Bank Country Code
            if (!string.IsNullOrEmpty(pacs008.BeneficiaryBankCountryCode))
            {
                if (!countries.Exists(x => x.CountryCode == pacs008.BeneficiaryBankCountryCode))
                {
                    BuildResponse($"Invalid beneficiary bank country code {pacs008.BeneficiaryBankCountryCode} used.");
                }
            }

            //Beneficiary Sub Bank Swift Code.
            if (!string.IsNullOrEmpty(pacs008.BeneficiarySubBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(pacs008.BeneficiarySubBankSwift))
                    {
                        BuildResponse($"Beneficiary sub bank swift code ({pacs008.BeneficiarySubBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Sub Bank IBAN.
            if (!string.IsNullOrEmpty(pacs008.BeneficiarySubBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(pacs008.BeneficiarySubBankIBAN))
                    {
                        BuildResponse($"Beneficiary sub bank IBAN ({pacs008.BeneficiarySubBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }

            //Beneficiary Sub Bank Country Code
            if (!string.IsNullOrEmpty(pacs008.BenSubBankCountryCode))
            {
                if (!countries.Exists(x => x.CountryCode == pacs008.BenSubBankCountryCode))
                {
                    BuildResponse($"Invalid beneficiary sub bank country code {pacs008.BenSubBankCountryCode} used.");
                }
            }

            //Email.
            if (!string.IsNullOrEmpty(pacs008.Email))
            {
                string[] emails = pacs008.Email.Split(";");
                foreach (string email in emails)
                {
                    if (!new EmailAddressAttribute().IsValid(email))
                    {
                        BuildResponse($"Invalid email address {email} used.");
                    }
                }
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the NatWest Domestic Payment request
        /// </summary>
        /// <param name="natWestDomesticPayment"></param>
        /// <param name="currencies"></param>
        /// <returns></returns>
        public string ValidateNatWestDomesticPayment(OpenPankingDomesticPayment natWestDomesticPayment, List<Currencies> currencies)
        {
            //Amount.
            if (natWestDomesticPayment.Amount < 0 || !IsNumericFromTryParse(natWestDomesticPayment.Amount.ToString()))
            {
                BuildResponse($"{(!IsNumericFromTryParse(natWestDomesticPayment.Amount.ToString()) ? "Ammount must contain numeric values only." : "Ammount must be 0 or greater.")}");
            }

            //Ccy.
            if (!string.IsNullOrEmpty(natWestDomesticPayment.CCY))
            {
                if (!currencies.Exists(c => c.CurrencyName == natWestDomesticPayment.CCY.ToUpper()))
                {
                    BuildResponse($"Invalid benficiary currency value {natWestDomesticPayment.CCY} used.");
                }
            }
            else
            {
                BuildResponse($"Ccy cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(natWestDomesticPayment.OrderRef))
            {
                BuildResponse("Order ref cannot be null or missing.");
            }

            //Order IBAN or Account No.
            if (string.IsNullOrEmpty(natWestDomesticPayment.OrderIBANorAccountNo))
            {
                BuildResponse("Order IBAN or account no cannot be null or missing.");
            }

            //Ben Name.
            if (string.IsNullOrEmpty(natWestDomesticPayment.BenName))
            {
                BuildResponse("Ben name cannot be null or missing.");
            }

            //Ben IBAN or Account No.
            if (string.IsNullOrEmpty(natWestDomesticPayment.BenIBANorAccountNo))
            {
                BuildResponse("Ben IBAN or account no cannot be null or missing.");
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Validate the NatWest International Payment request
        /// </summary>
        /// <param name="natWestInternationalPayment"></param>
        /// <param name="currencies"></param>
        /// <param name="countries"></param>
        /// <returns></returns>
        public string ValidateNatWestInternationalPayment(OpenBankingInternationalPayment natWestInternationalPayment, List<Currencies> currencies,
            List<Countries> countries, bool validateSwift, bool validateIban)
        {
            //Amount.
            if (natWestInternationalPayment.Amount < 0 || !IsNumericFromTryParse(natWestInternationalPayment.Amount.ToString()))
            {
                BuildResponse($"{(!IsNumericFromTryParse(natWestInternationalPayment.Amount.ToString()) ? "Ammount must contain numeric values only." : "Ammount must be 0 or greater.")}");
            }

            //Ccy.
            if (!string.IsNullOrEmpty(natWestInternationalPayment.CCY))
            {
                if (!currencies.Exists(c => c.CurrencyName == natWestInternationalPayment.CCY.ToUpper()))
                {
                    BuildResponse($"Invalid benficiary currency value {natWestInternationalPayment.CCY} used.");
                }
            }
            else
            {
                BuildResponse($"Ccy cannot be null or missing.");
            }

            //Order Ref.
            if (string.IsNullOrEmpty(natWestInternationalPayment.OrderRef))
            {
                BuildResponse("Order ref cannot be null or missing.");
            }

            //Order IBAN or Account No.
            if (string.IsNullOrEmpty(natWestInternationalPayment.OrderIBANorAccountNo))
            {
                BuildResponse("Order IBAN or account no cannot be null or missing.");
            }

            //Ben Name.
            if (string.IsNullOrEmpty(natWestInternationalPayment.BenName))
            {
                BuildResponse("Ben name cannot be null or missing.");
            }

            //Ben Bank Swift.
            if (!string.IsNullOrEmpty(natWestInternationalPayment.BenBankSwift))
            {
                if (validateSwift)
                {
                    if (!ValidSwift(natWestInternationalPayment.BenBankSwift))
                    {
                        BuildResponse($"Ben bank swift code ({natWestInternationalPayment.BenBankSwift}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse($"Ben bank swift code cannot be null or missing.");
            }

            //Ben Bank IBAN.
            if (!string.IsNullOrEmpty(natWestInternationalPayment.BenBankIBAN))
            {
                if (validateIban)
                {
                    if (!ValidIBAN(natWestInternationalPayment.BenBankIBAN))
                    {
                        BuildResponse($"Beneficiary bank IBAN ({natWestInternationalPayment.BenBankIBAN}) appears to be incorrectly formatted.");
                    }
                }
            }
            else
            {
                BuildResponse("Ben bank IBAN cannot be null or missing.");
            }

            //Ben Bank Country Code.
            if (!string.IsNullOrEmpty(natWestInternationalPayment.BenBankCountryCode))
            {
                if (!countries.Exists(c => c.CountryCode == natWestInternationalPayment.BenBankCountryCode.ToUpper()))
                {
                    BuildResponse($"Invalid bank country code {natWestInternationalPayment.BenBankCountryCode} used.");
                }
            }
            else
            {
                BuildResponse("Ben bank country code cannot be null or missing.");
            }

            _Ind = 1;
            return _response.ToString();
        }

        /// <summary>
        /// Perform validation checks on the IBAN values.
        /// </summary>
        /// <param name="ibanCode"></param>
        /// <returns></returns>
        private static bool ValidIBAN(string ibanCode)
        {
            string varFirstLetter = ibanCode.Substring(0, 1).ToUpper();
            string varSecondLetter = ibanCode.Substring(1, 1).ToUpper();
            string JoinLetter = $"{varFirstLetter}{varSecondLetter}";

            if (ibanCode.Length < 15 || ibanCode.Length > 32)
            {
                return false;
            }
            else if (char.Parse(varFirstLetter) < 15 || char.Parse(varFirstLetter) > 90)
            {
                return false;
            }
            else if (char.Parse(varSecondLetter) < 15 || char.Parse(varSecondLetter) > 90)
            {
                return false;
            }
            else if (JoinLetter == "AX" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "AL" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "AD" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "AT" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "BE" && ibanCode.Length != 16)
            {
                return false;
            }
            else if (JoinLetter == "BA" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "BG" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "HR" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "CY" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "CZ" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "DK" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "EE" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "FO" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "FI" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "FR" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "DE" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "GI" && ibanCode.Length != 23)
            {
                return false;
            }
            else if (JoinLetter == "GR" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "GL" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "HU" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "IS" && ibanCode.Length != 26)
            {
                return false;
            }
            else if (JoinLetter == "IE" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "IT" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "XK" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "LV" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "LI" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "LT" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "LU" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "MK" && ibanCode.Length != 19)
            {
                return false;
            }
            else if (JoinLetter == "MT" && ibanCode.Length != 31)
            {
                return false;
            }
            else if (JoinLetter == "MD" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "MC" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "ME" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "NL" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "NO" && ibanCode.Length != 15)
            {
                return false;
            }
            else if (JoinLetter == "PL" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "PT" && ibanCode.Length != 25)
            {
                return false;
            }
            else if (JoinLetter == "RO" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "SM" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "RS" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "SK" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "SI" && ibanCode.Length != 19)
            {
                return false;
            }
            else if (JoinLetter == "ES" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "SE" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "CH" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "GB" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "AZ" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "BH" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "BR" && ibanCode.Length != 29)
            {
                return false;
            }
            else if (JoinLetter == "VG" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "CR" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "DO" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "GF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "PF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "TF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "GE" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "GP" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "GU" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "IL" && ibanCode.Length != 23)
            {
                return false;
            }
            else if (JoinLetter == "JO" && ibanCode.Length != 30)
            {
                return false;
            }
            else if (JoinLetter == "KZ" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "KW" && ibanCode.Length != 30)
            {
                return false;
            }
            else if (JoinLetter == "LB" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "MQ" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "MR" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "MU" && ibanCode.Length != 30)
            {
                return false;
            }
            else if (JoinLetter == "YT" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "NC" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "PK" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "PS" && ibanCode.Length != 29)
            {
                return false;
            }
            else if (JoinLetter == "QA" && ibanCode.Length != 29)
            {
                return false;
            }
            else if (JoinLetter == "RE" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "BL" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "LC" && ibanCode.Length != 32)
            {
                return false;
            }
            else if (JoinLetter == "MF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "PM" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "ST" && ibanCode.Length != 25)
            {
                return false;
            }
            else if (JoinLetter == "SA" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "TL" && ibanCode.Length != 23)
            {
                return false;
            }
            else if (JoinLetter == "TN" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "TR" && ibanCode.Length != 26)
            {
                return false;
            }
            else if (JoinLetter == "UA" && ibanCode.Length != 29)
            {
                return false;
            }
            else if (JoinLetter == "AE" && ibanCode.Length != 23)
            {
                return false;
            }
            else if (JoinLetter == "WF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "ME" && ibanCode.Length != 22)
            {
                {
                    return false;
                }
            }
            else if (!ValidIBANConvertAndConfirm(ibanCode))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Confirm if the IBAN value is valid or not.
        /// </summary>
        /// <param name="ibanCode"></param>
        /// <returns></returns>
        private static bool ValidIBANConvertAndConfirm(string ibanCode)
        {
            string tempIbanCode;
            string replaceChar;
            string replaceBy;
            long leftOver = 0;
            int digit;

            var holdingVal = ibanCode.Replace(" ", "").ToUpper();
            tempIbanCode = $"{holdingVal.Substring(4, holdingVal.Length - 4)}{holdingVal.Substring(0, 4)}";
            for (int i = 10; i <= 35; i++)
            {
                replaceChar = Convert.ToChar(i + 55).ToString();
                replaceBy = Convert.ToString(i);
                tempIbanCode = tempIbanCode.Replace(replaceChar, replaceBy);
            }

            for (int i = 0; i <= tempIbanCode.Length - 1; i++)
            {
                digit = Convert.ToInt32(tempIbanCode.Substring(i, 1));
                leftOver = (10 * leftOver + digit) % 97;
            }

            if (leftOver == 1)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Perform validation checks on the Swift values.
        /// </summary>
        /// <param name="swiftCode"></param>
        /// <returns></returns>
        private static bool ValidSwift(string swiftCode)
        {
            swiftCode = swiftCode.Replace("-", "");
            if (swiftCode.Length != 8 && swiftCode.Length != 11 && swiftCode.Length != 6)
            {
                return false;
            }

            //if (!swiftCode.Take(6).All(char.IsLetter))
            //{
            //    return false;
            //}
            return true;
        }

        /// <summary>
        /// Perform validation on non integer values e.g. Float, Decimal etc.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool IsNumericFromTryParse(string value)
        {
            value = value.Replace(",", "").Replace("-", "");
            return (double.TryParse(value, NumberStyles.Float,
                    NumberFormatInfo.CurrentInfo, out double result));
        }

        /// <summary>
        /// Populate the string builder object _response with any validation erros found.
        /// </summary>
        /// <param name="_msg"></param>
        private void BuildResponse(string _msg)
        {
            _response.AppendLine($"{_Ind}. {_msg}");
            _Ind++;
        }
    }
}