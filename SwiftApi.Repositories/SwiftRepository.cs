﻿using Microsoft.Extensions.Logging;
using SwiftApi.Models.Api;
using SwiftApi.Models.Classes;
using SwiftApi.Models.Configuration;
using SwiftApi.Repositories.Interfaces;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using DataType = System.Data;

namespace SwiftApi.Repositories
{
    public class SwiftRepository : ISwift
    {
        private readonly ILogger _logger;
        private readonly SwiftConfig _swiftConfig;

        public SwiftRepository(SwiftConfig swiftConfig, ILogger logger)
        {
            _swiftConfig = swiftConfig;
            _logger = logger;
        }

        /// <summary>
        /// Call the Swift MT101 Stored Procedure.
        /// </summary>
        /// <param name="mt101"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT101(MT101 mt101, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT101_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt101.ClientSwiftCode;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.SmallDateTime).Value = mt101.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mt101.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = mt101.Ccy;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt101.OrderRef;
                comm.Parameters.Add("@OrderName", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt101.OrderName) ? (object)DBNull.Value : mt101.OrderName;
                comm.Parameters.Add("@OrderAddress", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.OrderAddress) ? (object)DBNull.Value : mt101.OrderAddress;
                comm.Parameters.Add("@OrderCity", DataType.SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(mt101.OrderCity) ? (object)DBNull.Value : mt101.OrderCity;
                comm.Parameters.Add("@OrderPostCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt101.OrderPostCode) ? (object)DBNull.Value : mt101.OrderPostCode;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt101.OrderSwift) ? (object)DBNull.Value : mt101.OrderSwift;
                comm.Parameters.Add("@OrderIban", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt101.OrderIban) ? (object)DBNull.Value : mt101.OrderIban;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.OrderOther) ? (object)DBNull.Value : mt101.OrderOther;
                comm.Parameters.Add("@OrderVOCode", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt101.OrderVOCode) ? (object)DBNull.Value : mt101.OrderVOCode;
                comm.Parameters.Add("@BenName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiaryName) ? (object)DBNull.Value : mt101.BeneficiaryName;
                comm.Parameters.Add("@BenAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiaryAddress1) ? (object)DBNull.Value : mt101.BeneficiaryAddress1;
                comm.Parameters.Add("@BenAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiaryAddress2) ? (object)DBNull.Value : mt101.BeneficiaryAddress2;
                comm.Parameters.Add("@BenZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt101.BeneficiaryPostCode) ? (object)DBNull.Value : mt101.BeneficiaryPostCode;
                comm.Parameters.Add("@BenSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt101.BeneficiarySwift) ? (object)DBNull.Value : mt101.BeneficiarySwift;
                comm.Parameters.Add("@BenIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt101.BeneficiaryIBAN) ? (object)DBNull.Value : mt101.BeneficiaryIBAN;
                comm.Parameters.Add("@BenBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiaryBankName) ? (object)DBNull.Value : mt101.BeneficiaryBankName;
                comm.Parameters.Add("@BenBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiaryBankAddress1) ? (object)DBNull.Value : mt101.BeneficiaryBankAddress1;
                comm.Parameters.Add("@BenBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiaryBankAddress2) ? (object)DBNull.Value : mt101.BeneficiaryBankAddress2;
                comm.Parameters.Add("@BenBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt101.BeneficiaryBankPostCode) ? (object)DBNull.Value : mt101.BeneficiaryBankPostCode;
                comm.Parameters.Add("@BenBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt101.BeneficiaryBankSwift) ? (object)DBNull.Value : mt101.BeneficiaryBankSwift;
                comm.Parameters.Add("@BenBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt101.BeneficiaryBankIBAN) ? (object)DBNull.Value : mt101.BeneficiaryBankIBAN;
                comm.Parameters.Add("@BenBankBIK", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiaryBankBIK) ? (object)DBNull.Value : mt101.BeneficiaryBankBIK;
                comm.Parameters.Add("@BenBankINN", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiaryBankINN) ? (object)DBNull.Value : mt101.BeneficiaryBankINN;
                comm.Parameters.Add("@BenSubBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankName) ? (object)DBNull.Value : mt101.BeneficiarySubBankName;
                comm.Parameters.Add("@BenSubBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankAddress1) ? (object)DBNull.Value : mt101.BeneficiarySubBankAddress1;
                comm.Parameters.Add("@BenSubBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankAddress2) ? (object)DBNull.Value : mt101.BeneficiarySubBankAddress2;
                comm.Parameters.Add("@BenSubBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankPostCode) ? (object)DBNull.Value : mt101.BeneficiarySubBankPostCode;
                comm.Parameters.Add("@BenSubBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankSwift) ? (object)DBNull.Value : mt101.BeneficiarySubBankSwift;
                comm.Parameters.Add("@BenSubBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankIBAN) ? (object)DBNull.Value : mt101.BeneficiarySubBankIBAN;
                comm.Parameters.Add("@BenSubBankOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankOther) ? (object)DBNull.Value : mt101.BeneficiarySubBankOther;
                comm.Parameters.Add("@BenSubBankBik", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankBik) ? (object)DBNull.Value : mt101.BeneficiarySubBankBik;
                comm.Parameters.Add("@BenSubBankINN", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt101.BeneficiarySubBankINN) ? (object)DBNull.Value : mt101.BeneficiarySubBankINN;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt101.SwiftUrgent;
                comm.Parameters.Add("@SwiftSendRef", DataType.SqlDbType.Bit).Value = mt101.SwiftSendRef;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT101 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt101.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT101_StoredProc}");
                return $"Error fetching the MT101 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT103 Stored Procedure.
        /// </summary>
        /// <param name="mt103"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT103(MT103 mt103, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT103_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt103.ClientSwiftCode;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.SmallDateTime).Value = mt103.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mt103.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = mt103.Ccy;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt103.OrderRef;
                comm.Parameters.Add("@OrderName", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt103.OrderName) ? (object)DBNull.Value : mt103.OrderName;
                comm.Parameters.Add("@OrderAddress", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.OrderAddress) ? (object)DBNull.Value : mt103.OrderAddress;
                comm.Parameters.Add("@OrderCity", DataType.SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(mt103.OrderCity) ? (object)DBNull.Value : mt103.OrderCity;
                comm.Parameters.Add("@OrderPostCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.OrderPostCode) ? (object)DBNull.Value : mt103.OrderPostCode;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.OrderSwift) ? (object)DBNull.Value : mt103.OrderSwift;
                comm.Parameters.Add("@OrderIban", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.OrderIbanAccountNumber) ? (object)DBNull.Value : mt103.OrderIbanAccountNumber;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.OrderOther) ? (object)DBNull.Value : mt103.OrderOther;
                comm.Parameters.Add("@OrderVOCode", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt103.OrderVOCode) ? (object)DBNull.Value : mt103.OrderVOCode;
                comm.Parameters.Add("@BenName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiaryName) ? (object)DBNull.Value : mt103.BeneficiaryName;
                comm.Parameters.Add("@BenAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiaryAddress1) ? (object)DBNull.Value : mt103.BeneficiaryAddress1;
                comm.Parameters.Add("@BenAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiaryAddress2) ? (object)DBNull.Value : mt103.BeneficiaryAddress2;
                comm.Parameters.Add("@BenZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.BeneficiaryPostCode) ? (object)DBNull.Value : mt103.BeneficiaryPostCode;
                comm.Parameters.Add("@BenSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.BeneficiarySwift) ? (object)DBNull.Value : mt103.BeneficiarySwift;
                comm.Parameters.Add("@BenIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt103.BeneficiaryIBAN) ? (object)DBNull.Value : mt103.BeneficiaryIBAN;
                comm.Parameters.Add("@BenCcy", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt103.BeneficiaryCcy) ? (object)DBNull.Value : mt103.BeneficiaryCcy;
                comm.Parameters.Add("@BenBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiaryBankName) ? (object)DBNull.Value : mt103.BeneficiaryBankName;
                comm.Parameters.Add("@BenBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiaryBankAddress1) ? (object)DBNull.Value : mt103.BeneficiaryBankAddress1;
                comm.Parameters.Add("@BenBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiaryBankAddress2) ? (object)DBNull.Value : mt103.BeneficiaryBankAddress2;
                comm.Parameters.Add("@BenBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.BeneficiaryBankPostCode) ? (object)DBNull.Value : mt103.BeneficiaryBankPostCode;
                comm.Parameters.Add("@BenBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.BeneficiaryBankSwift) ? (object)DBNull.Value : mt103.BeneficiaryBankSwift;
                comm.Parameters.Add("@BenBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt103.BeneficiaryBankIBAN) ? (object)DBNull.Value : mt103.BeneficiaryBankIBAN;
                comm.Parameters.Add("@BenSubBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiarySubBankName) ? (object)DBNull.Value : mt103.BeneficiarySubBankName;
                comm.Parameters.Add("@BenSubBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiarySubBankAddress1) ? (object)DBNull.Value : mt103.BeneficiarySubBankAddress1;
                comm.Parameters.Add("@BenSubBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiarySubBankAddress2) ? (object)DBNull.Value : mt103.BeneficiarySubBankAddress2;
                comm.Parameters.Add("@BenSubBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.BeneficiarySubBankPostCode) ? (object)DBNull.Value : mt103.BeneficiarySubBankPostCode;
                comm.Parameters.Add("@BenSubBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt103.BeneficiarySubBankSwift) ? (object)DBNull.Value : mt103.BeneficiarySubBankSwift;
                comm.Parameters.Add("@BenSubBankBik", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt103.BeneficiarySubBankBik) ? (object)DBNull.Value : mt103.BeneficiarySubBankBik;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt103.SwiftUrgent;
                comm.Parameters.Add("@SwiftSendRef", DataType.SqlDbType.Bit).Value = mt103.SwiftSendRef;
                comm.Parameters.Add("@ExchangeRate", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt103.ExchangeRate.ToString()) ? (object)DBNull.Value : mt103.ExchangeRate;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT103 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt103.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT103_StoredProc}");
                return $"Error fetching the MT103 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT92 Stored Procedure.
        /// </summary>
        /// <param name="mt192"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT192(MT192 mt192, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt192.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt192.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt192.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mt192.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mt192.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt192.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mt192.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt192.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt192.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT192.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT192 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt192.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT192 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT199 Stored Procedure.
        /// </summary>
        /// <param name="mt199"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT199(MT199 mt199, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt199.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt199.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt199.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt199.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt199.OrderRelatedRef) ? string.Empty : mt199.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt199.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt199.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT199.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT199 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt199.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT199 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT200 Stored Procedure.
        /// </summary>
        /// <param name="mt200"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT200(MT200 mt200, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT200_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt200.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt200.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt200.OrderRef;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.SmallDateTime).Value = mt200.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mt200.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = mt200.Ccy;
                comm.Parameters.Add("@OrderIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt200.OrderIBAN) ? (object)DBNull.Value : mt200.OrderIBAN;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt200.OrderOther) ? (object)DBNull.Value : mt200.OrderOther;
                comm.Parameters.Add("@BenIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt200.BenIBAN) ? (object)DBNull.Value : mt200.BenIBAN;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt200.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT200 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt200.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT200_StoredProc}");
                return $"Error fetching the MT200 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT202 Stored Procedure.
        /// </summary>
        /// <param name="mt202"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT202(MT202 mt202, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT202_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt202.ClientSwiftCode;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.SmallDateTime).Value = mt202.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mt202.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = mt202.Ccy;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt202.OrderRef;
                comm.Parameters.Add("@OrderName", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt202.OrderName) ? (object)DBNull.Value : mt202.OrderName;
                comm.Parameters.Add("@OrderAddress", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.OrderAddress) ? (object)DBNull.Value : mt202.OrderAddress;
                comm.Parameters.Add("@OrderCity", DataType.SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(mt202.OrderCity) ? (object)DBNull.Value : mt202.OrderCity;
                comm.Parameters.Add("@OrderPostCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.OrderPostCode) ? (object)DBNull.Value : mt202.OrderPostCode;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.OrderSwift) ? (object)DBNull.Value : mt202.OrderSwift;
                comm.Parameters.Add("@OrderAccountNumber", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.OrderAccountNumber) ? (object)DBNull.Value : mt202.OrderAccountNumber;
                comm.Parameters.Add("@OrderIban", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt202.OrderIban) ? (object)DBNull.Value : mt202.OrderIban;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.OrderOther) ? (object)DBNull.Value : mt202.OrderOther;
                comm.Parameters.Add("@OrderVOCode", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt202.OrderVOCode) ? (object)DBNull.Value : mt202.OrderVOCode;
                comm.Parameters.Add("@BenRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.BeneficiaryRef) ? (object)DBNull.Value : mt202.BeneficiaryRef;
                comm.Parameters.Add("@BenName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiaryName) ? (object)DBNull.Value : mt202.BeneficiaryName;
                comm.Parameters.Add("@BenAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiaryAddress1) ? (object)DBNull.Value : mt202.BeneficiaryAddress1;
                comm.Parameters.Add("@BenAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiaryAddress2) ? (object)DBNull.Value : mt202.BeneficiaryAddress2;
                comm.Parameters.Add("@BenZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.BeneficiaryPostCode) ? (object)DBNull.Value : mt202.BeneficiaryPostCode;
                comm.Parameters.Add("@BenSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.BeneficiarySwift) ? (object)DBNull.Value : mt202.BeneficiarySwift;
                comm.Parameters.Add("@BenAccountNumber", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.BeneficiaryAccountNumber) ? (object)DBNull.Value : mt202.BeneficiaryAccountNumber;
                comm.Parameters.Add("@BenIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt202.BeneficiaryIBAN) ? (object)DBNull.Value : mt202.BeneficiaryIBAN;
                comm.Parameters.Add("@BenCcy", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt202.BeneficiaryCcy) ? (object)DBNull.Value : mt202.BeneficiaryCcy;
                comm.Parameters.Add("@BenSubBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankName) ? (object)DBNull.Value : mt202.BeneficiarySubBankName;
                comm.Parameters.Add("@BenSubBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankAddress1) ? (object)DBNull.Value : mt202.BeneficiarySubBankAddress1;
                comm.Parameters.Add("@BenSubBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankAddress2) ? (object)DBNull.Value : mt202.BeneficiarySubBankAddress2;
                comm.Parameters.Add("@BenSubBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankPostCode) ? (object)DBNull.Value : mt202.BeneficiarySubBankPostCode;
                comm.Parameters.Add("@BenSubBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankSwift) ? (object)DBNull.Value : mt202.BeneficiarySubBankSwift;
                comm.Parameters.Add("@BenSubBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankIBAN) ? (object)DBNull.Value : mt202.BeneficiarySubBankIBAN;
                comm.Parameters.Add("@BenSubBankOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankOther) ? (object)DBNull.Value : mt202.BeneficiarySubBankOther;
                comm.Parameters.Add("@BenSubBankBik", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankBik) ? (object)DBNull.Value : mt202.BeneficiarySubBankBik;
                comm.Parameters.Add("@BenSubBankINN", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiarySubBankINN) ? (object)DBNull.Value : mt202.BeneficiarySubBankINN;
                comm.Parameters.Add("@BenBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiaryBankName) ? (object)DBNull.Value : mt202.BeneficiaryBankName;
                comm.Parameters.Add("@BenBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiaryBankAddress1) ? (object)DBNull.Value : mt202.BeneficiaryBankAddress1;
                comm.Parameters.Add("@BenBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiaryBankAddress1) ? (object)DBNull.Value : mt202.BeneficiaryBankAddress1;
                comm.Parameters.Add("@BenBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.BeneficiaryBankAddress1) ? (object)DBNull.Value : mt202.BeneficiaryBankAddress1;
                comm.Parameters.Add("@BenBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt202.BeneficiaryBankSwift) ? (object)DBNull.Value : mt202.BeneficiaryBankSwift;
                comm.Parameters.Add("@BenBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(mt202.BeneficiaryBankIBAN) ? (object)DBNull.Value : mt202.BeneficiaryBankIBAN;
                comm.Parameters.Add("@BenBankBIK", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiaryBankBIK) ? (object)DBNull.Value : mt202.BeneficiaryBankBIK;
                comm.Parameters.Add("@BenBankINN", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt202.BeneficiaryBankINN) ? (object)DBNull.Value : mt202.BeneficiaryBankINN;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt202.SwiftUrgent;
                comm.Parameters.Add("@SwiftSendRef", DataType.SqlDbType.Bit).Value = mt202.SwiftSendRef;
                comm.Parameters.Add("@varSwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT202 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt202.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT202_StoredProc}");
                return $"Error fetching the MT202 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT210 Stored Procedure.
        /// </summary>
        /// <param name="mt210"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT210(MT210 mt210, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT210_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt210.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt210.ClientSwiftCode;
                comm.Parameters.Add("@BenSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt210.BenSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt210.OrderRef;
                comm.Parameters.Add("@BrokerAccountNo", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt210.BrokerAccountNo) ? string.Empty : mt210.BrokerAccountNo;
                comm.Parameters.Add("@RelatedOrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt210.RelatedOrderRef;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.SmallDateTime).Value = mt210.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mt210.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = mt210.Ccy;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt210.OrderSwiftCode) ? string.Empty : mt210.OrderSwiftCode;
                comm.Parameters.Add("@BenSubBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt210.BenSubBankSwift) ? string.Empty : mt210.BenSubBankSwift;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt210.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT210 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt210.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT210_StoredProc}");
                return $"Error fetching the MT210 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT92 Stored Procedure.
        /// </summary>
        /// <param name="mT292"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT292(MT292 mT292, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mT292.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mT292.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mT292.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mT292.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mT292.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mT292.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mT292.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mT292.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT292.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT292 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mT292.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT292 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT299 Stored Procedure.
        /// </summary>
        /// <param name="mt299"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT299(MT299 mt299, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt299.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt299.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt299.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt299.OrderRelatedRef) ? string.Empty : mt299.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt299.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt299.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT299.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT299 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt299.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT299 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT380 Stored Procedure.
        /// </summary>
        /// <param name="mt380"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT380(MT380 mt380, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT380_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt380.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt380.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt380.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt380.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt380.PreviousOrderRef) ? string.Empty : mt380.PreviousOrderRef;
                comm.Parameters.Add("@LinkMessageType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt380.LinkMessageType) ? string.Empty : mt380.LinkMessageType;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt380.TradeType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 30).Value = mt380.SafeKeepingAccount;
                comm.Parameters.Add("@BuySell", DataType.SqlDbType.Bit).Value = mt380.BuySell.ToUpper() == "BUY" ? true : false;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt380.TradeDate;
                comm.Parameters.Add("@SettlementDate", DataType.SqlDbType.DateTime).Value = mt380.SettlementDate;
                comm.Parameters.Add("@OrderAmount", DataType.SqlDbType.Money).Value = mt380.OrderAmount;
                comm.Parameters.Add("@OrderCurrency", DataType.SqlDbType.NVarChar, 3).Value = mt380.OrderCcy;
                comm.Parameters.Add("@CounterCurrency", DataType.SqlDbType.NVarChar, 3).Value = mt380.CounterCcy;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar, 35).Value = mt380.Message;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT380 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt380.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT380_StoredProc}");
                return $"Error fetching the MT380 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT381 Stored Procedure.
        /// </summary>
        /// <param name="mt381"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT381(MT381 mt381, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT381_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt381.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt381.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt381.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt381.PreviousOrderRef) ? string.Empty : mt381.PreviousOrderRef;
                comm.Parameters.Add("@LinkMessageType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt381.LinkMessageType) ? string.Empty : mt381.LinkMessageType;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt381.TradeType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 30).Value = mt381.SafeKeepingAccount;
                comm.Parameters.Add("@BuySell", DataType.SqlDbType.Bit).Value = mt381.BuySell.ToUpper() == "BUY" ? true : false;
                comm.Parameters.Add("@OrderDateAndTime", DataType.SqlDbType.DateTime).Value = mt381.OrderDateAndTime;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt381.TradeDate;
                comm.Parameters.Add("@SettlementDate", DataType.SqlDbType.DateTime).Value = mt381.SettlementDate;
                comm.Parameters.Add("@OrderAmount", DataType.SqlDbType.Money).Value = mt381.OrderAmount;
                comm.Parameters.Add("@OrderCurrency", DataType.SqlDbType.NVarChar, 3).Value = mt381.OrderCcy;
                comm.Parameters.Add("@ExchangeRate", DataType.SqlDbType.Float).Value = mt381.ExchangeRate;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar, 35).Value = mt381.Message;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT381 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt381.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT381_StoredProc}");
                return $"Error fetching the MT381 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT392 Stored Procedure.
        /// </summary>
        /// <param name="mt392"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT392(MT392 mt392, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt392.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt392.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mt392.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mt392.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt392.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mt392.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt392.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt392.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT392.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT392 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt392.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT392 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT99 Stored Procedure.
        /// </summary>
        /// <param name="mt399"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT399(MT399 mt399, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt399.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt399.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt399.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt399.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt399.OrderRelatedRef) ? string.Empty : mt399.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt399.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt399.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT399.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT399 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt399.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT399 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT92 Stored Procedure.
        /// </summary>
        /// <param name="mt492"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT492(MT492 mt492, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt492.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt492.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mt492.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mt492.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt492.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mt492.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt492.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt492.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT492.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT492 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt492.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT392 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT99 Stored Procedure.
        /// </summary>
        /// <param name="mt499"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT499(MT499 mt499, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt499.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt499.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt499.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt499.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt499.OrderRelatedRef) ? string.Empty : mt499.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt499.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt499.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT499.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT499 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt499.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT499 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT502 Stored Procedure.
        /// </summary>
        /// <param name="mt502"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT502(MT502 mt502, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT502_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt502.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt502.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt502.OrderRef;
                comm.Parameters.Add("@BrokerAccountNumber", DataType.SqlDbType.NVarChar, 30).Value = mt502.BrokerAccountNumber;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 5).Value = mt502.TradeType;
                comm.Parameters.Add("@CancelOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt502.CancelOrderRef) ? (object)DBNull.Value : mt502.CancelOrderRef;
                comm.Parameters.Add("@IndicatorA", DataType.SqlDbType.NVarChar, 5).Value = mt502.IndicatorA;
                comm.Parameters.Add("@IndicatorB", DataType.SqlDbType.NVarChar, 5).Value = mt502.IndicatorB;
                comm.Parameters.Add("@BuySell", DataType.SqlDbType.Bit).Value = mt502.BuySell.ToUpper() == "BUY" ? true : false;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt502.Quantity;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mt502.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = mt502.Ccy;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt502.InstrumentISIN) ? (object)DBNull.Value : mt502.InstrumentISIN;
                comm.Parameters.Add("@PlaceIndicatorA", DataType.SqlDbType.NVarChar, 5).Value = mt502.PlaceIndicatorA;
                comm.Parameters.Add("@PlaceIndicatorB", DataType.SqlDbType.NVarChar, 5).Value = mt502.PlaceIndicatorB;
                comm.Parameters.Add("@PlaceIndicator", DataType.SqlDbType.NVarChar, 50).Value = mt502.PlaceIndicator;
                comm.Parameters.Add("@OrderType", DataType.SqlDbType.NVarChar, 5).Value = mt502.OrderType;
                comm.Parameters.Add("@OrderTimeLimit", DataType.SqlDbType.NVarChar, 5).Value = mt502.OrderTimeLimit;
                comm.Parameters.Add("@OrderPayType", DataType.SqlDbType.NVarChar, 5).Value = mt502.OrderPayType;
                comm.Parameters.Add("@OrderSettlementType", DataType.SqlDbType.NVarChar, 5).Value = mt502.OrderSettlementType;
                comm.Parameters.Add("@OrderSettlementCondition", DataType.SqlDbType.NVarChar, 5).Value = mt502.OrderSettlementCondition;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT502 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt502.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT502_StoredProc}");
                return $"Error fetching the MT502 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT508 Stored Procedure.
        /// </summary>
        /// <param name="mt508"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT508(MT508 mt508, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT508_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt508.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 16).Value = mt508.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt508.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt508.PreviousOrderRef) ? (object)DBNull.Value : mt508.PreviousOrderRef;
                comm.Parameters.Add("@LinkedMessageType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt508.LinkedMessageType) ? (object)DBNull.Value : mt508.LinkedMessageType;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt508.TradeType;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt508.InstrumentISIN;
                comm.Parameters.Add("@InstrumentDescription", DataType.SqlDbType.NVarChar, 35).Value = mt508.InstrumentDescription;
                comm.Parameters.Add("@QuantityType", DataType.SqlDbType.NVarChar, 5).Value = mt508.QuantityType;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt508.Quantity;
                comm.Parameters.Add("@TransactionSafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt508.TransactionSafeKeepingAccountType) ? (object)DBNull.Value : mt508.TransactionSafeKeepingAccountType;
                comm.Parameters.Add("@TransactionSafeKeepingAccount", DataType.SqlDbType.NVarChar, 35).Value = mt508.TransactionSafeKeepingAccount;
                comm.Parameters.Add("@SettlementDate", DataType.SqlDbType.DateTime).Value = mt508.SettlementDate;
                comm.Parameters.Add("@FromBalanceIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt508.FromBalanceIndicator;
                comm.Parameters.Add("@ToBalanceIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt508.ToBalanceIndicator;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt508.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT508 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt508.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT508_StoredProc}");
                return $"Error fetching the MT508 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT509 Stored Procedure.
        /// </summary>
        /// <param name="mt509"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT509(MT509 mt509, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT509_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt509.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt509.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt509.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt509.PreviousOrderRef) ? (object)DBNull.Value : mt509.PreviousOrderRef;
                comm.Parameters.Add("@LinkedMessageType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt509.LinkedMessageType) ? (object)DBNull.Value : mt509.LinkedMessageType;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt509.TradeType;
                comm.Parameters.Add("@StatusCodeType", DataType.SqlDbType.NVarChar, 5).Value = mt509.StatusCodeType;
                comm.Parameters.Add("@StatusCode", DataType.SqlDbType.NVarChar, 5).Value = mt509.StatusCode;
                comm.Parameters.Add("@ReasonCodeType", DataType.SqlDbType.NVarChar, 5).Value = mt509.ReasonCodeType;
                comm.Parameters.Add("@ReasonCode", DataType.SqlDbType.NVarChar, 5).Value = mt509.ReasonCode;
                comm.Parameters.Add("@Reason", DataType.SqlDbType.NVarChar, 35).Value = mt509.Reason;
                comm.Parameters.Add("@BuySellIndicator", DataType.SqlDbType.NVarChar, 5).Value = mt509.BuySellIndicator;
                comm.Parameters.Add("@PaymentIndicator", DataType.SqlDbType.NVarChar, 5).Value = mt509.PaymentIndicator;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt509.InstrumentISIN;
                comm.Parameters.Add("@QuantityCCY", DataType.SqlDbType.NVarChar, 3).Value = mt509.QuantityCCY;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt509.Quantity;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt509.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT509 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt509.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT509_StoredProc}");
                return $"Error fetching the MT509 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT515 Stored Procedure.
        /// </summary>
        /// <param name="mt515"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT515(MT515 mt515, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT515_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt515.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt515.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt515.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt515.PreviousOrderRef) ? (object)DBNull.Value : mt515.PreviousOrderRef;
                comm.Parameters.Add("@LinkedMessageType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt515.LinkedMessageType) ? (object)DBNull.Value : mt515.LinkedMessageType;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt515.TradeType;
                comm.Parameters.Add("@TransactionTypeIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt515.TransactionTypeIndicator;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt515.InstrumentISIN;
                comm.Parameters.Add("@InstrumentDescription", DataType.SqlDbType.NVarChar, 35).Value = mt515.InstrumentDescription;
                comm.Parameters.Add("@DealPrice", DataType.SqlDbType.Float).Value = mt515.DealPrice;
                comm.Parameters.Add("@DealType", DataType.SqlDbType.NVarChar, 4).Value = mt515.DealType;
                comm.Parameters.Add("@DealCcy", DataType.SqlDbType.NVarChar, 3).Value = mt515.DealCcy;
                comm.Parameters.Add("@QuantityType", DataType.SqlDbType.NVarChar, 5).Value = mt515.QuantityType;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt515.Quantity;
                comm.Parameters.Add("@TransactionSafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt515.TransactionSafeKeepingAccountType) ? (object)DBNull.Value : mt515.TransactionSafeKeepingAccountType;
                comm.Parameters.Add("@TransactionSafeKeepingAccount", DataType.SqlDbType.NVarChar, 35).Value = mt515.TransactionSafeKeepingAccount;
                comm.Parameters.Add("@TransactionIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt515.TransactionIndicator;
                comm.Parameters.Add("@BuySellIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt515.BuySellIndicator;
                comm.Parameters.Add("@PaymentIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt515.PaymentIndicator;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt515.TradeDate;
                comm.Parameters.Add("@SettlementDate", DataType.SqlDbType.DateTime).Value = mt515.SettlementDate;
                comm.Parameters.Add("@BuyerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt515.BuyerSwiftCode) ? (object)DBNull.Value : mt515.BuyerSwiftCode;
                comm.Parameters.Add("@SellerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt515.SellerSwiftCode) ? (object)DBNull.Value : mt515.SellerSwiftCode;
                comm.Parameters.Add("@TradeAmount", DataType.SqlDbType.Money).Value = mt515.TradeAmount;
                comm.Parameters.Add("@TradeAmountCCY", DataType.SqlDbType.NVarChar, 3).Value = mt515.TradeAmountCCY;
                comm.Parameters.Add("@BrokerCommsAmount", DataType.SqlDbType.Money).Value = mt515.BrokerCommsAmount;
                comm.Parameters.Add("@BrokerCommsAmountCCY", DataType.SqlDbType.NVarChar, 3).Value = mt515.BrokerCommsAmountCCY;
                comm.Parameters.Add("@AccruedInterestAmount", DataType.SqlDbType.Money).Value = mt515.AccruedInterestAmount;
                comm.Parameters.Add("@AccruedInterestAmountCCY", DataType.SqlDbType.NVarChar, 3).Value = mt515.AccruedInterestAmountCCY;
                comm.Parameters.Add("@SettlementAmount", DataType.SqlDbType.Money).Value = mt515.SettlementAmount;
                comm.Parameters.Add("@SettlementAmountCCY", DataType.SqlDbType.NVarChar, 3).Value = mt515.SettlementAmountCCY;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt515.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT515 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt515.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT515_StoredProc}");
                return $"Error fetching the MT515 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT535 Stored Procedure.
        /// </summary>
        /// <param name="mt535"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT535(MT535 mt535, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT535_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt535.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt535.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt535.OrderRef;
                comm.Parameters.Add("@RelatedRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt535.RelatedRef) ? (object)DBNull.Value : mt535.RelatedRef;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt535.TradeType;
                comm.Parameters.Add("@PageNumber", DataType.SqlDbType.NVarChar, 5).Value = mt535.PageNumber;
                comm.Parameters.Add("@StatementSequenceType", DataType.SqlDbType.NVarChar, 4).Value = mt535.StatementSequenceType;
                comm.Parameters.Add("@StatementDate", DataType.SqlDbType.DateTime).Value = mt535.StatementDate;
                comm.Parameters.Add("@StatementFrequencyIndicatorCode", DataType.SqlDbType.NVarChar, 4).Value = mt535.StatementFrequencyIndicatorCode;
                comm.Parameters.Add("@UpdatesIndicatorCode", DataType.SqlDbType.NVarChar, 4).Value = mt535.UpdatesIndicatorCode;
                comm.Parameters.Add("@StatementTypeCode", DataType.SqlDbType.NVarChar, 4).Value = mt535.StatementTypeCode;
                comm.Parameters.Add("@StatementBasisCode", DataType.SqlDbType.NVarChar, 4).Value = mt535.StatementBasisCode;
                comm.Parameters.Add("@SafekeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt535.SafekeepingAccountType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 35).Value = mt535.SafekeepingAccount;
                comm.Parameters.Add("@ActivityFlag", DataType.SqlDbType.NVarChar, 1).Value = mt535.ActivityFlag;
                comm.Parameters.Add("@SubSafekeepingStatementFlag", DataType.SqlDbType.NVarChar, 1).Value = mt535.SubSafekeepingStatementFlag;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt535.InstrumentISIN;
                comm.Parameters.Add("@InstrumentDescription", DataType.SqlDbType.NVarChar, 35).Value = mt535.InstrumentDescription;
                comm.Parameters.Add("@DealPrice", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt535.DealPrice.ToString()) ? (object)DBNull.Value : mt535.DealPrice;
                comm.Parameters.Add("@DealType", DataType.SqlDbType.NVarChar, 4).Value = mt535.DealType;
                comm.Parameters.Add("@DealCcy", DataType.SqlDbType.NVarChar, 3).Value = mt535.DealCcy;
                comm.Parameters.Add("@AggregateBalanceType", DataType.SqlDbType.NVarChar, 5).Value = mt535.AggregateBalanceType;
                comm.Parameters.Add("@AggregateBalance", DataType.SqlDbType.Money).Value = mt535.AggregateBalance;
                comm.Parameters.Add("@AggregateBalanceSafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt535.AggregateBalanceSafeKeepingAccountType;
                comm.Parameters.Add("@AggregateBalanceSafeKeepingCode", DataType.SqlDbType.NVarChar, 4).Value = mt535.AggregateBalanceSafeKeepingCode;
                comm.Parameters.Add("@AggregateBalanceSafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt535.AggregateBalanceSafeKeepingSwiftCode;
                comm.Parameters.Add("@HoldingAmount", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt535.HoldingAmount.ToString()) ? (object)DBNull.Value : mt535.HoldingAmount;
                comm.Parameters.Add("@HoldingAmountCCY", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt535.HoldingAmountCCY) ? (object)DBNull.Value : mt535.HoldingAmountCCY;
                comm.Parameters.Add("@ExchangeRate", DataType.SqlDbType.Money).Value = mt535.ExchangeRate;
                comm.Parameters.Add("@TotalHoldingsValueofPage", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt535.TotalHoldingsValueofPage.ToString()) ? (object)DBNull.Value : mt535.TotalHoldingsValueofPage;
                comm.Parameters.Add("@TotalHoldingsValueofPageCCY", DataType.SqlDbType.NVarChar, 5).Value = string.IsNullOrEmpty(mt535.HoldingAmountCCY) ? (object)DBNull.Value : mt535.HoldingAmountCCY;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt535.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT535 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt535.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT535_StoredProc}");
                return $"Error fetching the MT535 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT536 Stored Procedure.
        /// </summary>
        /// <param name="mt536"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT536(MT536 mt536, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT536_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt536.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt536.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt536.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt536.PreviousOrderRef) ? (object)DBNull.Value : mt536.PreviousOrderRef;
                comm.Parameters.Add("@LinkedMessageType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt536.LinkedMessageType) ? (object)DBNull.Value : mt536.LinkedMessageType;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt536.AccountNumber) ? (object)DBNull.Value : mt536.AccountNumber;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt536.TradeType;
                comm.Parameters.Add("@PageNumber", DataType.SqlDbType.NVarChar, 5).Value = mt536.PageNumber;
                comm.Parameters.Add("@StatementSequenceType", DataType.SqlDbType.NVarChar, 4).Value = mt536.StatementSequenceType;
                comm.Parameters.Add("@StatementDate", DataType.SqlDbType.DateTime).Value = mt536.StatementDate;
                comm.Parameters.Add("@StatementFrequencyIndicatorCode", DataType.SqlDbType.NVarChar, 4).Value = mt536.StatementFrequencyIndicatorCode;
                comm.Parameters.Add("@UpdatesIndicatorCode", DataType.SqlDbType.NVarChar, 4).Value = mt536.UpdatesIndicatorCode;
                comm.Parameters.Add("@StatementBasisCode", DataType.SqlDbType.NVarChar, 4).Value = mt536.StatementBasisCode;
                comm.Parameters.Add("@SafekeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt536.SafekeepingAccountType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 35).Value = mt536.SafekeepingAccount;
                comm.Parameters.Add("@ActivityFlag", DataType.SqlDbType.NVarChar, 1).Value = mt536.ActivityFlag;
                comm.Parameters.Add("@SubSafekeepingStatementFlag", DataType.SqlDbType.NVarChar, 1).Value = mt536.SubSafekeepingStatementFlag;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt536.InstrumentISIN;
                comm.Parameters.Add("@InstrumentDescription", DataType.SqlDbType.NVarChar, 35).Value = mt536.InstrumentDescription;
                comm.Parameters.Add("@DealPrice", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt536.DealPrice.ToString()) ? (object)DBNull.Value : mt536.DealPrice;
                comm.Parameters.Add("@DealType", DataType.SqlDbType.NVarChar, 4).Value = mt536.DealType;
                comm.Parameters.Add("@DealCcy", DataType.SqlDbType.NVarChar, 3).Value = string.IsNullOrEmpty(mt536.DealCcy) ? (object)DBNull.Value : mt536.DealCcy;
                comm.Parameters.Add("@PostingQuantityType", DataType.SqlDbType.NVarChar, 5).Value = mt536.PostingQuantityType;
                comm.Parameters.Add("@PostingQuantity", DataType.SqlDbType.Money).Value = mt536.PostingQuantity;
                comm.Parameters.Add("@TransactionSafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt536.TransactionSafeKeepingAccountType) ? (object)DBNull.Value : mt536.TransactionSafeKeepingAccountType;
                comm.Parameters.Add("@TransactionSafeKeepingCode", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt536.TransactionSafeKeepingCode) ? (object)DBNull.Value : mt536.TransactionSafeKeepingCode;
                comm.Parameters.Add("@TransactionSafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt536.TransactionSafeKeepingSwiftCode) ? (object)DBNull.Value : mt536.TransactionSafeKeepingSwiftCode;
                comm.Parameters.Add("@TransactionIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt536.TransactionIndicator;
                comm.Parameters.Add("@DeliveryIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt536.DeliveryIndicator;
                comm.Parameters.Add("@PaymentIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt536.PaymentIndicator;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt536.TradeDate;
                comm.Parameters.Add("@SettlementDate", DataType.SqlDbType.DateTime).Value = mt536.SettlementDate;
                comm.Parameters.Add("@EffectiveSettlementDate", DataType.SqlDbType.DateTime).Value = mt536.EffectiveSettlementDate;
                comm.Parameters.Add("@IsReceivingAgent", DataType.SqlDbType.Bit).Value = mt536.IsReceivingAgent;
                comm.Parameters.Add("@AgentSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt536.AgentSwiftCode;
                comm.Parameters.Add("@BuyerSellerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt536.BuyerSellerSwiftCode;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt536.SettlementSwiftCode) ? (object)DBNull.Value : mt536.SettlementSwiftCode;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt536.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT536 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt536.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT536_StoredProc}");
                return $"Error fetching the MT536 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT537 Stored Procedure.
        /// </summary>
        /// <param name="mt537"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT537(MT537 mt537, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT537_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt537.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt537.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt537.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt537.PreviousOrderRef) ? (object)DBNull.Value : mt537.PreviousOrderRef;
                comm.Parameters.Add("@LinkedMessageType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt537.LinkedMessageType) ? (object)DBNull.Value : mt537.LinkedMessageType;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt537.AccountNumber) ? (object)DBNull.Value : mt537.AccountNumber;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt537.TradeType;
                comm.Parameters.Add("@PageNumber", DataType.SqlDbType.NVarChar, 5).Value = mt537.PageNumber;
                comm.Parameters.Add("@StatementSequenceType", DataType.SqlDbType.NVarChar, 4).Value = mt537.StatementSequenceType;
                comm.Parameters.Add("@StatementDate", DataType.SqlDbType.DateTime).Value = mt537.StatementDate;
                comm.Parameters.Add("@StatementFrequencyIndicatorCode", DataType.SqlDbType.NVarChar, 4).Value = mt537.StatementFrequencyIndicatorCode;
                comm.Parameters.Add("@UpdatesIndicatorCode", DataType.SqlDbType.NVarChar, 4).Value = mt537.UpdatesIndicatorCode;
                comm.Parameters.Add("@StatementStructureCode", DataType.SqlDbType.NVarChar, 4).Value = mt537.StatementStructureCode;
                comm.Parameters.Add("@SafekeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt537.SafekeepingAccountType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 35).Value = mt537.SafekeepingAccount;
                comm.Parameters.Add("@ActivityFlag", DataType.SqlDbType.NVarChar, 1).Value = mt537.ActivityFlag;
                comm.Parameters.Add("@StatusCodeType", DataType.SqlDbType.NVarChar, 5).Value = mt537.StatusCodeType;
                comm.Parameters.Add("@StatusCode", DataType.SqlDbType.NVarChar, 5).Value = mt537.StatusCode;
                comm.Parameters.Add("@ReasonCodeType", DataType.SqlDbType.NVarChar, 5).Value = mt537.ReasonCodeType;
                comm.Parameters.Add("@ReasonCode", DataType.SqlDbType.NVarChar, 5).Value = mt537.ReasonCode;
                comm.Parameters.Add("@Reason", DataType.SqlDbType.NVarChar, 35).Value = mt537.Reason;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt537.InstrumentISIN;
                comm.Parameters.Add("@InstrumentDescription", DataType.SqlDbType.NVarChar, 35).Value = mt537.InstrumentDescription;
                comm.Parameters.Add("@PostingQuantityType", DataType.SqlDbType.NVarChar, 5).Value = mt537.PostingQuantityType;
                comm.Parameters.Add("@PostingQuantity", DataType.SqlDbType.Money).Value = mt537.PostingQuantity;
                comm.Parameters.Add("@TransactionSafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt537.TransactionSafeKeepingAccountType) ? (object)DBNull.Value : mt537.TransactionSafeKeepingAccountType;
                comm.Parameters.Add("@TransactionSafeKeepingCode", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt537.TransactionSafeKeepingCode) ? (object)DBNull.Value : mt537.TransactionSafeKeepingCode;
                comm.Parameters.Add("@TransactionSafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt537.TransactionSafeKeepingSwiftCode) ? (object)DBNull.Value : mt537.TransactionSafeKeepingSwiftCode;
                comm.Parameters.Add("@TransactionIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt537.TransactionIndicator;
                comm.Parameters.Add("@DeliveryIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt537.DeliveryIndicator;
                comm.Parameters.Add("@PaymentIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt537.PaymentIndicator;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt537.TradeDate;
                comm.Parameters.Add("@SettlementDate", DataType.SqlDbType.DateTime).Value = mt537.SettlementDate;
                comm.Parameters.Add("@IsReceivingAgent", DataType.SqlDbType.Bit).Value = mt537.IsReceivingAgent;
                comm.Parameters.Add("@AgentSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt537.AgentSwiftCode) ? (object)DBNull.Value : mt537.AgentSwiftCode;
                comm.Parameters.Add("@AgentCodeAndID", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt537.AgentCodeAndID) ? (object)DBNull.Value : mt537.AgentCodeAndID;
                comm.Parameters.Add("@BuyerSellerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt537.BuyerSellerSwiftCode) ? (object)DBNull.Value : mt537.BuyerSellerSwiftCode;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt537.SettlementSwiftCode) ? (object)DBNull.Value : mt537.SettlementSwiftCode;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt537.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT537 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt537.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT537_StoredProc}");
                return $"Error fetching the MT537 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT540 Stored Procedure.
        /// </summary>
        /// <param name="mt540"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT540(MT540 mt540, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT540_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt540.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt540.BrokerSwiftCode;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt540.TradeType;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt540.OrderRef;
                comm.Parameters.Add("@CancelOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt540.CancelOrderRef) ? (object)DBNull.Value : mt540.CancelOrderRef;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt540.TradeDate;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.DateTime).Value = mt540.SettleDate;
                comm.Parameters.Add("@IsEquity", DataType.SqlDbType.Bit).Value = mt540.IsEquity;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mt540.Amount;
                comm.Parameters.Add("@BrokerAccountNumber", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt540.BrokerAccountNumber) ? (object)DBNull.Value : mt540.BrokerAccountNumber;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt540.InstrumentISIN;
                comm.Parameters.Add("@SafeKeepingPlaceCode", DataType.SqlDbType.NVarChar, 4).Value = mt540.SafeKeepingPlaceCode;
                comm.Parameters.Add("@SafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt540.SafeKeepingSwiftCode) ? (object)DBNull.Value : mt540.SafeKeepingSwiftCode;
                comm.Parameters.Add("@AgentSwiftCode", DataType.SqlDbType.NVarChar, 12).Value = string.IsNullOrEmpty(mt540.AgentSwiftCode) ? (object)DBNull.Value : mt540.AgentSwiftCode;
                comm.Parameters.Add("@AgentCodeAndID", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt540.AgentCodeAndID) ? (object)DBNull.Value : mt540.AgentCodeAndID;
                comm.Parameters.Add("@AgentID", DataType.SqlDbType.NVarChar, 12).Value = string.IsNullOrEmpty(mt540.AgentID) ? (object)DBNull.Value : mt540.AgentID;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 12).Value = string.IsNullOrEmpty(mt540.SettlementSwiftCode) ? (object)DBNull.Value : mt540.SettlementSwiftCode;
                comm.Parameters.Add("@Instructions", DataType.SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(mt540.Instructions) ? (object)DBNull.Value : mt540.Instructions;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT540 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt540.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT540_StoredProc}");
                return $"Error fetching the MT540 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT541 Stored Procedure.
        /// </summary>
        /// <param name="mt541"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT541(MT541 mt541, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT541_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt541.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt541.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt541.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt541.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt541.PreviousOrderRef) ? (object)DBNull.Value : mt541.PreviousOrderRef;
                comm.Parameters.Add("@LinkedMessageType", DataType.SqlDbType.NVarChar, 3).Value = string.IsNullOrEmpty(mt541.LinkedMessageType) ? (object)DBNull.Value : mt541.LinkedMessageType;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt541.SwiftUrgent;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt541.TradeType;
                comm.Parameters.Add("@PreparationDate", DataType.SqlDbType.DateTime).Value = string.IsNullOrEmpty(mt541.PreparationDate.ToString()) ? (object)DBNull.Value : mt541.PreparationDate;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt541.TradeDate;
                comm.Parameters.Add("@SettlementDate", DataType.SqlDbType.DateTime).Value = mt541.SettleDate;
                if (mt541.DealPrice > 0)
                {
                    comm.Parameters.Add("@DealPrice", DataType.SqlDbType.Money).Value = mt541.DealPrice;
                }
                if (mt541.DealPerc > 0)
                {
                    comm.Parameters.Add("@DealPerc", DataType.SqlDbType.Money).Value = mt541.DealPerc;
                }
                comm.Parameters.Add("@DealCcy", DataType.SqlDbType.NVarChar, 3).Value = string.IsNullOrEmpty(mt541.DealCcy) ? (object)DBNull.Value : mt541.DealCcy;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt541.InstrumentISIN) ? (object)DBNull.Value : mt541.InstrumentISIN;
                comm.Parameters.Add("@QuantityType", DataType.SqlDbType.NVarChar, 4).Value = mt541.QuantityType;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt541.Quantity;
                comm.Parameters.Add("@SafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt541.SafeKeepingAccountType;
                comm.Parameters.Add("@SafeKeepingAccount", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt541.SafeKeepingAccount) ? (object)DBNull.Value : mt541.SafeKeepingAccount;
                comm.Parameters.Add("@SafeKeepingPlaceCode", DataType.SqlDbType.NVarChar, 4).Value = mt541.SafeKeepingPlaceCode;
                comm.Parameters.Add("@SafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt541.SafeKeepingSwiftCode) ? (object)DBNull.Value : mt541.SafeKeepingSwiftCode;
                comm.Parameters.Add("@BuyerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt541.BuyerSwiftCode) ? (object)DBNull.Value : mt541.BuyerSwiftCode;
                comm.Parameters.Add("@BuyerName", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt541.BuyerName) ? (object)DBNull.Value : mt541.BuyerName;
                comm.Parameters.Add("@BuyerAddress", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt541.BuyerAddress) ? (object)DBNull.Value : mt541.BuyerAddress;
                comm.Parameters.Add("@SellerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt541.SellerSwiftCode) ? (object)DBNull.Value : mt541.SellerSwiftCode;
                comm.Parameters.Add("@SellerName", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt541.SellerName) ? (object)DBNull.Value : mt541.SellerName;
                comm.Parameters.Add("@SellerAddress", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt541.SellerAddress) ? (object)DBNull.Value : mt541.SellerAddress;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt541.SettlementSwiftCode) ? (object)DBNull.Value : mt541.SettlementSwiftCode;
                comm.Parameters.Add("@SettlementTransactionType", DataType.SqlDbType.NVarChar, 4).Value = mt541.SettlementTransactionType;
                comm.Parameters.Add("@SettlementTransactionIndicator", DataType.SqlDbType.NVarChar, 50).Value = mt541.SettlementTransactionIndicator;
                comm.Parameters.Add("@AgentCodeAndID", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt541.AgentCodeAndID) ? (object)DBNull.Value : mt541.AgentCodeAndID;
                comm.Parameters.Add("@AgentID", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt541.AgentID) ? (object)DBNull.Value : mt541.AgentID;
                comm.Parameters.Add("@SettlementAmountType", DataType.SqlDbType.NVarChar, 4).Value = mt541.SettlementAmountType;
                comm.Parameters.Add("@SettlementAmount", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt541.SettlementAmount.ToString()) ? (object)DBNull.Value : mt541.SettlementAmount;
                comm.Parameters.Add("@SettlementCcy", DataType.SqlDbType.NVarChar, 3).Value = string.IsNullOrEmpty(mt541.SettlementCcy) ? (object)DBNull.Value : mt541.SettlementCcy;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT541 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt541.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT541_StoredProc}");
                return $"Error fetching the MT541 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT542 Stored Procedure.
        /// </summary>
        /// <param name="mt542"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT542(MT542 mt542, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT542_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt542.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt542.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt542.BrokerSwiftCode;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt542.TradeType;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt542.OrderRef;
                comm.Parameters.Add("@CancelOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt542.CancelOrderRef) ? (object)DBNull.Value : mt542.CancelOrderRef;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt542.TradeDate;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.DateTime).Value = mt542.SettleDate;
                comm.Parameters.Add("@IsEquity", DataType.SqlDbType.Bit).Value = mt542.IsEquity;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mt542.Amount;
                comm.Parameters.Add("@BrokerAccountNumber", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt542.BrokerAccountNo) ? (object)DBNull.Value : mt542.BrokerAccountNo;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt542.InstrumentISIN;
                comm.Parameters.Add("@SafeKeepingPlaceCode", DataType.SqlDbType.NVarChar, 4).Value = mt542.SafekeepingPlaceCode;
                comm.Parameters.Add("@SafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt542.SafekeepingSwiftCode) ? (object)DBNull.Value : mt542.SafekeepingSwiftCode;
                comm.Parameters.Add("@AgentSwiftCode", DataType.SqlDbType.NVarChar, 12).Value = string.IsNullOrEmpty(mt542.AgentSwiftCode) ? (object)DBNull.Value : mt542.AgentSwiftCode;
                comm.Parameters.Add("@AgentCodeAndID", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt542.AgentCodeAndID) ? (object)DBNull.Value : mt542.AgentCodeAndID;
                comm.Parameters.Add("@AgentID", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt542.AgentID) ? (object)DBNull.Value : mt542.AgentID;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 12).Value = string.IsNullOrEmpty(mt542.SettlementSwiftCode) ? (object)DBNull.Value : mt542.SettlementSwiftCode;
                comm.Parameters.Add("@Instructions", DataType.SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(mt542.Instructions) ? (object)DBNull.Value : mt542.Instructions;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT542 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt542.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT542_StoredProc}");
                return $"Error fetching the MT542 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT543 Stored Procedure.
        /// </summary>
        /// <param name="mt543"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT543(MT543 mt543, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT543_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt543.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt543.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt543.OrderRef;
                comm.Parameters.Add("@PreviousOrderRef", DataType.SqlDbType.NVarChar, 16).Value = string.IsNullOrEmpty(mt543.PreviousOrderRef) ? (object)DBNull.Value : mt543.PreviousOrderRef;
                comm.Parameters.Add("@LinkedMessageType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt543.LinkedMessageType) ? (object)DBNull.Value : mt543.LinkedMessageType;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt543.SwiftUrgent;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt543.TradeType;
                comm.Parameters.Add("@PreparationDate", DataType.SqlDbType.DateTime).Value = mt543.PreparationDate;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt543.TradeDate;
                comm.Parameters.Add("@SettlementDate", DataType.SqlDbType.DateTime).Value = mt543.SettlementDate;
                if (mt543.DealPrice > 0)
                {
                    comm.Parameters.Add("@DealPrice", DataType.SqlDbType.Money).Value = mt543.DealPrice;
                }
                if (mt543.DealPerc > 0)
                {
                    comm.Parameters.Add("@DealPerc", DataType.SqlDbType.Money).Value = mt543.DealPerc;
                }
                comm.Parameters.Add("@DealCcy", DataType.SqlDbType.NVarChar, 3).Value = string.IsNullOrEmpty(mt543.DealCcy) ? (object)DBNull.Value : mt543.DealCcy;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt543.InstrumentISIN) ? (object)DBNull.Value : mt543.InstrumentISIN;
                comm.Parameters.Add("@QuantityType", DataType.SqlDbType.NVarChar, 4).Value = mt543.QuantityType;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt543.Quantity;
                comm.Parameters.Add("@SafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt543.SafeKeepingAccountType;
                comm.Parameters.Add("@SafeKeepingAccount", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt543.SafeKeepingAccount) ? (object)DBNull.Value : mt543.SafeKeepingAccount;
                comm.Parameters.Add("@SafeKeepingPlaceCode", DataType.SqlDbType.NVarChar, 4).Value = mt543.SafeKeepingPlaceCode;
                comm.Parameters.Add("@SafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt543.SafeKeepingSwiftCode) ? (object)DBNull.Value : mt543.SafeKeepingSwiftCode;
                comm.Parameters.Add("@BuyerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt543.BuyerSwiftCode) ? (object)DBNull.Value : mt543.BuyerSwiftCode;
                comm.Parameters.Add("@BuyerName", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt543.BuyerName) ? (object)DBNull.Value : mt543.BuyerName;
                comm.Parameters.Add("@BuyerAddress", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt543.BuyerAddress) ? (object)DBNull.Value : mt543.BuyerAddress;
                comm.Parameters.Add("@SellerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt543.SellerSwiftCode) ? (object)DBNull.Value : mt543.SellerSwiftCode;
                comm.Parameters.Add("@SellerName", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt543.SellerName) ? (object)DBNull.Value : mt543.SellerName;
                comm.Parameters.Add("@SellerAddress", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt543.SellerAddress) ? (object)DBNull.Value : mt543.SellerAddress;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt543.SettlementSwiftCode;
                comm.Parameters.Add("@TransactionIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt543.TransactionIndicator;
                comm.Parameters.Add("@AgentCodeAndID", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt543.AgentCodeAndID) ? (object)DBNull.Value : mt543.AgentCodeAndID;
                comm.Parameters.Add("@AgentID", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt543.AgentID) ? (object)DBNull.Value : mt543.AgentID;
                comm.Parameters.Add("@SettlementAmountType", DataType.SqlDbType.NVarChar, 4).Value = mt543.SettlementAmountType;
                comm.Parameters.Add("@SettlementAmount", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt543.SettlementAmount.ToString()) ? (object)DBNull.Value : mt543.SettlementAmount;
                comm.Parameters.Add("@SettlementCcy", DataType.SqlDbType.NVarChar, 3).Value = string.IsNullOrEmpty(mt543.SettlementCcy.ToString()) ? (object)DBNull.Value : mt543.SettlementCcy;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT543 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt543.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT543_StoredProc}");
                return $"Error fetching the MT543 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT545 Stored Procedure.
        /// </summary>
        /// <param name="mt545"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT545(MT545 mt545, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT545_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt545.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt545.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt545.OrderRef;
                comm.Parameters.Add("@RelatedRef", DataType.SqlDbType.NVarChar, 16).Value = mt545.RelatedRef;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt545.TradeDate;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.DateTime).Value = mt545.SettleDate;
                comm.Parameters.Add("@EffectiveSettleDate", DataType.SqlDbType.DateTime).Value = mt545.EffectiveSettleDate;
                if (mt545.DealPrice > 0)
                {
                    comm.Parameters.Add("@DealPrice", DataType.SqlDbType.Money).Value = mt545.DealPrice;
                }
                if (mt545.DealPerc > 0)
                {
                    comm.Parameters.Add("@DealPerc", DataType.SqlDbType.Money).Value = mt545.DealPerc;
                }
                comm.Parameters.Add("@DealCcy", DataType.SqlDbType.NVarChar, 3).Value = mt545.DealCcy;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt545.InstrumentISIN;
                comm.Parameters.Add("@InstrumentDescription", DataType.SqlDbType.NVarChar, 100).Value = mt545.InstrumentDescription;
                comm.Parameters.Add("@QuantityType", DataType.SqlDbType.NVarChar, 5).Value = mt545.QuantityType;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt545.Quantity;
                comm.Parameters.Add("@SettlementAmount", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt545.SettlementAmount.ToString()) ? (object)DBNull.Value : mt545.SettlementAmount;
                comm.Parameters.Add("@Ccy", DataType.SqlDbType.NVarChar, 3).Value = mt545.Ccy;
                comm.Parameters.Add("@SafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt545.SafekeepingAccountType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 50).Value = mt545.SafekeepingAccount;
                comm.Parameters.Add("@SafeKeepingPlaceCode", DataType.SqlDbType.NVarChar, 4).Value = mt545.SafeKeepingPlaceCode;
                comm.Parameters.Add("@SafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt545.SafeKeepingSwiftCode) ? (object)DBNull.Value : mt545.SafeKeepingSwiftCode;
                comm.Parameters.Add("@TransactionIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt545.TransactionIndicator;
                comm.Parameters.Add("@AgentSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt545.AgentSwiftCode) ? (object)DBNull.Value : mt545.AgentSwiftCode;
                comm.Parameters.Add("@AgentCodeAndID", DataType.SqlDbType.NVarChar, 50).Value = mt545.AgentCodeAndID;
                comm.Parameters.Add("@AgentID", DataType.SqlDbType.NVarChar, 10).Value = mt545.AgentID;
                comm.Parameters.Add("@CptySwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt545.CptySwiftCode;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt545.SettlementSwiftCode;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt545.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT545 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt545.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT545_StoredProc}");
                return $"Error fetching the MT545 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT547 Stored Procedure.
        /// </summary>
        /// <param name="mt547"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT547(MT547 mt547, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT547_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt547.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt547.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt547.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt547.OrderRef;
                comm.Parameters.Add("@RelatedRef", DataType.SqlDbType.NVarChar, 16).Value = mt547.RelatedRef;
                comm.Parameters.Add("@TradeDate", DataType.SqlDbType.DateTime).Value = mt547.TradeDate;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.DateTime).Value = mt547.SettleDate;
                comm.Parameters.Add("@EffectiveSettleDate", DataType.SqlDbType.DateTime).Value = mt547.EffectiveSettleDate;
                if (mt547.DealPrice > 0)
                {
                    comm.Parameters.Add("@DealPrice", DataType.SqlDbType.Money).Value = mt547.DealPrice;
                }
                if (mt547.DealPerc > 0)
                {
                    comm.Parameters.Add("@DealPerc", DataType.SqlDbType.Money).Value = mt547.DealPerc;
                }
                comm.Parameters.Add("@DealCcy", DataType.SqlDbType.NVarChar, 3).Value = mt547.DealCcy;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt547.InstrumentISIN;
                comm.Parameters.Add("@InstrumentDescription", DataType.SqlDbType.NVarChar, 35).Value = mt547.InstrumentDescription;
                comm.Parameters.Add("@QuantityType", DataType.SqlDbType.NVarChar, 5).Value = mt547.QuantityType;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt547.Quantity;
                comm.Parameters.Add("@SettlementAmount", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt547.SettlementAmount.ToString()) ? (object)DBNull.Value : mt547.SettlementAmount;
                comm.Parameters.Add("@Ccy", DataType.SqlDbType.NVarChar, 3).Value = mt547.Ccy;
                comm.Parameters.Add("@SafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt547.SafekeepingAccountType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 50).Value = mt547.SafekeepingAccount;
                comm.Parameters.Add("@SafeKeepingPlaceCode", DataType.SqlDbType.NVarChar, 4).Value = mt547.SafeKeepingPlaceCode;
                comm.Parameters.Add("@SafeKeepingSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt547.SafeKeepingSwiftCode) ? (object)DBNull.Value : mt547.SafeKeepingSwiftCode;
                comm.Parameters.Add("@TransactionIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt547.TransactionIndicator;
                comm.Parameters.Add("@AgentSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt547.AgentSwiftCode) ? (object)DBNull.Value : mt547.AgentSwiftCode;
                comm.Parameters.Add("@AgentCodeAndID", DataType.SqlDbType.NVarChar, 50).Value = mt547.AgentCodeAndID;
                comm.Parameters.Add("@AgentID", DataType.SqlDbType.NVarChar, 10).Value = mt547.AgentID;
                comm.Parameters.Add("@CptySwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt547.CptySwiftCode;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt547.SettlementSwiftCode;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt547.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT547 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt547.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT547_StoredProc}");
                return $"Error fetching the MT547 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT548 Stored Procedure.
        /// </summary>
        /// <param name="mt548"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT548(MT548 mt548, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT548_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt548.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt548.ClientSwiftCode;
                comm.Parameters.Add("@OrderSwiftCode", DataType.SqlDbType.NVarChar, 30).Value = mt548.OrderSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt548.OrderRef;
                comm.Parameters.Add("@RelatedRef", DataType.SqlDbType.NVarChar, 16).Value = mt548.RelatedRef;
                comm.Parameters.Add("@LinkedMessageType", DataType.SqlDbType.NVarChar, 4).Value = mt548.RelatedRef;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt548.TradeType;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.DateTime).Value = mt548.SettleDate;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = mt548.InstrumentISIN;
                comm.Parameters.Add("@InstrumentDescription", DataType.SqlDbType.NVarChar, 100).Value = mt548.InstrumentDescription;
                comm.Parameters.Add("@QuantityType", DataType.SqlDbType.NVarChar, 5).Value = mt548.QuantityType;
                comm.Parameters.Add("@Quantity", DataType.SqlDbType.Money).Value = mt548.Quantity;
                comm.Parameters.Add("@SettlementAmount", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt548.SettlementAmount.ToString()) ? (object)DBNull.Value : mt548.SettlementAmount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = mt548.Ccy;
                comm.Parameters.Add("@StatusCodeType", DataType.SqlDbType.NVarChar, 5).Value = mt548.StatusCodeType;
                comm.Parameters.Add("@StatusCode", DataType.SqlDbType.NVarChar, 5).Value = mt548.StatusCode;
                comm.Parameters.Add("@ReasonCodeType", DataType.SqlDbType.NVarChar, 5).Value = mt548.ReasonCodeType;
                comm.Parameters.Add("@ReasonCode", DataType.SqlDbType.NVarChar, 5).Value = mt548.ReasonCode;
                comm.Parameters.Add("@Reason", DataType.SqlDbType.NVarChar, 35).Value = mt548.Reason;
                comm.Parameters.Add("@SafekeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt548.SafekeepingAccountType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 50).Value = mt548.SafekeepingAccount;
                comm.Parameters.Add("@TransactionIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt548.TransactionIndicator;
                comm.Parameters.Add("@DeliveryIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt548.DeliveryIndicator;
                comm.Parameters.Add("@PaymentIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt548.PaymentIndicator;
                comm.Parameters.Add("@AgentSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt548.AgentSwiftCode) ? (object)DBNull.Value : mt548.AgentSwiftCode;
                comm.Parameters.Add("@AgentCodeAndID", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt548.AgentCodeAndID) ? (object)DBNull.Value : mt548.AgentCodeAndID;
                comm.Parameters.Add("@AgentID", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(mt548.AgentID) ? (object)DBNull.Value : mt548.AgentID;
                comm.Parameters.Add("@SettlementSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt548.SettlementSwiftCode) ? (object)DBNull.Value : mt548.SettlementSwiftCode;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt548.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT548 Swift File ({}).  OrderRef={OrderRef} ClientId={ClientId}", result, mt548.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT548_StoredProc}");
                return $"Error fetching the MT548 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT565 Stored Procedure.
        /// </summary>
        /// <param name="mt565"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT565(MT565 mt565, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT565_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt565.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 11).Value = mt565.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt565.OrderRef;
                comm.Parameters.Add("@CorporateActionRef", DataType.SqlDbType.NVarChar, 16).Value = mt565.CorporateActionRef;
                comm.Parameters.Add("@TradeType", DataType.SqlDbType.NVarChar, 4).Value = mt565.TradeType;
                comm.Parameters.Add("@CorporateActionEventIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt565.CorporateActionEventIndicator;
                comm.Parameters.Add("@RelatedOrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt565.RelatedOrderRef;
                comm.Parameters.Add("@LinkIndicatorType", DataType.SqlDbType.NVarChar, 4).Value = mt565.LinkIndicatorType;
                comm.Parameters.Add("@LinkMessageType", DataType.SqlDbType.NVarChar, 3).Value = mt565.LinkMessageType;
                comm.Parameters.Add("@InstrumentISIN", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(mt565.InstrumentISIN) ? (object)DBNull.Value : mt565.InstrumentISIN;
                comm.Parameters.Add("@InstrumentQtyType", DataType.SqlDbType.NVarChar, 4).Value = string.IsNullOrEmpty(mt565.InstrumentQtyType) ? (object)DBNull.Value : mt565.InstrumentQtyType;
                comm.Parameters.Add("@InstrumentQty", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(mt565.InstrumentQty.ToString()) ? (object)DBNull.Value : mt565.InstrumentQty;
                comm.Parameters.Add("@SafeKeepingAccountType", DataType.SqlDbType.NVarChar, 4).Value = mt565.SafeKeepingAccountType;
                comm.Parameters.Add("@SafekeepingAccount", DataType.SqlDbType.NVarChar, 35).Value = mt565.SafekeepingAccount;
                comm.Parameters.Add("@BeneficiarySwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mt565.BeneficiarySwiftCode) ? (object)DBNull.Value : mt565.BeneficiarySwiftCode;
                comm.Parameters.Add("@BeneficiaryName", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt565.BeneficiaryName) ? (object)DBNull.Value : mt565.BeneficiaryName;
                comm.Parameters.Add("@BeneficiaryAddress", DataType.SqlDbType.NVarChar, 35).Value = string.IsNullOrEmpty(mt565.BeneficiaryAddress) ? (object)DBNull.Value : mt565.BeneficiaryAddress;
                comm.Parameters.Add("@PreviousActionNotification", DataType.SqlDbType.Bit).Value = mt565.PreviousActionNotification;
                comm.Parameters.Add("@CorporateActionNumber", DataType.SqlDbType.NVarChar, 3).Value = string.IsNullOrEmpty(mt565.CorporateActionNumber) ? (object)DBNull.Value : mt565.CorporateActionNumber;
                comm.Parameters.Add("@CorporateActionIndicator", DataType.SqlDbType.NVarChar, 4).Value = mt565.CorporateActionIndicator;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt565.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT565 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt565.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT565_StoredProc}");
                return $"Error fetching the MT565 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT92 Stored Procedure.
        /// </summary>
        /// <param name="mt592"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT592(MT592 mt592, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt592.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt592.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mt592.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mt592.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt592.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mt592.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt592.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt592.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT592.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT592 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt592.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT592 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT599 Stored Procedure.
        /// </summary>
        /// <param name="mt599"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT599(MT599 mt599, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt599.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt599.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt599.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt599.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt599.OrderRelatedRef) ? string.Empty : mt599.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt599.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt599.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT599.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT599 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt599.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT599 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT92 Stored Procedure.
        /// </summary>
        /// <param name="mt692"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT692(MT692 mt692, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt692.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt692.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mt692.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mt692.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt692.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mt692.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt692.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt692.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT692.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT692 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt692.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT692 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT99 Stored Procedure.
        /// </summary>
        /// <param name="mt699"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT699(MT699 mt699, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt699.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt699.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt699.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt699.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt699.OrderRelatedRef) ? string.Empty : mt699.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt699.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt699.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT699.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT699 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt699.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT699 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT92 Stored Procedure.
        /// </summary>
        /// <param name="mt792"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT792(MT792 mt792, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt792.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt792.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mt792.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mt792.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt792.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mt792.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt792.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt792.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT792.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT792 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt792.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT792 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT99 Stored Procedure.
        /// </summary>
        /// <param name="mt799"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT799(MT799 mt799, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt799.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt799.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt799.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt799.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt799.OrderRelatedRef) ? string.Empty : mt799.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt799.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt799.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT799.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT799 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt799.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT799 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT92 Stored Procedure.
        /// </summary>
        /// <param name="mt892"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT892(MT892 mt892, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt892.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt892.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mt892.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mt892.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt892.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mt892.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt892.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt892.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT892.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT892 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt892.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT892 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT99 Stored Procedure.
        /// </summary>
        /// <param name="mt899"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT899(MT899 mt899, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt899.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt899.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt899.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt899.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt899.OrderRelatedRef) ? string.Empty : mt899.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt899.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt899.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT899.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT899 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt899.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT899 Swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT900 Stored Procedure.
        /// </summary>
        /// <param name="mT900"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT900(MT900 mT900, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT900_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mT900.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mT900.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mT900.OrderRef;
                comm.Parameters.Add("@RelatedOrderRef", DataType.SqlDbType.NVarChar, 16).Value = mT900.RelatedOrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mT900.AccountNumber;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.DateTime).Value = mT900.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mT900.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 10).Value = mT900.Ccy;
                comm.Parameters.Add("@OrderInstitution", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mT900.OrderInstitution) ? string.Empty : mT900.OrderInstitution;
                comm.Parameters.Add("@Information", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mT900.Information) ? string.Empty : mT900.Information;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mT900.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT900 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mT900.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT900_StoredProc}");
                return $"Error fetching the MT900 Swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT910 Stored Procedure.
        /// </summary>
        /// <param name="mT910"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT910(MT910 mT910, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT910_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mT910.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mT910.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mT910.OrderRef;
                comm.Parameters.Add("@RelatedOrderRef", DataType.SqlDbType.NVarChar, 16).Value = mT910.RelatedOrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mT910.AccountNumber;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.DateTime).Value = mT910.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = mT910.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 10).Value = mT910.Ccy;
                comm.Parameters.Add("@OrderInstitution", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mT910.OrderInstitution) ? string.Empty : mT910.OrderInstitution;
                comm.Parameters.Add("@IntermediarySwiftCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(mT910.IntermediarySwiftCode) ? string.Empty : mT910.IntermediarySwiftCode;
                comm.Parameters.Add("@Information", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mT910.Information) ? string.Empty : mT910.Information;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mT910.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT910 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mT910.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT910 Swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT920 Stored Procedure.
        /// </summary>
        /// <param name="mt920"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT920(MT920 mt920, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT920_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt920.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt920.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt920.OrderRef;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt920.OrderSwift;
                comm.Parameters.Add("@OrderCCY", DataType.SqlDbType.NVarChar, 3).Value = mt920.OrderCcy;
                if (mt920.OrderAmount > 0)
                {
                    comm.Parameters.Add("@OrderAmount", DataType.SqlDbType.Money).Value = mt920.OrderAmount;
                }
                comm.Parameters.Add("@OrderAccountNumber", DataType.SqlDbType.NVarChar, 50).Value = mt920.OrderAccountNumber;
                comm.Parameters.Add("@OrderType", DataType.SqlDbType.NVarChar, 3).Value = mt920.OrderType;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt920.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT920 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt920.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT920_StoredProc}");
                return $"Error fetching the MT92 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT940 Stored Procedure.
        /// </summary>
        /// <param name="mt94"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT940(MT94 mt94, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT940_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt94.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt94.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt94.OrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mt94.AccountNumber;
                comm.Parameters.Add("@StatementNumber", DataType.SqlDbType.NVarChar, 10).Value = mt94.StatementNumber;
                comm.Parameters.Add("@StatementSequenceNumber", DataType.SqlDbType.NVarChar, 3).Value = mt94.StatementSequenceNumber;
                comm.Parameters.Add("@OpeningBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.OpeningBalanceIsCredit;
                comm.Parameters.Add("@OpeningBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.OpeningBalanceStatementDate;
                comm.Parameters.Add("@OpeningBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.OpeningBalanceCCY;
                comm.Parameters.Add("@OpeningBalanceAmount", DataType.SqlDbType.Money).Value = mt94.OpeningBalanceAmount;
                comm.Parameters.Add("@StatementLine", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt94.StatementLine) ? (object)DBNull.Value : mt94.StatementLine;
                comm.Parameters.Add("@ClosingBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.ClosingBalanceIsCredit;
                comm.Parameters.Add("@ClosingBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.ClosingBalanceStatementDate;
                comm.Parameters.Add("@ClosingBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.ClosingBalanceCCY;
                comm.Parameters.Add("@ClosingBalanceAmount", DataType.SqlDbType.Money).Value = mt94.ClosingBalanceAmount;
                comm.Parameters.Add("@ClosingAvailableBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.ClosingAvailableBalanceIsCredit;
                comm.Parameters.Add("@ClosingAvailableBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.ClosingAvailableBalanceStatementDate;
                comm.Parameters.Add("@ClosingAvailableBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.ClosingAvailableBalanceCCY;
                comm.Parameters.Add("@ClosingAvailableBalanceAmount", DataType.SqlDbType.Money).Value = mt94.ClosingAvailableBalanceAmount;
                comm.Parameters.Add("@Information", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt94.Information) ? (object)DBNull.Value : mt94.Information;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt94.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT940 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt94.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT940_StoredProc}");
                return $"Error fetching the MT940 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT941 Stored Procedure.
        /// </summary>
        /// <param name="mt94"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT941(MT94 mt94, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT941_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt94.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt94.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt94.OrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mt94.AccountNumber;
                comm.Parameters.Add("@StatementNumber", DataType.SqlDbType.NVarChar, 10).Value = mt94.StatementNumber;
                comm.Parameters.Add("@StatementSequenceNumber", DataType.SqlDbType.NVarChar, 3).Value = mt94.StatementSequenceNumber;
                comm.Parameters.Add("@OpeningBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.OpeningBalanceIsCredit;
                comm.Parameters.Add("@OpeningBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.OpeningBalanceStatementDate;
                comm.Parameters.Add("@OpeningBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.OpeningBalanceCCY;
                comm.Parameters.Add("@OpeningBalanceAmount", DataType.SqlDbType.Money).Value = mt94.OpeningBalanceAmount;
                comm.Parameters.Add("@StatementLine", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt94.StatementLine) ? (object)DBNull.Value : mt94.StatementLine;
                comm.Parameters.Add("@ClosingBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.ClosingBalanceIsCredit;
                comm.Parameters.Add("@ClosingBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.ClosingBalanceStatementDate;
                comm.Parameters.Add("@ClosingBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.ClosingBalanceCCY;
                comm.Parameters.Add("@ClosingBalanceAmount", DataType.SqlDbType.Money).Value = mt94.ClosingBalanceAmount;
                comm.Parameters.Add("@ClosingAvailableBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.ClosingAvailableBalanceIsCredit;
                comm.Parameters.Add("@ClosingAvailableBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.ClosingAvailableBalanceStatementDate;
                comm.Parameters.Add("@ClosingAvailableBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.ClosingAvailableBalanceCCY;
                comm.Parameters.Add("@ClosingAvailableBalanceAmount", DataType.SqlDbType.Money).Value = mt94.ClosingAvailableBalanceAmount;
                comm.Parameters.Add("@Information", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt94.Information) ? (object)DBNull.Value : mt94.Information;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt94.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT941 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt94.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT941_StoredProc}");
                return $"Error fetching the MT941 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT941 Stored Procedure.
        /// </summary>
        /// <param name="mt94"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT942(MT94 mt94, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT942_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt94.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt94.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt94.OrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mt94.AccountNumber;
                comm.Parameters.Add("@StatementNumber", DataType.SqlDbType.NVarChar, 10).Value = mt94.StatementNumber;
                comm.Parameters.Add("@StatementSequenceNumber", DataType.SqlDbType.NVarChar, 3).Value = mt94.StatementSequenceNumber;
                comm.Parameters.Add("@OpeningBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.OpeningBalanceIsCredit;
                comm.Parameters.Add("@OpeningBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.OpeningBalanceStatementDate;
                comm.Parameters.Add("@OpeningBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.OpeningBalanceCCY;
                comm.Parameters.Add("@OpeningBalanceAmount", DataType.SqlDbType.Money).Value = mt94.OpeningBalanceAmount;
                comm.Parameters.Add("@StatementLine", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt94.StatementLine) ? (object)DBNull.Value : mt94.StatementLine;
                comm.Parameters.Add("@ClosingBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.ClosingBalanceIsCredit;
                comm.Parameters.Add("@ClosingBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.ClosingBalanceStatementDate;
                comm.Parameters.Add("@ClosingBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.ClosingBalanceCCY;
                comm.Parameters.Add("@ClosingBalanceAmount", DataType.SqlDbType.Money).Value = mt94.ClosingBalanceAmount;
                comm.Parameters.Add("@ClosingAvailableBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt94.ClosingAvailableBalanceIsCredit;
                comm.Parameters.Add("@ClosingAvailableBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt94.ClosingAvailableBalanceStatementDate;
                comm.Parameters.Add("@ClosingAvailableBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt94.ClosingAvailableBalanceCCY;
                comm.Parameters.Add("@ClosingAvailableBalanceAmount", DataType.SqlDbType.Money).Value = mt94.ClosingAvailableBalanceAmount;
                comm.Parameters.Add("@Information", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt94.Information) ? (object)DBNull.Value : mt94.Information;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt94.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT942 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt94.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT942_StoredProc}");
                return $"Error fetching the MT942 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT950 Stored Procedure.
        /// </summary>
        /// <param name="mt95"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT950(MT95 mt95, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT950_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt95.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt95.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt95.OrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mt95.AccountNumber;
                comm.Parameters.Add("@StatementNumber", DataType.SqlDbType.NVarChar, 10).Value = mt95.StatementNumber;
                comm.Parameters.Add("@StatementSequenceNumber", DataType.SqlDbType.NVarChar, 3).Value = mt95.StatementSequenceNumber;
                comm.Parameters.Add("@OpeningBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.OpeningBalanceIsCredit;
                comm.Parameters.Add("@OpeningBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.OpeningBalanceStatementDate;
                comm.Parameters.Add("@OpeningBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.OpeningBalanceCCY;
                comm.Parameters.Add("@OpeningBalanceAmount", DataType.SqlDbType.Money).Value = mt95.OpeningBalanceAmount;
                comm.Parameters.Add("@StatementLine", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt95.StatementLine) ? (object)DBNull.Value : mt95.StatementLine;
                comm.Parameters.Add("@ClosingBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.ClosingBalanceIsCredit;
                comm.Parameters.Add("@ClosingBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.ClosingBalanceStatementDate;
                comm.Parameters.Add("@ClosingBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.ClosingBalanceCCY;
                comm.Parameters.Add("@ClosingBalanceAmount", DataType.SqlDbType.Money).Value = mt95.ClosingBalanceAmount;
                comm.Parameters.Add("@ClosingAvailableBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.ClosingAvailableBalanceIsCredit;
                comm.Parameters.Add("@ClosingAvailableBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.ClosingAvailableBalanceStatementDate;
                comm.Parameters.Add("@ClosingAvailableBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.ClosingAvailableBalanceCCY;
                comm.Parameters.Add("@ClosingAvailableBalanceAmount", DataType.SqlDbType.Money).Value = mt95.ClosingAvailableBalanceAmount;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt95.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT950 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt95.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT950_StoredProc}");
                return $"Error fetching the MT950 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT970 Stored Procedure.
        /// </summary>
        /// <param name="mt95"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT970(MT95 mt95, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT970_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt95.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt95.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt95.OrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mt95.AccountNumber;
                comm.Parameters.Add("@StatementNumber", DataType.SqlDbType.NVarChar, 10).Value = mt95.StatementNumber;
                comm.Parameters.Add("@StatementSequenceNumber", DataType.SqlDbType.NVarChar, 3).Value = mt95.StatementSequenceNumber;
                comm.Parameters.Add("@OpeningBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.OpeningBalanceIsCredit;
                comm.Parameters.Add("@OpeningBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.OpeningBalanceStatementDate;
                comm.Parameters.Add("@OpeningBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.OpeningBalanceCCY;
                comm.Parameters.Add("@OpeningBalanceAmount", DataType.SqlDbType.Money).Value = mt95.OpeningBalanceAmount;
                comm.Parameters.Add("@StatementLine", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt95.StatementLine) ? (object)DBNull.Value : mt95.StatementLine;
                comm.Parameters.Add("@ClosingBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.ClosingBalanceIsCredit;
                comm.Parameters.Add("@ClosingBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.ClosingBalanceStatementDate;
                comm.Parameters.Add("@ClosingBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.ClosingBalanceCCY;
                comm.Parameters.Add("@ClosingBalanceAmount", DataType.SqlDbType.Money).Value = mt95.ClosingBalanceAmount;
                comm.Parameters.Add("@ClosingAvailableBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.ClosingAvailableBalanceIsCredit;
                comm.Parameters.Add("@ClosingAvailableBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.ClosingAvailableBalanceStatementDate;
                comm.Parameters.Add("@ClosingAvailableBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.ClosingAvailableBalanceCCY;
                comm.Parameters.Add("@ClosingAvailableBalanceAmount", DataType.SqlDbType.Money).Value = mt95.ClosingAvailableBalanceAmount;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt95.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT970 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt95.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT970_StoredProc}");
                return $"Error fetching the MT970 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT971 Stored Procedure.
        /// </summary>
        /// <param name="mt971"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT971(MT971 mt971, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT971_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt971.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt971.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt971.OrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mt971.AccountNumber;
                comm.Parameters.Add("@ClosingBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt971.ClosingBalanceIsCredit;
                comm.Parameters.Add("@ClosingBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt971.ClosingBalanceStatementDate;
                comm.Parameters.Add("@ClosingBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt971.ClosingBalanceCCY;
                comm.Parameters.Add("@ClosingBalanceAmount", DataType.SqlDbType.Money).Value = mt971.ClosingBalanceAmount;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt971.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT971 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt971.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT971_StoredProc}");
                return $"Error fetching the MT971 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT972 Stored Procedure.
        /// </summary>
        /// <param name="mt95"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT972(MT95 mt95, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT972_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt95.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt95.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt95.OrderRef;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mt95.AccountNumber;
                comm.Parameters.Add("@StatementNumber", DataType.SqlDbType.NVarChar, 10).Value = mt95.StatementNumber;
                comm.Parameters.Add("@StatementSequenceNumber", DataType.SqlDbType.NVarChar, 3).Value = mt95.StatementSequenceNumber;
                comm.Parameters.Add("@OpeningBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.OpeningBalanceIsCredit;
                comm.Parameters.Add("@OpeningBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.OpeningBalanceStatementDate;
                comm.Parameters.Add("@OpeningBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.OpeningBalanceCCY;
                comm.Parameters.Add("@OpeningBalanceAmount", DataType.SqlDbType.Money).Value = mt95.OpeningBalanceAmount;
                comm.Parameters.Add("@StatementLine", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(mt95.StatementLine) ? (object)DBNull.Value : mt95.StatementLine;
                comm.Parameters.Add("@ClosingBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.ClosingBalanceIsCredit;
                comm.Parameters.Add("@ClosingBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.ClosingBalanceStatementDate;
                comm.Parameters.Add("@ClosingBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.ClosingBalanceCCY;
                comm.Parameters.Add("@ClosingBalanceAmount", DataType.SqlDbType.Money).Value = mt95.ClosingBalanceAmount;
                comm.Parameters.Add("@ClosingAvailableBalanceIsCredit", DataType.SqlDbType.Bit).Value = mt95.ClosingAvailableBalanceIsCredit;
                comm.Parameters.Add("@ClosingAvailableBalanceStatementDate", DataType.SqlDbType.DateTime).Value = mt95.ClosingAvailableBalanceStatementDate;
                comm.Parameters.Add("@ClosingAvailableBalanceCCY", DataType.SqlDbType.NVarChar, 3).Value = mt95.ClosingAvailableBalanceCCY;
                comm.Parameters.Add("@ClosingAvailableBalanceAmount", DataType.SqlDbType.Money).Value = mt95.ClosingAvailableBalanceAmount;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt95.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT972 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt95.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT972_StoredProc}");
                return $"Error fetching the MT972 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT973 Stored Procedure.
        /// </summary>
        /// <param name="mt973"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT973(MT973 mt973, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT973_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt973.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 50).Value = mt973.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = mt973.OrderRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar, 150).Value = mt973.Message;
                comm.Parameters.Add("@AccountNumber", DataType.SqlDbType.NVarChar, 35).Value = mt973.AccountNumber;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt973.SwiftUrgent;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT973 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt973.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the swift file for {_swiftConfig.MT973_StoredProc}");
                return $"Error fetching the MT973 swift file details : Error ({ex.Message})";
            }
        }

        /// <summary>
        /// Call the Swift MT92 Stored Procedure.
        /// </summary>
        /// <param name="mt992"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT992(MT992 mt992, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT92_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt992.ClientSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt992.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = mt992.OrderRelatedRef;
                comm.Parameters.Add("@OrderRelatedDate", DataType.SqlDbType.DateTime).Value = mt992.OrderRelatedDate;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = mt992.OrderSwift;
                comm.Parameters.Add("@MessageType", DataType.SqlDbType.Int).Value = mt992.MessageType;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt992.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt992.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT992.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT992 Swift file ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt992.OrderRef, clientId);
                return result;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT92_StoredProc}");
                return $"Error fetching the MT992 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift MT999 Stored Procedure.
        /// </summary>
        /// <param name="mt999"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchMT999(MT999 mt999, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.MT99_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId; //mt999.ClientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt999.ClientSwiftCode;
                comm.Parameters.Add("@BrokerSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = mt999.BrokerSwiftCode;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = mt999.OrderRef;
                comm.Parameters.Add("@OrderRelatedRef", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(mt999.OrderRelatedRef) ? string.Empty : mt999.OrderRelatedRef;
                comm.Parameters.Add("@Message", DataType.SqlDbType.NVarChar).Value = mt999.Message;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = mt999.SwiftUrgent;
                comm.Parameters.Add("@SwiftType", DataType.SqlDbType.NVarChar, 3).Value = SwiftTypes.MT999.Substring(2, 3);
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("MT999 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, mt999.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.MT99_StoredProc}");
                return $"Error fetching the MT999 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift Pain001 Stored Procedure.
        /// </summary>
        /// <param name="pain001"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchPain001(Pain001 pain001, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.Pain001_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = pain001.ClientSwiftCode;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.SmallDateTime).Value = pain001.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = pain001.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = pain001.Ccy;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = pain001.OrderRef;
                comm.Parameters.Add("@OrderName", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(pain001.OrderName) ? (object)DBNull.Value : pain001.OrderName;
                comm.Parameters.Add("@OrderAddress", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.OrderAddress) ? (object)DBNull.Value : pain001.OrderAddress;
                comm.Parameters.Add("@OrderCity", DataType.SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(pain001.OrderCity) ? (object)DBNull.Value : pain001.OrderCity;
                comm.Parameters.Add("@OrderPostCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.OrderPostCode) ? (object)DBNull.Value : pain001.OrderPostCode;
                comm.Parameters.Add("@OrderCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pain001.OrderCountryCode) ? (object)DBNull.Value : pain001.OrderCountryCode;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.OrderSwift) ? (object)DBNull.Value : pain001.OrderSwift;
                comm.Parameters.Add("@OrderIban", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.OrderIbanAccountNumber) ? (object)DBNull.Value : pain001.OrderIbanAccountNumber;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.OrderOther) ? (object)DBNull.Value : pain001.OrderOther;
                comm.Parameters.Add("@OrderVOCode", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(pain001.OrderVOCode) ? (object)DBNull.Value : pain001.OrderVOCode;
                comm.Parameters.Add("@BenName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryName) ? (object)DBNull.Value : pain001.BeneficiaryName;
                comm.Parameters.Add("@BenAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryAddress1) ? (object)DBNull.Value : pain001.BeneficiaryAddress1;
                comm.Parameters.Add("@BenAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryAddress2) ? (object)DBNull.Value : pain001.BeneficiaryAddress2;
                comm.Parameters.Add("@BenZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiaryPostCode) ? (object)DBNull.Value : pain001.BeneficiaryPostCode;
                comm.Parameters.Add("@BenCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pain001.BenCountryCode) ? (object)DBNull.Value : pain001.BenCountryCode;
                comm.Parameters.Add("@BenCcy", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(pain001.BeneficiaryCcy) ? (object)DBNull.Value : pain001.BeneficiaryCcy;
                comm.Parameters.Add("@BenBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankName) ? (object)DBNull.Value : pain001.BeneficiaryBankName;
                comm.Parameters.Add("@BenBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankAddress1) ? (object)DBNull.Value : pain001.BeneficiaryBankAddress1;
                comm.Parameters.Add("@BenBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankAddress2) ? (object)DBNull.Value : pain001.BeneficiaryBankAddress2;
                comm.Parameters.Add("@BenBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankPostCode) ? (object)DBNull.Value : pain001.BeneficiaryBankPostCode;
                comm.Parameters.Add("@BenBankCountryCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankCountryCode) ? (object)DBNull.Value : pain001.BeneficiaryBankCountryCode;
                comm.Parameters.Add("@BenBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankSwift) ? (object)DBNull.Value : pain001.BeneficiaryBankSwift;
                comm.Parameters.Add("@BenBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankIBAN) ? (object)DBNull.Value : pain001.BeneficiaryBankIBAN;
                comm.Parameters.Add("@BenSubBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankName) ? (object)DBNull.Value : pain001.BeneficiarySubBankName;
                comm.Parameters.Add("@BenSubBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankAddress1) ? (object)DBNull.Value : pain001.BeneficiarySubBankAddress1;
                comm.Parameters.Add("@BenSubBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankAddress2) ? (object)DBNull.Value : pain001.BeneficiarySubBankAddress2;
                comm.Parameters.Add("@BenSubBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankPostCode) ? (object)DBNull.Value : pain001.BeneficiarySubBankPostCode;
                comm.Parameters.Add("@BenSubBankCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pain001.BenSubBankCountryCode) ? (object)DBNull.Value : pain001.BenSubBankCountryCode;
                comm.Parameters.Add("@BenSubBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankSwift) ? (object)DBNull.Value : pain001.BeneficiarySubBankSwift;
                comm.Parameters.Add("@BenSubBankBik", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankBik) ? (object)DBNull.Value : pain001.BeneficiarySubBankBik;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = pain001.SwiftUrgent;
                comm.Parameters.Add("@SwiftSendRef", DataType.SqlDbType.Bit).Value = pain001.SwiftSendRef;
                comm.Parameters.Add("@ExchangeRate", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(pain001.ExchangeRate.ToString()) ? (object)DBNull.Value : pain001.ExchangeRate;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("Pain 001 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, pain001.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Pain 001 Swift file for {_swiftConfig.Pain001_StoredProc}");
                return $"Error fetching the Pain 001 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift Pain001NatEst Stored Procedure.
        /// </summary>
        /// <param name="pain001"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchPain001NatWest(Pain001NatWest pain001, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.Pain001NatWest_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@NatWestClientID", DataType.SqlDbType.NVarChar, 50).Value = pain001.NatWestClientID;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = pain001.ClientSwiftCode;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.SmallDateTime).Value = pain001.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = pain001.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = pain001.Ccy;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = pain001.OrderRef;
                comm.Parameters.Add("@OrderName", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(pain001.OrderName) ? (object)DBNull.Value : pain001.OrderName;
                comm.Parameters.Add("@OrderAddress", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.OrderAddress) ? (object)DBNull.Value : pain001.OrderAddress;
                comm.Parameters.Add("@OrderCity", DataType.SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(pain001.OrderCity) ? (object)DBNull.Value : pain001.OrderCity;
                comm.Parameters.Add("@OrderPostCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.OrderPostCode) ? (object)DBNull.Value : pain001.OrderPostCode;
                comm.Parameters.Add("@OrderCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pain001.OrderCountryCode) ? (object)DBNull.Value : pain001.OrderCountryCode;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.OrderSwift) ? (object)DBNull.Value : pain001.OrderSwift;
                comm.Parameters.Add("@OrderIban", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.OrderIbanAccountNumber) ? (object)DBNull.Value : pain001.OrderIbanAccountNumber;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.OrderOther) ? (object)DBNull.Value : pain001.OrderOther;
                comm.Parameters.Add("@OrderVOCode", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(pain001.OrderVOCode) ? (object)DBNull.Value : pain001.OrderVOCode;
                comm.Parameters.Add("@BenName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryName) ? (object)DBNull.Value : pain001.BeneficiaryName;
                comm.Parameters.Add("@BenAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryAddress1) ? (object)DBNull.Value : pain001.BeneficiaryAddress1;
                comm.Parameters.Add("@BenAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryAddress2) ? (object)DBNull.Value : pain001.BeneficiaryAddress2;
                comm.Parameters.Add("@BenZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiaryPostCode) ? (object)DBNull.Value : pain001.BeneficiaryPostCode;
                comm.Parameters.Add("@BenCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pain001.BenCountryCode) ? (object)DBNull.Value : pain001.BenCountryCode;
                comm.Parameters.Add("@BenCcy", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(pain001.BeneficiaryCcy) ? (object)DBNull.Value : pain001.BeneficiaryCcy;
                comm.Parameters.Add("@BenBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankName) ? (object)DBNull.Value : pain001.BeneficiaryBankName;
                comm.Parameters.Add("@BenBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankAddress1) ? (object)DBNull.Value : pain001.BeneficiaryBankAddress1;
                comm.Parameters.Add("@BenBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankAddress2) ? (object)DBNull.Value : pain001.BeneficiaryBankAddress2;
                comm.Parameters.Add("@BenBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankPostCode) ? (object)DBNull.Value : pain001.BeneficiaryBankPostCode;
                comm.Parameters.Add("@BenBankCountryCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankCountryCode) ? (object)DBNull.Value : pain001.BeneficiaryBankCountryCode;
                comm.Parameters.Add("@BenBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankSwift) ? (object)DBNull.Value : pain001.BeneficiaryBankSwift;
                comm.Parameters.Add("@BenBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(pain001.BeneficiaryBankIBAN) ? (object)DBNull.Value : pain001.BeneficiaryBankIBAN;
                comm.Parameters.Add("@BenSubBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankName) ? (object)DBNull.Value : pain001.BeneficiarySubBankName;
                comm.Parameters.Add("@BenSubBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankAddress1) ? (object)DBNull.Value : pain001.BeneficiarySubBankAddress1;
                comm.Parameters.Add("@BenSubBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankAddress2) ? (object)DBNull.Value : pain001.BeneficiarySubBankAddress2;
                comm.Parameters.Add("@BenSubBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankPostCode) ? (object)DBNull.Value : pain001.BeneficiarySubBankPostCode;
                comm.Parameters.Add("@BenSubBankCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pain001.BenSubBankCountryCode) ? (object)DBNull.Value : pain001.BenSubBankCountryCode;
                comm.Parameters.Add("@BenSubBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankSwift) ? (object)DBNull.Value : pain001.BeneficiarySubBankSwift;
                comm.Parameters.Add("@BenSubBankBik", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pain001.BeneficiarySubBankBik) ? (object)DBNull.Value : pain001.BeneficiarySubBankBik;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = pain001.SwiftUrgent;
                comm.Parameters.Add("@SwiftSendRef", DataType.SqlDbType.Bit).Value = pain001.SwiftSendRef;
                comm.Parameters.Add("@ExchangeRate", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(pain001.ExchangeRate.ToString()) ? (object)DBNull.Value : pain001.ExchangeRate;
                comm.Parameters.Add("@ExportFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("Pain 001 NatWest Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, pain001.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Pain 001 NatWest Swift file for {_swiftConfig.Pain001NatWest_StoredProc}");
                return $"Error fetching the Pain 001 NatWest Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the Swift Pacs008 Stored Procedure.
        /// </summary>
        /// <param name="pacs008"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchPacs008(Pacs008 pacs008, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.Pacs008_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ClientSwiftCode", DataType.SqlDbType.NVarChar, 20).Value = pacs008.ClientSwiftCode;
                comm.Parameters.Add("@SettleDate", DataType.SqlDbType.SmallDateTime).Value = pacs008.SettleDate;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Money).Value = pacs008.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 3).Value = pacs008.Ccy;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 50).Value = pacs008.OrderRef;
                comm.Parameters.Add("@OrderName", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(pacs008.OrderName) ? (object)DBNull.Value : pacs008.OrderName;
                comm.Parameters.Add("@OrderAddress", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.OrderAddress) ? (object)DBNull.Value : pacs008.OrderAddress;
                comm.Parameters.Add("@OrderCity", DataType.SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(pacs008.OrderCity) ? (object)DBNull.Value : pacs008.OrderCity;
                comm.Parameters.Add("@OrderPostCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.OrderPostCode) ? (object)DBNull.Value : pacs008.OrderPostCode;
                comm.Parameters.Add("@OrderCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pacs008.OrderCountryCode) ? (object)DBNull.Value : pacs008.OrderCountryCode;
                comm.Parameters.Add("@OrderSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.OrderSwift) ? (object)DBNull.Value : pacs008.OrderSwift;
                comm.Parameters.Add("@OrderIban", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.OrderIbanAccountNumber) ? (object)DBNull.Value : pacs008.OrderIbanAccountNumber;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.OrderOther) ? (object)DBNull.Value : pacs008.OrderOther;
                comm.Parameters.Add("@OrderVOCode", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(pacs008.OrderVOCode) ? (object)DBNull.Value : pacs008.OrderVOCode;
                comm.Parameters.Add("@BenName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiaryName) ? (object)DBNull.Value : pacs008.BeneficiaryName;
                comm.Parameters.Add("@BenAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiaryAddress1) ? (object)DBNull.Value : pacs008.BeneficiaryAddress1;
                comm.Parameters.Add("@BenAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiaryAddress2) ? (object)DBNull.Value : pacs008.BeneficiaryAddress2;
                comm.Parameters.Add("@BenZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.BeneficiaryPostCode) ? (object)DBNull.Value : pacs008.BeneficiaryPostCode;
                comm.Parameters.Add("@BenCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pacs008.BenCountryCode) ? (object)DBNull.Value : pacs008.BenCountryCode;
                comm.Parameters.Add("@BenCcy", DataType.SqlDbType.NVarChar, 10).Value = string.IsNullOrEmpty(pacs008.BeneficiaryCcy) ? (object)DBNull.Value : pacs008.BeneficiaryCcy;
                comm.Parameters.Add("@BenBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiaryBankName) ? (object)DBNull.Value : pacs008.BeneficiaryBankName;
                comm.Parameters.Add("@BenBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiaryBankAddress1) ? (object)DBNull.Value : pacs008.BeneficiaryBankAddress1;
                comm.Parameters.Add("@BenBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiaryBankAddress2) ? (object)DBNull.Value : pacs008.BeneficiaryBankAddress2;
                comm.Parameters.Add("@BenBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.BeneficiaryBankPostCode) ? (object)DBNull.Value : pacs008.BeneficiaryBankPostCode;
                comm.Parameters.Add("@BenBankCountryCode", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.BeneficiaryBankCountryCode) ? (object)DBNull.Value : pacs008.BeneficiaryBankCountryCode;
                comm.Parameters.Add("@BenBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.BeneficiaryBankSwift) ? (object)DBNull.Value : pacs008.BeneficiaryBankSwift;
                comm.Parameters.Add("@BenBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = string.IsNullOrEmpty(pacs008.BeneficiaryBankIBAN) ? (object)DBNull.Value : pacs008.BeneficiaryBankIBAN;
                comm.Parameters.Add("@BenSubBankName", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiarySubBankName) ? (object)DBNull.Value : pacs008.BeneficiarySubBankName;
                comm.Parameters.Add("@BenSubBankAddress1", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiarySubBankAddress1) ? (object)DBNull.Value : pacs008.BeneficiarySubBankAddress1;
                comm.Parameters.Add("@BenSubBankAddress2", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiarySubBankAddress2) ? (object)DBNull.Value : pacs008.BeneficiarySubBankAddress2;
                comm.Parameters.Add("@BenSubBankZip", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.BeneficiarySubBankPostCode) ? (object)DBNull.Value : pacs008.BeneficiarySubBankPostCode;
                comm.Parameters.Add("@BenSubBankCountryCode", DataType.SqlDbType.NVarChar, 2).Value = string.IsNullOrEmpty(pacs008.BenSubBankCountryCode) ? (object)DBNull.Value : pacs008.BenSubBankCountryCode;
                comm.Parameters.Add("@BenSubBankSwift", DataType.SqlDbType.NVarChar, 50).Value = string.IsNullOrEmpty(pacs008.BeneficiarySubBankSwift) ? (object)DBNull.Value : pacs008.BeneficiarySubBankSwift;
                comm.Parameters.Add("@BenSubBankBik", DataType.SqlDbType.NVarChar, 150).Value = string.IsNullOrEmpty(pacs008.BeneficiarySubBankBik) ? (object)DBNull.Value : pacs008.BeneficiarySubBankBik;
                comm.Parameters.Add("@SwiftUrgent", DataType.SqlDbType.Bit).Value = pacs008.SwiftUrgent;
                comm.Parameters.Add("@SwiftSendRef", DataType.SqlDbType.Bit).Value = pacs008.SwiftSendRef;
                comm.Parameters.Add("@ExchangeRate", DataType.SqlDbType.Money).Value = string.IsNullOrEmpty(pacs008.ExchangeRate.ToString()) ? (object)DBNull.Value : pacs008.ExchangeRate;
                comm.Parameters.Add("@SwiftFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("Pacs 008 Swift File ({}). OrderRef={OrderRef} ClientId={ClientId}", result, pacs008.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the Swift file for {_swiftConfig.Pacs008_StoredProc}");
                return $"Error fetching the Pacs 008 Swift file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the NatWest Domestic Payment Stored Procedure.
        /// </summary>
        /// <param name="natWestDomesticPayment"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchNatWestDomesticPayment(OpenPankingDomesticPayment natWestDomesticPayment, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.FlowPaymentCreateSwiftOpenBankingDomesticPaymentStoredproc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ConsentID", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(natWestDomesticPayment.ConsentID) ? (object)DBNull.Value : natWestDomesticPayment.ConsentID;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Decimal).Value = natWestDomesticPayment.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 10).Value = natWestDomesticPayment.CCY;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = natWestDomesticPayment.OrderRef;
                comm.Parameters.Add("@OrderSortCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(natWestDomesticPayment.OrderSortCode) ? (object)DBNull.Value : natWestDomesticPayment.OrderSortCode;
                comm.Parameters.Add("@OrderIBANorAccountNo", DataType.SqlDbType.NVarChar, 32).Value = natWestDomesticPayment.OrderIBANorAccountNo;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 70).Value = string.IsNullOrEmpty(natWestDomesticPayment.OrderOther) ? (object)DBNull.Value : natWestDomesticPayment.OrderOther;
                comm.Parameters.Add("@BenName", DataType.SqlDbType.NVarChar, 250).Value = natWestDomesticPayment.BenName;
                comm.Parameters.Add("@BenSortCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(natWestDomesticPayment.BenSortCode) ? (object)DBNull.Value : natWestDomesticPayment.BenSortCode;
                comm.Parameters.Add("@BenIBANorAccountNo", DataType.SqlDbType.NVarChar, 32).Value = natWestDomesticPayment.BenIBANorAccountNo;
                comm.Parameters.Add("@ExportFile", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("Open banking domestic payment ({}). OrderRef={OrderRef} ClientId={ClientId}", result, natWestDomesticPayment.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the open banking domestic payment for {_swiftConfig.FlowPaymentCreateSwiftOpenBankingDomesticPaymentStoredproc}");
                return $"Error fetching the open banking domestic payment file details : Error {ex.Message}";
            }
        }

        /// <summary>
        /// Call the NatWest International Payment Stored Procedure.
        /// </summary>
        /// <param name="natWestInternationalPayment"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchNatWestInternationalPayment(OpenBankingInternationalPayment natWestInternationalPayment, int clientId)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.FlowPaymentCreateSwiftOpenBankingInternationalPaymentStoredproc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ConsentID", DataType.SqlDbType.NVarChar, 30).Value = string.IsNullOrEmpty(natWestInternationalPayment.ConsentID) ? (object)DBNull.Value : natWestInternationalPayment.ConsentID;
                comm.Parameters.Add("@Amount", DataType.SqlDbType.Decimal).Value = natWestInternationalPayment.Amount;
                comm.Parameters.Add("@CCY", DataType.SqlDbType.NVarChar, 10).Value = natWestInternationalPayment.CCY;
                comm.Parameters.Add("@OrderRef", DataType.SqlDbType.NVarChar, 16).Value = natWestInternationalPayment.OrderRef;
                comm.Parameters.Add("@OrderSortCode", DataType.SqlDbType.NVarChar, 11).Value = string.IsNullOrEmpty(natWestInternationalPayment.OrderSortCode) ? (object)DBNull.Value : natWestInternationalPayment.OrderSortCode;
                comm.Parameters.Add("@OrderIBANorAccountNo", DataType.SqlDbType.NVarChar, 32).Value = natWestInternationalPayment.OrderIBANorAccountNo;
                comm.Parameters.Add("@OrderOther", DataType.SqlDbType.NVarChar, 70).Value = string.IsNullOrEmpty(natWestInternationalPayment.OrderOther) ? (object)DBNull.Value : natWestInternationalPayment.OrderOther;
                comm.Parameters.Add("@BenName", DataType.SqlDbType.NVarChar, 35).Value = natWestInternationalPayment.BenName;
                comm.Parameters.Add("@BenBankSwift", DataType.SqlDbType.NVarChar, 11).Value = natWestInternationalPayment.BenBankSwift;
                comm.Parameters.Add("@BenBankIBAN", DataType.SqlDbType.NVarChar, 32).Value = natWestInternationalPayment.BenBankIBAN;
                comm.Parameters.Add("@BenBankCountryCode", DataType.SqlDbType.NVarChar, 2).Value = natWestInternationalPayment.BenBankCountryCode;
                comm.Parameters.Add("@Export", DataType.SqlDbType.NVarChar, 4000).Direction = DataType.ParameterDirection.Output;
                string result = (string)await comm.ExecuteScalarAsync();

                _logger.LogInformation("Open banking international payment ({}). OrderRef={OrderRef} ClientId={ClientId}", result, natWestInternationalPayment.OrderRef, clientId);
                return result.ToString();
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching the open banking international payment for {_swiftConfig.FlowPaymentCreateSwiftOpenBankingDomesticPaymentStoredproc}");
                return $"Error fetching the open banking international payment file details : Error {ex.Message}";
            }
        }
    }
}