﻿using Ionic.Zip;
using Microsoft.Extensions.Logging;
using SwiftApi.Models.Api;
using SwiftApi.Models.Configuration;
using SwiftApi.Repositories.Interfaces;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SwiftApi.Repositories
{
    public class FileRepository : IFile
    {
        private readonly ILogger _logger;
        private readonly SwiftConfig _swiftConfig;

        public FileRepository(SwiftConfig swiftConfig, ILogger logger)
        {
            _swiftConfig = swiftConfig;
            _logger = logger;
        }

        public async Task<string> GenerateFinFile(ClientDetails clientDetails, string filePath, string swiftFile, string swiftType, string orderRef)
        {
            string finDoc = string.Empty;
            try
            {
                var directory = CheckforDirectory(filePath, clientDetails.ClientId);
                if (string.IsNullOrEmpty(directory))
                {
                    Directory.CreateDirectory($"{filePath}{clientDetails.ClientId}");
                    directory = $"{filePath}{clientDetails.ClientId}";
                }

                //Build the Fin file path.
                finDoc = $"{directory}\\{swiftType}_Swift_{orderRef.Replace("/", "_")}_{String.Format("{0:ddMMyyyy_HHmmss}", DateTime.Now)}.fin";

                using (StreamWriter sw = new StreamWriter(finDoc))
                {
                    sw.Write(swiftFile);
                }

                return finDoc;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the file ({}). ClientId={ClientId}", finDoc, clientDetails.ClientId);
                return string.Empty;
            }
        }

        public async Task<string> GenerateFinZipFile(ClientDetails clientDetails, string filePath)
        {
            //Extract the File name and Path.
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var fileFullPath = Path.GetDirectoryName(filePath);
            var fillFullZipPath = $"{fileFullPath}\\{fileName}.zip";

            try
            {
                using (ZipFile finZip = new ZipFile())
                {
                    finZip.Password = clientDetails.Password;
                    finZip.AddFile(filePath, "");
                    finZip.Save(fillFullZipPath);
                };

                _logger.LogInformation("Created the zip file ({}). ClientId={ClientId}", Path.GetFileName(fillFullZipPath), clientDetails.ClientId);
                return fillFullZipPath;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error creating the zip file ({}). ClientId={ClientId}", Path.GetFileName(fillFullZipPath), clientDetails.ClientId);
                return string.Empty;
            }
        }

        /// <summary>
        /// Check to see if the directory exists for the path passed to it.
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string CheckforDirectory(string filepath, string clientId)
        {
            try
            {
                var directories = Directory.GetDirectories(filepath, $"*{clientId}");
                if (directories.Length > 0)
                {
                    return directories[0].ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error searching for directory ClientId={ClientId}.", clientId);
                return string.Empty;
            }
        }
    }
}