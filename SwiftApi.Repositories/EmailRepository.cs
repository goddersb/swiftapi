﻿using Microsoft.Exchange.WebServices.Data;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Client;
using SwiftApi.Models.Api;
using SwiftApi.Models.Configuration;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SwiftApi.Repositories
{
    public class EmailRepository
    {
        private readonly ILogger _logger;
        private readonly SwiftConfig _swiftConfig;
        private readonly ExchangeConfig _exchangeConfig;

        public EmailRepository(SwiftConfig swiftConfig, ILogger logger, ExchangeConfig exchangeConfig)
        {
            _swiftConfig = swiftConfig;
            _logger = logger;
            _exchangeConfig = exchangeConfig;
        }

        private async Task<ExchangeService> getService()
        {
            var identityToken = ConfidentialClientApplicationBuilder
                    .Create(_exchangeConfig.AppId)
                    .WithClientSecret(_exchangeConfig.ClientSecret)
                    .WithTenantId(_exchangeConfig.TenantId)
                    .Build();

            var scopes = new string[] { "https://outlook.office365.com/.default" };
            var token = await identityToken.AcquireTokenForClient(scopes).ExecuteAsync();

            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2016)
            {
                TraceEnabled = true,
                UseDefaultCredentials = false,
                Credentials = new OAuthCredentials(token.AccessToken),
                Url = new Uri(_exchangeConfig.ExchangeEmailPath)
            };

            return service;
        }


        public async Task<bool> SendEmail(ClientDetails clientDetails, string swiftType, string orderRef) 
        {
            try
            {
                var htmlBody = "<!DOCTYPE html><html><style>";
                htmlBody += "div {font-size: 16px;font-family: Tahoma;color: #666666; padding-left:12pt;},";
                htmlBody += "table.datatable{border: 1px solid; border-color:#000033;font-size: 14px;font-family: Tahoma; border-collapse: collapse;},";
                htmlBody += "td.datadef{border: 1px solid; padding-left:10pt; font-family:Calibri; font-size: 16px; padding-right:10pt; color:#193366; height:25px;},";
                htmlBody += "th.datahd{border: 1px solid; vertical-align:center; font-family:Calibri; font-size: 18px; color: #193366; width:45%}";
                htmlBody += "</style>";
                htmlBody += $"<div><p>The {swiftType} swift file is attached to this email.</p></div>";
                htmlBody += "<div><p>The file is password protected, which requires your client password in order to view the details.</p></div></html>";

                //var htmlBody = "<!DOCTYPE html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">";
                //htmlBody += "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset = iso-8859-1\">";
                //htmlBody += "<style>td.fmttext {text-align:center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:17px; margin-bottom:50px;margin-left:100px;padding-bottom:20px;}";
                //htmlBody += "td {mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;}";
                //htmlBody += "td.fmttbl2 {text-align:center; height:30px; background-color:#FDF5E6; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:12px; border-width:1px; border-style:solid; border-color:#C1c1c1;}</style>";
                //htmlBody += "<!--[if (gte mso 9)|(IE)]><style type=\"text/css\">table {border-collapse:collapse;border-spacing0;mso-table-lspace:0pt !important;mso-table-rspace:0pt !important;}</style><![endif]-->";
                //htmlBody += "</head>";
                //htmlBody += "<body style=\"width:800px; margin:0 auto!important; padding:0!important; margin top:50px;\">";
                //htmlBody += "<table style=\"margin-left:100px; background-color:#E4E4E4; width:600px; perspective:1px; separate !important;\" role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
                //htmlBody += "<tbody><tr><td style=\"padding:20px 20px 0px 20px; text-align: center;\"><img style=\"display:block; text-align:center; margin:auto; vertical-align:middle;\" src=\"C:\\DRS_Big.png\"></td>";
                //htmlBody += "</tr><tr><td style=\"text-align:center; font-family:Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:36px; font-weight:900; padding-top:20px; padding-bottom:20px;\">";
                //htmlBody += $"{swiftType} Swift File</td></tr><tr><td class=\"fmttext\">";
                //htmlBody += $"<p>The {swiftType} Swift file requested is attached to this email.</p>";
                //htmlBody += "<p>This is password protected and requires your client password in order to view the contents of this file.</p>";
                //htmlBody += "</td><td>&nbsp;</td></tr><tr><td class=\"fmttbl2\">";
                //htmlBody += "<p>© Dashro Solutions 2022 - Office 7, 35-37 Ludgate Hill, London EC4M 7JN</p>";
                //htmlBody += "</td></tr></tbody></table></body></html>";

                ExchangeService service = await getService();
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeConfig.ExchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeConfig.ExchangeAddress);

                var email = new EmailMessage(service)
                {
                    Subject = $"{swiftType} Swift file {(!string.IsNullOrEmpty(orderRef) ? "for Order Ref :" : string.Empty)} {(!string.IsNullOrEmpty(orderRef) ? orderRef : string.Empty)} requested by {clientDetails.Name}."
                };
                email.Attachments.AddFileAttachment(clientDetails.FileName, clientDetails.FilePath);
                email.Body = new MessageBody(BodyType.HTML, htmlBody);
                string[] emailAddresses = clientDetails.Email.Split(";");
                foreach (string emailaddress in emailAddresses)
                {
                    email.ToRecipients.Add(emailaddress);
                }
                await email.SendAndSaveCopy();

                //Delete the Zipp file.
                File.Delete(clientDetails.FilePath);

                _logger.LogInformation("Email successfully sent to {} containing the file {}. ClientId={ClientId}", clientDetails.Email, clientDetails.FilePath, clientDetails.ClientId);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to send the email to {} containing the file {}. ClientId={ClientId}", clientDetails.Email, clientDetails.FilePath, clientDetails.ClientId);
            }
            return false;
        }
    }
}