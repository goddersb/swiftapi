﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SwiftApi.Models;
using SwiftApi.Models.Api;
using SwiftApi.Models.Configuration;
using SwiftApi.Repositories.Repo_Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using DataType = System.Data;

namespace SwiftApi.Repositories.Interfaces
{
    public class GenericFunctionsRepository : IGenericFunctions
    {
        private readonly ILogger _logger;
        private readonly SwiftConfig _swiftConfig;
        private readonly Extensions _extensions;

        public GenericFunctionsRepository(SwiftConfig swiftConfig, ILogger logger)
        {
            _swiftConfig = swiftConfig;
            _logger = logger;
            _extensions = new Extensions(_logger);
        }

        public async Task<List<Currencies>> FetchCurrencies()
        {
            List<Currencies> currencies = new List<Currencies>();
            try
            {
                using var conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using var comm = new SqlCommand(_swiftConfig.GetCurrencies_StoredProc, conn);
                SqlDataReader reader = comm.ExecuteReader();
                if (reader.HasRows)
                {
                    currencies = await _extensions.DataReaderMapToList<Currencies>(reader);
                }
                return currencies;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error fetching list of valid currecncies.");
                return null;
            }
        }

        public async Task<List<Countries>> FetchCountries()
        {
            List<Countries> countries = new List<Countries>();
            try
            {
                using var conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using var comm = new SqlCommand(_swiftConfig.GetCountries_StoredProc, conn);
                SqlDataReader reader = comm.ExecuteReader();
                if (reader.HasRows)
                {
                    countries = await _extensions.DataReaderMapToList<Countries>(reader);
                }
                return countries;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error fetching list of valid countries.");
                return null;
            }
        }

        public async Task<string> FetchApiKey(string userId)
        {
            try
            {
                using var conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using var comm = new SqlCommand(_swiftConfig.GetApiKey_StoredProc, conn);
                comm.CommandType = DataType.CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", DataType.SqlDbType.NVarChar, 50).Value = userId;
                string result = (string)await comm.ExecuteScalarAsync();
                return result.ToUpper();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error fetching the API Key for client id {userId}");
                return null;
            }
        }
    }
}