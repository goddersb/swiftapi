﻿using Microsoft.Extensions.Logging;
using SwiftApi.Models.Api;
using SwiftApi.Models.Configuration;
using SwiftApi.Repositories.Interfaces;
using SwiftApi.Repositories.Repo_Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SwiftApi.Repositories
{
    public class SqlRepository : ISql
    {
        private readonly ILogger _logger;
        private readonly SwiftConfig _swiftConfig;
        private readonly Extensions _extensions;

        public SqlRepository(SwiftConfig swiftConfig, ILogger logger)
        {
            _swiftConfig = swiftConfig;
            _logger = logger;
            _extensions = new Extensions(_logger);
        }

        /// <summary>
        /// Fetch the file paths.
        /// </summary>
        /// <returns></returns>
        public async Task<List<FilePaths>> FetchFilePaths()
        {
            List<FilePaths> filePaths = new List<FilePaths>();
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.FetchFilePathsStoredProcedure, conn);
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    filePaths = await _extensions.DataReaderMapToList<FilePaths>(reader);
                }
                return filePaths;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching list of valid file paths.");
                return filePaths;
            }
        }

        /// <summary>
        /// Retrieve the client details by clientId.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<ClientDetails> FetchClientDetails(double clientId)
        {
            ClientDetails clientDetails = new ClientDetails();
            try
            {
                using var conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using var comm = new SqlCommand(_swiftConfig.FetchClientDetailsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    reader.Read();
                    clientDetails.ClientId = clientId.ToString();
                    clientDetails.Name = reader["Name"].ToString();
                    clientDetails.CompanyName = reader["Company Name"].ToString();
                    clientDetails.Website = reader["Website"].ToString();
                    clientDetails.Email = reader["Email"].ToString();
                    clientDetails.Password = reader["Password"].ToString();
                    clientDetails.ApiKey = reader["ApiKey"].ToString();
                    clientDetails.RequestsAvailable = (int)reader["RequestsAvailable"];
                    clientDetails.Active = (bool)reader["Active"];
                    clientDetails.RequestsAllocated = (int)reader["RequestsAllocated"];
                    clientDetails.RequestsUsed = (int)reader["RequestsUsed"];
                    clientDetails.EndDate = (DateTime)reader["EndDate"];
                }
                return clientDetails;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, "Error fetching client details. ClientId={ClientId}", clientId);
                return null;
            }
        }

        /// <summary>
        /// Insert details into the Client Request Tables, this logs each request made by the client.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="apiKey"></param>
        /// <param name="requestType"></param>
        /// <param name="requestStatus"></param>
        public async void InsertClientRequest(double clientId, string apiKey, string requestType, string requestStatus)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.InsertClientSwiftStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@ApiKey", SqlDbType.VarChar, 150).Value = apiKey;
                comm.Parameters.Add("@RequestType", SqlDbType.VarChar, 50).Value = requestType;
                comm.Parameters.Add("@RequestStatus", SqlDbType.VarChar, 50).Value = requestStatus;
                await comm.ExecuteNonQueryAsync();

                _logger.LogInformation("Executed the stored procedure {} sucessfully. ClientId={ClientId}", _swiftConfig.InsertClientSwiftStoredProcedure, clientId);
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure {}. ClientId={ClientId}", _swiftConfig.InsertClientSwiftStoredProcedure, clientId);
            }
        }

        /// <summary>
        /// Insert the file details into the table SafeSearchService_FtpFiles.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="ftpFilePath"></param>
        /// <returns></returns>
        public async Task<bool> InsertFtpFiles(double clientId, string ftpFilePath)
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.InsertFtpFilesStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                comm.Parameters.Add("@FtpFile", SqlDbType.VarChar, 150).Value = ftpFilePath;
                await comm.ExecuteNonQueryAsync();
                return true;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex.Message, "Error executing the stored procedure {}.", _swiftConfig.InsertFtpFilesStoredProcedure);
                return false;
            }
        }

        /// <summary>
        /// Validate the Client Id value.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<bool> ValidClientId(string clientId)
        {
            ClientDetails clientDetails = new ClientDetails();
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.FetchClientDetailsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    return true;
                }
                return false;
            }
            catch (FormatException ex)
            {
                _logger.LogError($"Error fetching client details, error message ({ex.Message}).");
                return false;
            }
            catch (SqlException ex)
            {
                _logger.LogError($"Error fetching client details, error message ({ex.Message}).");
                return false;
            }
        }

        public async Task<List<FileExport>> FetchFileExports(int? feId, string? feKey)
        {
            List<FileExport> fileExports = new List<FileExport>();
            try
            {
                using SqlConnection conn = new SqlConnection(_swiftConfig.DolfinPaymentConnectionString);
                conn.Open();
                using SqlCommand comm = new SqlCommand(_swiftConfig.FetchFileExportsStoredProcedure, conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 300;
                comm.Parameters.Add("@FE_ID", SqlDbType.Int).Value = feId;
                comm.Parameters.Add("@FE_Key", SqlDbType.Int).Value = feKey;
                SqlDataReader reader = await comm.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    fileExports = await _extensions.DataReaderMapToList<FileExport>(reader);
                }
                return fileExports;
            }
            catch (SqlException ex)
            {
                _logger.LogError(ex, $"Error fetching list of valid file exports.");
                return fileExports;
            }
        }
    }
}