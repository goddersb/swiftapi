﻿using SwiftApi.Models;
using SwiftApi.Models.Api;
using System.Collections.Generic;

namespace SwiftApi.Repositories.Interfaces
{
    public interface ISwiftValidate
    {
        public string ValidateMT101(MT101 mt101, List<Currencies> currencies, bool validateSwift, bool validateIban) { return string.Empty; }

        public string ValidateMT103(MT103 mt103, List<Currencies> currencies, bool validateSwift, bool validateIban) { return string.Empty; }

        public string ValidateMT92<T>(T item, bool validateSwift) { return string.Empty; }

        public string ValidateMT99<T>(T item, bool validateSwift) { return string.Empty; }

        public string ValidateMT200(MT200 mt200, List<Currencies> currencies, bool validateSwift, bool validateIban) { return string.Empty; }

        public string ValidateMT202(MT202 mt202, List<Currencies> currencies, bool validateSwift, bool validateIban) { return string.Empty; }

        public string ValidateMT210(MT210 mt210, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT380(MT380 mt380, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT502(MT502 mt502, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT508(MT508 mt508, bool validateSwift) { return string.Empty; }

        public string ValidateMT509(MT509 mt509, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT515(MT515 mt515, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT535(MT535 mt535, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT536(MT536 mt536, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT537(MT537 mt537, bool validateSwift) { return string.Empty; }

        public string ValidateMT540(MT540 mt540, bool validateSwift) { return string.Empty; }

        public string ValidateMT541(MT541 mt541, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT542(MT542 mt542, bool validateSwift) { return string.Empty; }

        public string ValidateMT543(MT543 mt543, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT545(MT545 mt545, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT547(MT547 mt547, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT548(MT548 mt548, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT565(MT565 mt565, bool validateSwift) { return string.Empty; }

        public string ValidateMT900(MT900 mT900, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT910(MT910 mT910, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT920(MT920 mt920, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT94(MT94 mt940, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT95(MT95 mt95, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT971(MT971 mt971, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateMT973(MT973 mt973, List<Currencies> currencies, bool validateSwift) { return string.Empty; }

        public string ValidateNatWestDomesticPayment(OpenPankingDomesticPayment natWestDomesticPayment, List<Currencies> currencies) { return string.Empty; }

        public string ValidateNatWestInternationalPayment(OpenBankingInternationalPayment natWestInternationalPayment, List<Countries> countries, bool validateSwift, bool validateIban) { return string.Empty; }
    }
}