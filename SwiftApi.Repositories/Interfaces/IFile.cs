﻿using SwiftApi.Models.Api;
using System.Threading.Tasks;

namespace SwiftApi.Repositories.Interfaces
{
    public interface IFile
    {
        Task<string> GenerateFinFile(ClientDetails clientDetails, string filePath, string swiftFile, string swiftType, string orderRef);

        Task<string> GenerateFinZipFile(ClientDetails clientDetails, string filePath);
    }
}