﻿using SwiftApi.Models.Api;
using System.Threading.Tasks;

namespace SwiftApi.Repositories.Interfaces
{
    public interface ISwift
    {
        Task<string> FetchMT101(MT101 mt101, int clientId);

        Task<string> FetchMT103(MT103 mt103, int clientId);

        Task<string> FetchMT192(MT192 mt192, int clientId);

        Task<string> FetchMT199(MT199 mt199, int clientId);

        Task<string> FetchMT200(MT200 mt200, int clientId);

        Task<string> FetchMT202(MT202 mt202, int clientId);

        Task<string> FetchMT210(MT210 mt210, int clientId);

        Task<string> FetchMT292(MT292 mT292, int clientId);

        Task<string> FetchMT299(MT299 mt299, int clientId);

        Task<string> FetchMT380(MT380 mt380, int clientId);

        Task<string> FetchMT381(MT381 mt381, int clientId);

        Task<string> FetchMT392(MT392 mt392, int clientId);

        Task<string> FetchMT399(MT399 mt399, int clientId);

        Task<string> FetchMT492(MT492 mt492, int clientId);

        Task<string> FetchMT499(MT499 mt499, int clientId);

        Task<string> FetchMT502(MT502 mt502, int clientId);

        Task<string> FetchMT508(MT508 mt508, int clientId);

        Task<string> FetchMT509(MT509 mt509, int clientId);

        Task<string> FetchMT515(MT515 mt515, int clientId);

        Task<string> FetchMT535(MT535 mt535, int clientId);

        Task<string> FetchMT536(MT536 mt536, int clientId);

        Task<string> FetchMT537(MT537 mt537, int clientId);

        Task<string> FetchMT540(MT540 mt540, int clientId);

        Task<string> FetchMT541(MT541 mt541, int clientId);

        Task<string> FetchMT542(MT542 mt542, int clientId);

        Task<string> FetchMT543(MT543 mt543, int clientId);

        Task<string> FetchMT545(MT545 mt545, int clientId);

        Task<string> FetchMT547(MT547 mt547, int clientId);

        Task<string> FetchMT548(MT548 mt548, int clientId);

        Task<string> FetchMT565(MT565 mt565, int clientId);

        Task<string> FetchMT592(MT592 mt592, int clientId);

        Task<string> FetchMT599(MT599 mt599, int clientId);

        Task<string> FetchMT692(MT692 mt692, int clientId);

        Task<string> FetchMT699(MT699 mt699, int clientId);

        Task<string> FetchMT792(MT792 mt792, int clientId);

        Task<string> FetchMT799(MT799 mt799, int clientId);

        Task<string> FetchMT892(MT892 mt892, int clientId);

        Task<string> FetchMT899(MT899 mt899, int clientId);

        Task<string> FetchMT900(MT900 mT900, int clientId);

        Task<string> FetchMT910(MT910 mT910, int clientId);

        Task<string> FetchMT920(MT920 mt920, int clientId);

        Task<string> FetchMT940(MT94 mT94, int clientId);

        Task<string> FetchMT941(MT94 mT94, int clientId);

        Task<string> FetchMT942(MT94 mT94, int clientId);

        Task<string> FetchMT950(MT95 mt95, int clientId);

        Task<string> FetchMT970(MT95 mt95, int clientId);

        Task<string> FetchMT971(MT971 mt971, int clientId);

        Task<string> FetchMT972(MT95 mt95, int clientId);

        Task<string> FetchMT973(MT973 mt973, int clientId);

        Task<string> FetchMT992(MT992 mt992, int clientId);

        Task<string> FetchMT999(MT999 mt999, int clientId);

        Task<string> FetchPain001(Pain001 pain001, int clientId);

        Task<string> FetchPain001NatWest(Pain001NatWest pain001, int clientId);

        Task<string> FetchPacs008(Pacs008 pacs008, int clientId);

        Task<string> FetchNatWestDomesticPayment(OpenPankingDomesticPayment natWestDomesticPayment, int clientId);

        Task<string> FetchNatWestInternationalPayment(OpenBankingInternationalPayment natWestInternationalPayment, int clientId);
    }
}