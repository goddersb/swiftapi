﻿using SwiftApi.Models;
using SwiftApi.Models.Api;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SwiftApi.Repositories.Interfaces
{
    public interface IGenericFunctions
    {
        Task<List<Currencies>> FetchCurrencies();

        Task<List<Countries>> FetchCountries();
    }
}