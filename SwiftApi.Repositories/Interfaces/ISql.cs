﻿using SwiftApi.Models.Api;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SwiftApi.Repositories.Interfaces
{
    public interface ISql
    {
        Task<List<FilePaths>> FetchFilePaths();

        Task<List<FileExport>> FetchFileExports(int? feId, string? feKey);

        public void InsertClientRequest(double clientId, string apiKey, string requestType, string requestStatus);

        Task<bool> InsertFtpFiles(double clientId, string ftpFilePath);

        Task<bool> ValidClientId(string clientId);
    }
}