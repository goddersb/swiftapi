﻿using SwiftApi.Models.Api;
using System.Threading.Tasks;

namespace SwiftApi.Repositories.Interfaces
{
    public interface IEmail
    {
        Task<bool> SendEmail(ClientDetails clientDetails, string swiftType);
    }
}