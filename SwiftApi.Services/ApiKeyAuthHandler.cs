﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using SwiftApi.Models.Configuration;
using SwiftApi.Repositories.Interfaces;
using SwiftApi.Repositories;

namespace SwiftApi.Services
{
    public class ApiKeyAuthHandler : AuthenticationHandler<ApiKeyAuthOpts>
    {
        private readonly SwiftConfig _swiftConfig;

        public ApiKeyAuthHandler(IOptionsMonitor<ApiKeyAuthOpts> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, 
                                                SwiftConfig swiftConfig)
            : base(options, logger, encoder, clock)
        {
            _swiftConfig = swiftConfig;
        }

        /// <summary>
        /// Handle the authorisation of the request based on the API Key.
        /// </summary>
        /// <returns></returns>
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var genericFunctionsRepository = new GenericFunctionsRepository(_swiftConfig, Logger);
            var sqlRepository = new SqlRepository(_swiftConfig, Logger);

            string token = null;
            string apiKey;
            string authorization = Request.Headers["ApiKey"];
            var userId = Request.Query["ClientId"].ToString();

            //Check to see if the client Id is valid.
            if (!await sqlRepository.ValidClientId(userId))
            {
                Logger.LogError("Authentication Error - invalid Client Id value. ClientId={ClientId}", userId);
                return AuthenticateResult.Fail($"Authentication Error - invalid Client Id value ({userId})");
            }

            if (string.IsNullOrEmpty(authorization))
            {
                var controllerName = Request.Path.ToString().Substring(5, Request.Path.ToString().IndexOf("M")).Replace("v1/", "");
                Logger.LogError("Authentication Error", $"No API Key passed to the {controllerName} Endpoint.");
                return AuthenticateResult.NoResult();
            }
            else
            {
                apiKey = await genericFunctionsRepository.FetchApiKey(userId);
                token = authorization.ToUpper();
            }

            if (string.IsNullOrEmpty(token))
            {
                Logger.LogError("Authentication Error - token not found. ClientId={ClientId}", userId);
                return AuthenticateResult.NoResult();
            }

            bool result = false;
            if (token == (string.IsNullOrEmpty(apiKey) ? string.Empty: apiKey))
            {
                result = true;
            }
            else
            {
                result = false;
            }
            if (!result)
            {
                Logger.LogError("Authentication Error token not match. ClientId={ClientId}", userId);
                return AuthenticateResult.Fail($"token not matched ({apiKey})");
            }
            else
            {
                var id = new ClaimsIdentity(
                    new Claim[] { new Claim("Key", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(token)))},
                    Scheme.Name
                );
                ClaimsPrincipal principal = new ClaimsPrincipal(id);
                var ticket = new AuthenticationTicket(principal, new AuthenticationProperties(), Scheme.Name);
                return AuthenticateResult.Success(ticket);
            }
        }

        private static string ReverseUsingArrayClass(string text)
        {
               char[] chars = text.ToCharArray();
               Array.Reverse(chars);
               return new string(chars);
        }
    }
}