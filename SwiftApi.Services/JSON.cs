﻿using Newtonsoft.Json;

namespace SwiftApi.Services
{
    public class JSON
    {
        public static string Serializer(object payload)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            return JsonConvert.SerializeObject(payload, settings);
        }

        public static string Deserializer(object payload)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            var returnDesializedPayload = JsonConvert.DeserializeObject<dynamic>((string)payload);
            return returnDesializedPayload.Tostring();
        }
    }
}