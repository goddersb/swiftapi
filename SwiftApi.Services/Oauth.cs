﻿using SwiftApi.Logging;
using SwiftApi.Models.Configuration;

namespace SwiftApi.Services
{
    public class Oauth
    {
        private readonly ILog _logger;
        private readonly SwiftConfig _swiftConfig;

        public Oauth(SwiftConfig swiftConfig, ILog logger)
        {
            _logger = logger;
            _swiftConfig = swiftConfig;
        }
    }
}