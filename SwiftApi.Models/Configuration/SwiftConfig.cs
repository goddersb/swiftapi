﻿namespace SwiftApi.Models.Configuration
{
    public class SwiftConfig
    {
        public string DolfinPayments { get; set; }
        public string DolfinPaymentConnectionString { get; set; }
        public string GetCurrencies_StoredProc { get; set; }
        public string GetCountries_StoredProc { get; set; }
        public string GetApiKey_StoredProc { get; set; }
        public string MT101_StoredProc { get; set; }
        public string MT103_StoredProc { get; set; }
        public string MT92_StoredProc { get; set; }
        public string MT99_StoredProc { get; set; }
        public string MT200_StoredProc { get; set; }
        public string MT202_StoredProc { get; set; }
        public string MT210_StoredProc { get; set; }
        public string MT380_StoredProc { get; set; }
        public string MT381_StoredProc { get; set; }
        public string MT502_StoredProc { get; set; }
        public string MT508_StoredProc { get; set; }
        public string MT509_StoredProc { get; set; }
        public string MT515_StoredProc { get; set; }
        public string MT535_StoredProc { get; set; }
        public string MT536_StoredProc { get; set; }
        public string MT537_StoredProc { get; set; }
        public string MT540_StoredProc { get; set; }
        public string MT541_StoredProc { get; set; }
        public string MT542_StoredProc { get; set; }
        public string MT543_StoredProc { get; set; }
        public string MT545_StoredProc { get; set; }
        public string MT547_StoredProc { get; set; }
        public string MT548_StoredProc { get; set; }
        public string MT565_StoredProc { get; set; }
        public string MT900_StoredProc { get; set; }
        public string MT910_StoredProc { get; set; }
        public string MT920_StoredProc { get; set; }
        public string MT940_StoredProc { get; set; }
        public string MT941_StoredProc { get; set; }
        public string MT942_StoredProc { get; set; }
        public string MT950_StoredProc { get; set; }
        public string MT970_StoredProc { get; set; }
        public string MT971_StoredProc { get; set; }
        public string MT972_StoredProc { get; set; }
        public string MT973_StoredProc { get; set; }
        public string Pain001_StoredProc { get; set; }
        public string Pacs008_StoredProc { get; set; }
        public string Pain001NatWest_StoredProc { get; set; }
        public string FetchFilePathsStoredProcedure { get; set; }
        public string FetchClientDetailsStoredProcedure { get; set; }
        public string FinExtractFolder { get; set; }
        public string InsertClientSwiftStoredProcedure { get; set; }
        public string InsertFtpFilesStoredProcedure { get; set; }
        public string FetchFileExportsStoredProcedure { get; set; }
        public string FlowPaymentCreateSwiftOpenBankingDomesticPaymentStoredproc { get; set; }
        public string FlowPaymentCreateSwiftOpenBankingInternationalPaymentStoredproc { get; set; }
    }
}