﻿namespace SwiftApi.Models.Classes
{
    public static class RequestStatus
    {
        public const string Completed = "Completed";
        public const string Failed = "Failed";
        public const string Preview = "Preview";
    }
}