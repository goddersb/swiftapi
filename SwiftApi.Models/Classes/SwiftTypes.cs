﻿namespace SwiftApi.Models.Classes
{
    public static class SwiftTypes
    {
        public const string MT101 = "MT101";
        public const string MT103 = "MT103";
        public const string MT192 = "MT192";
        public const string MT199 = "MT199";
        public const string MT200 = "MT200";
        public const string MT202 = "MT202";
        public const string MT210 = "MT210";
        public const string MT292 = "MT292";
        public const string MT299 = "MT299";
        public const string MT380 = "MT380";
        public const string MT381 = "MT381";
        public const string MT392 = "MT392";
        public const string MT399 = "MT399";
        public const string MT492 = "MT492";
        public const string MT499 = "MT499";
        public const string MT502 = "MT502";
        public const string MT508 = "MT508";
        public const string MT509 = "MT509";
        public const string MT515 = "MT515";
        public const string MT535 = "MT535";
        public const string MT536 = "MT536";
        public const string MT537 = "MT537";
        public const string MT540 = "MT540";
        public const string MT541 = "MT541";
        public const string MT542 = "MT542";
        public const string MT543 = "MT543";
        public const string MT545 = "MT545";
        public const string MT547 = "MT547";
        public const string MT548 = "MT548";
        public const string MT565 = "MT565";
        public const string MT592 = "MT592";
        public const string MT599 = "MT599";
        public const string MT692 = "MT692";
        public const string MT699 = "MT699";
        public const string MT792 = "MT792";
        public const string MT799 = "MT799";
        public const string MT892 = "MT892";
        public const string MT899 = "MT899";
        public const string MT900 = "MT900";
        public const string MT910 = "MT910";
        public const string MT920 = "MT920";
        public const string MT940 = "MT940";
        public const string MT941 = "MT941";
        public const string MT942 = "MT942";
        public const string MT950 = "MT950";
        public const string MT970 = "MT970";
        public const string MT971 = "MT971";
        public const string MT972 = "MT972";
        public const string MT973 = "MT973";
        public const string MT992 = "MT992";
        public const string MT999 = "MT999";
        public const string PAIN001 = "PAIN001";
        public const string PAIN001NatWest = "PAIN001_NatWest";
        public const string PACS008 = "PACS008";
        public const string OpenBankingDomesticPayment = "OpenBankingDomesticPayment";
        public const string OpenBankingInternationalPayment = "OpenBankingInternationalPayment";
    }
}