﻿using System;

namespace SwiftApi.Models.Api
{
    public class FileExport
    {
        public int FE_ID { get; set; }

        public string FE_Key { get; set; }

        public string FE_Name { get; set; }

#nullable enable
        public bool? FE_ExportBuffer { get; set; }

        public string? FE_UserModifiedBy { get; set; }

        public DateTime? FE_UserModifiedTime { get; set; }

#nullable disable
    }
}