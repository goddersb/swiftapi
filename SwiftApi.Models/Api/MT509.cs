﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT509
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string OrderSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(16)]
        public string? PreviousOrderRef { get; set; }

        [StringLength(4)]
        public string? LinkedMessageType { get; set; }

#nullable disable

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(5)]
        public string StatusCodeType { get; set; }

        [StringLength(5)]
        public string StatusCode { get; set; }

        [StringLength(5)]
        public string ReasonCodeType { get; set; }

        [StringLength(5)]
        public string ReasonCode { get; set; }

        [StringLength(35)]
        public string Reason { get; set; }

        [StringLength(5)]
        public string BuySellIndicator { get; set; }

        [StringLength(5)]
        public string PaymentIndicator { get; set; }

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(3)]
        public string QuantityCCY { get; set; }

        public double Quantity { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}