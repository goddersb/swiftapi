﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT508
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string OrderSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(16)]
        public string? PreviousOrderRef { get; set; }

        [StringLength(4)]
        public string? LinkedMessageType { get; set; }

#nullable disable

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(35)]
        public string InstrumentDescription { get; set; }

        [StringLength(4)]
        public string QuantityType { get; set; }

        public double Quantity { get; set; }
#nullable enable

        [StringLength(4)]
        public string? TransactionSafeKeepingAccountType { get; set; }

#nullable disable

        [StringLength(35)]
        public string TransactionSafeKeepingAccount { get; set; }

        public DateTime SettlementDate { get; set; }

        [StringLength(4)]
        public string FromBalanceIndicator { get; set; }

        [StringLength(4)]
        public string ToBalanceIndicator { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}