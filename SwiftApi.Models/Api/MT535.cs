﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT535
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string OrderSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(16)]
        public string RelatedRef { get; set; }

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(5)]
        public string PageNumber { get; set; }

        [StringLength(4)]
        public string StatementSequenceType { get; set; }

#nullable enable
        public DateTime? StatementDate { get; set; }
#nullable disable

        [StringLength(4)]
        public string StatementFrequencyIndicatorCode { get; set; }

        [StringLength(4)]
        public string UpdatesIndicatorCode { get; set; }

        [StringLength(4)]
        public string StatementTypeCode { get; set; }

        [StringLength(4)]
        public string StatementBasisCode { get; set; }

        [StringLength(4)]
        public string SafekeepingAccountType { get; set; }

        [StringLength(35)]
        public string SafekeepingAccount { get; set; }

        [StringLength(1)]
        public string ActivityFlag { get; set; }

        [StringLength(1)]
        public string SubSafekeepingStatementFlag { get; set; }

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(35)]
        public string InstrumentDescription { get; set; }

#nullable enable
        public double? DealPrice { get; set; }
#nullable disable

        [StringLength(4)]
        public string DealType { get; set; }

        [StringLength(3)]
        public string DealCcy { get; set; }

        [StringLength(5)]
        public string AggregateBalanceType { get; set; }

#nullable enable
        public double? AggregateBalance { get; set; }
#nullable disable

        [StringLength(4)]
        public string AggregateBalanceSafeKeepingAccountType { get; set; }

        [StringLength(4)]
        public string AggregateBalanceSafeKeepingCode { get; set; }

        [StringLength(11)]
        public string AggregateBalanceSafeKeepingSwiftCode { get; set; }

#nullable enable
        public double? HoldingAmount { get; set; }
#nullable disable

        [StringLength(4)]
        public string HoldingAmountCCY { get; set; }

#nullable enable
        public double? ExchangeRate { get; set; }
        public double? TotalHoldingsValueofPage { get; set; }
#nullable disable

        [StringLength(5)]
        public string TotalHoldingsValueofPageCCY { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}