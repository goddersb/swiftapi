﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT547
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string OrderSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(16)]
        public string RelatedRef { get; set; }

#nullable enable
        public DateTime? TradeDate { get; set; }
        public DateTime? SettleDate { get; set; }
        public DateTime? EffectiveSettleDate { get; set; }
        public double? DealPrice { get; set; }
        public double? DealPerc { get; set; }
#nullable disable

        [StringLength(3)]
        public string DealCcy { get; set; }

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(35)]
        public string InstrumentDescription { get; set; }

        [StringLength(4)]
        public string QuantityType { get; set; }

        public int Quantity { get; set; }

        public double SettlementAmount { get; set; }

        [StringLength(3)]
        public string Ccy { get; set; }

        [StringLength(4)]
        public string SafekeepingAccountType { get; set; }

        [StringLength(50)]
        public string SafekeepingAccount { get; set; }

        [StringLength(4)]
        public string SafeKeepingPlaceCode { get; set; }

        [StringLength(50)]
        public string SafeKeepingSwiftCode { get; set; }

        [StringLength(4)]
        public string TransactionIndicator { get; set; }

        [StringLength(11)]
        public string AgentSwiftCode { get; set; }

        [StringLength(50)]
        public string AgentCodeAndID { get; set; }

        [StringLength(10)]
        public string AgentID { get; set; }

        [StringLength(11)]
        public string CptySwiftCode { get; set; }

        [StringLength(11)]
        public string SettlementSwiftCode { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}