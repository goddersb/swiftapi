﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT973
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(50)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(150)]
        public string Message { get; set; }

        [StringLength(35)]
        public string AccountNumber { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}