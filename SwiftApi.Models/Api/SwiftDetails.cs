﻿namespace SwiftApi.Models.Api
{
    public class SwiftDetails
    {
        public string Message { get; set; }
        public string swiftFile { get; set; }
        public int responseCode { get; set; }
    }
}