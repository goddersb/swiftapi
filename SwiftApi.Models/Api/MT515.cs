﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT515
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string OrderSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(16)]
        public string? PreviousOrderRef { get; set; }

        [StringLength(4)]
        public string? LinkedMessageType { get; set; }

#nullable disable

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(4)]
        public string TransactionTypeIndicator { get; set; }

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(35)]
        public string InstrumentDescription { get; set; }

#nullable enable
        public double? DealPrice { get; set; }
#nullable disable

        [StringLength(4)]
        public string DealType { get; set; }

        [StringLength(3)]
        public string DealCcy { get; set; }

        [StringLength(4)]
        public string QuantityType { get; set; }

        public double Quantity { get; set; }

#nullable enable

        [StringLength(4)]
        public string? TransactionSafeKeepingAccountType { get; set; }

#nullable disable

        [StringLength(35)]
        public string TransactionSafeKeepingAccount { get; set; }

        [StringLength(4)]
        public string TransactionIndicator { get; set; }

        [StringLength(4)]
        public string BuySellIndicator { get; set; }

        [StringLength(4)]
        public string PaymentIndicator { get; set; }

        public DateTime TradeDate { get; set; }

        public DateTime SettlementDate { get; set; }

#nullable enable

        [StringLength(11)]
        public string? BuyerSwiftCode { get; set; }

        [StringLength(11)]
        public string? SellerSwiftCode { get; set; }

#nullable disable

        public double TradeAmount { get; set; }

        [StringLength(3)]
        public string TradeAmountCCY { get; set; }

        public double BrokerCommsAmount { get; set; }

        [StringLength(3)]
        public string BrokerCommsAmountCCY { get; set; }

        public double AccruedInterestAmount { get; set; }

        [StringLength(3)]
        public string AccruedInterestAmountCCY { get; set; }

        public double SettlementAmount { get; set; }

        [StringLength(3)]
        public string SettlementAmountCCY { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}