﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT537
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string OrderSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(16)]
        public string? PreviousOrderRef { get; set; }

        [StringLength(4)]
        public string? LinkedMessageType { get; set; }

        [StringLength(16)]
        public string? AccountNumber { get; set; }

#nullable disable

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(5)]
        public string PageNumber { get; set; }

        [StringLength(4)]
        public string StatementSequenceType { get; set; }

#nullable enable
        public DateTime? StatementDate { get; set; }
#nullable disable

        [StringLength(4)]
        public string StatementFrequencyIndicatorCode { get; set; }

        [StringLength(4)]
        public string UpdatesIndicatorCode { get; set; }

        [StringLength(4)]
        public string StatementStructureCode { get; set; }

        [StringLength(4)]
        public string SafekeepingAccountType { get; set; }

        [StringLength(35)]
        public string SafekeepingAccount { get; set; }

        [StringLength(1)]
        public string ActivityFlag { get; set; }

        [StringLength(5)]
        public string StatusCodeType { get; set; }

        [StringLength(5)]
        public string StatusCode { get; set; }

        [StringLength(5)]
        public string ReasonCodeType { get; set; }

        [StringLength(5)]
        public string ReasonCode { get; set; }

        [StringLength(35)]
        public string Reason { get; set; }

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(35)]
        public string InstrumentDescription { get; set; }

        [StringLength(5)]
        public string PostingQuantityType { get; set; }

#nullable enable
        public double? PostingQuantity { get; set; }

        [StringLength(4)]
        public string? TransactionSafeKeepingAccountType { get; set; }

        [StringLength(4)]
        public string? TransactionSafeKeepingCode { get; set; }

        [StringLength(11)]
        public string? TransactionSafeKeepingSwiftCode { get; set; }

#nullable disable

        [StringLength(4)]
        public string TransactionIndicator { get; set; }

        [StringLength(4)]
        public string DeliveryIndicator { get; set; }

        [StringLength(4)]
        public string PaymentIndicator { get; set; }

        public DateTime TradeDate { get; set; }

        public DateTime SettlementDate { get; set; }

        [DefaultValue(false)]
        public bool IsReceivingAgent { get; set; }

#nullable enable

        [StringLength(11)]
        public string? AgentSwiftCode { get; set; }

        [StringLength(50)]
        public string? AgentCodeAndID { get; set; }

        [StringLength(11)]
        public string? BuyerSellerSwiftCode { get; set; }

        [StringLength(11)]
        public string? SettlementSwiftCode { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}