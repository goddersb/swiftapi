﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT381
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(16)]
        public string PreviousOrderRef { get; set; }

        [StringLength(4)]
        public string LinkMessageType { get; set; }

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(30)]
        public string SafeKeepingAccount { get; set; }

        public string BuySell { get; set; }

        public DateTime OrderDateAndTime { get; set; }
        public DateTime TradeDate { get; set; }
        public DateTime SettlementDate { get; set; }

        public double OrderAmount { get; set; }

        [StringLength(3)]
        public string OrderCcy { get; set; }

        public double ExchangeRate { get; set; }

        [StringLength(35)]
        public string Message { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}