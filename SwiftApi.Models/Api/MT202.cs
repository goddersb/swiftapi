﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT202
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        public DateTime? SettleDate { get; set; }
        public double Amount { get; set; }

        [StringLength(10)]
        public string Ccy { get; set; }

        [StringLength(50)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(30)]
        public string? OrderName { get; set; }

        [StringLength(150)]
        public string? OrderAddress { get; set; }

        [StringLength(100)]
        public string? OrderCity { get; set; }

        [StringLength(50)]
        public string? OrderPostCode { get; set; }

        [StringLength(50)]
        public string? OrderSwift { get; set; }

        [StringLength(50)]
        public string? OrderAccountNumber { get; set; }

        [StringLength(32)]
        public string? OrderIban { get; set; }

        [StringLength(150)]
        public string? OrderOther { get; set; }

        [StringLength(10)]
        public string? OrderVOCode { get; set; }

        [StringLength(50)]
        public string? BeneficiaryRef { get; set; }

        [StringLength(150)]
        public string? BeneficiaryName { get; set; }

        [StringLength(150)]
        public string? BeneficiaryAddress1 { get; set; }

        [StringLength(150)]
        public string? BeneficiaryAddress2 { get; set; }

        [StringLength(50)]
        public string? BeneficiaryPostCode { get; set; }

        [StringLength(50)]
        public string? BeneficiarySwift { get; set; }

        [StringLength(50)]
        public string? BeneficiaryAccountNumber { get; set; }

        [StringLength(32)]
        public string? BeneficiaryIBAN { get; set; }

        [StringLength(10)]
        public string? BeneficiaryCcy { get; set; }

        [StringLength(150)]
        public string? BeneficiarySubBankName { get; set; }

        [StringLength(150)]
        public string? BeneficiarySubBankAddress1 { get; set; }

        [StringLength(150)]
        public string? BeneficiarySubBankAddress2 { get; set; }

        [StringLength(50)]
        public string? BeneficiarySubBankPostCode { get; set; }

        [StringLength(50)]
        public string? BeneficiarySubBankSwift { get; set; }

        [StringLength(32)]
        public string? BeneficiarySubBankIBAN { get; set; }

        [StringLength(150)]
        public string? BeneficiarySubBankOther { get; set; }

        [StringLength(150)]
        public string? BeneficiarySubBankBik { get; set; }

        [StringLength(150)]
        public string? BeneficiarySubBankINN { get; set; }

        [StringLength(150)]
        public string? BeneficiaryBankName { get; set; }

        [StringLength(150)]
        public string? BeneficiaryBankAddress1 { get; set; }

        [StringLength(150)]
        public string? BeneficiaryBankAddress2 { get; set; }

        [StringLength(50)]
        public string? BeneficiaryBankPostCode { get; set; }

        [StringLength(50)]
        public string? BeneficiaryBankSwift { get; set; }

        [StringLength(32)]
        public string? BeneficiaryBankIBAN { get; set; }

        [StringLength(150)]
        public string? BeneficiaryBankBIK { get; set; }

        [StringLength(150)]
        public string? BeneficiaryBankINN { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool SwiftSendRef { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        [DefaultValue(false)]
        public bool ValidateIBAN { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}