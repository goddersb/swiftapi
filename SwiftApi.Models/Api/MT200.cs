﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT200
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(50)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(50)]
        public string OrderRef { get; set; }

        public DateTime? SettleDate { get; set; }
        public double Amount { get; set; }

        [StringLength(10)]
        public string Ccy { get; set; }

#nullable enable

        [StringLength(32)]
        public string? BenIBAN { get; set; }

        [StringLength(32)]
        public string? OrderIBAN { get; set; }

        [StringLength(150)]
        public string? OrderOther { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        [DefaultValue(false)]
        public bool ValidateIBAN { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}