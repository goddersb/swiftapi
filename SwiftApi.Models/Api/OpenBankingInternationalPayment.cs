﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class OpenBankingInternationalPayment
    {
#nullable enable

        [StringLength(50)]
        public string? ConsentID { get; set; }

#nullable disable
        public decimal Amount { get; set; }

        [StringLength(10)]
        public string CCY { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(11)]
        public string? OrderSortCode { get; set; }

#nullable disable

        [StringLength(32)]
        public string OrderIBANorAccountNo { get; set; }

#nullable enable

        [StringLength(70)]
        public string? OrderOther { get; set; }

#nullable disable

        [StringLength(35)]
        public string BenName { get; set; }

        [StringLength(11)]
        public string BenBankSwift { get; set; }

        [StringLength(32)]
        public string BenBankIBAN { get; set; }

        [StringLength(2)]
        public string BenBankCountryCode { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        [DefaultValue(false)]
        public bool ValidateIBAN { get; set; }

#nullable enable

        [DefaultValue("")]
        public string? Email { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}