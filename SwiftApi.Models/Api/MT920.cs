﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT920
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(50)]
        public string OrderRef { get; set; }

        [StringLength(50)]
        public string OrderSwift { get; set; }

        [StringLength(3)]
        public string OrderCcy { get; set; }

        public double OrderAmount { get; set; }

        [StringLength(50)]
        public string OrderAccountNumber { get; set; }

        [StringLength(3)]
        public string OrderType { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}