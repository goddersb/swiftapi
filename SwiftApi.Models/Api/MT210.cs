﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT210
    {
        [StringLength(50)]
        public string ClientSwiftCode { get; set; }

        [StringLength(50)]
        public string BenSwiftCode { get; set; }

        [StringLength(50)]
        public string OrderRef { get; set; }

        [StringLength(50)]
        public string BrokerAccountNo { get; set; }

        [StringLength(30)]
        public string RelatedOrderRef { get; set; }

        public DateTime? SettleDate { get; set; }

        public double Amount { get; set; }
        public string Ccy { get; set; }

        [StringLength(50)]
        public string OrderSwiftCode { get; set; }

        [StringLength(50)]
        public string BenSubBankSwift { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}