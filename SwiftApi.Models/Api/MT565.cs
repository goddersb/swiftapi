﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT565
    {
        [StringLength(11)]
        public string ClientSwiftCode { get; set; }

        [StringLength(11)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(16)]
        public string CorporateActionRef { get; set; }

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(4)]
        public string CorporateActionEventIndicator { get; set; }

        [StringLength(16)]
        public string RelatedOrderRef { get; set; }

        [StringLength(4)]
        public string LinkIndicatorType { get; set; }

        [StringLength(3)]
        public string LinkMessageType { get; set; }

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(4)]
        public string InstrumentQtyType { get; set; }

        public double InstrumentQty { get; set; }

        public string SafeKeepingAccountType { get; set; }

        [StringLength(35)]
        public string SafekeepingAccount { get; set; }

        [StringLength(11)]
        public string BeneficiarySwiftCode { get; set; }

        [StringLength(35)]
        public string BeneficiaryName { get; set; }

        [StringLength(35)]
        public string BeneficiaryAddress { get; set; }

        public bool PreviousActionNotification { get; set; }

        [StringLength(3)]
        public string CorporateActionNumber { get; set; }

        [StringLength(4)]
        public string CorporateActionIndicator { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}