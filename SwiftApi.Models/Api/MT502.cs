﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT502
    {
        [StringLength(30)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(30)]
        public string BrokerAccountNumber { get; set; }

        [StringLength(5)]
        public string TradeType { get; set; }

        [StringLength(16)]
        public string CancelOrderRef { get; set; }

        [StringLength(5)]
        public string IndicatorA { get; set; }

        [StringLength(5)]
        public string IndicatorB { get; set; }

        public string BuySell { get; set; }
        public double Quantity { get; set; }
        public double Amount { get; set; }

        [StringLength(3)]
        public string Ccy { get; set; }

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(5)]
        public string PlaceIndicatorA { get; set; }

        [StringLength(5)]
        public string PlaceIndicatorB { get; set; }

        [StringLength(30)]
        public string PlaceIndicator { get; set; }

        [StringLength(5)]
        public string OrderType { get; set; }

        [StringLength(5)]
        public string OrderTimeLimit { get; set; }

        [StringLength(5)]
        public string OrderPayType { get; set; }

        [StringLength(5)]
        public string OrderSettlementType { get; set; }

        [StringLength(5)]
        public string OrderSettlementCondition { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}