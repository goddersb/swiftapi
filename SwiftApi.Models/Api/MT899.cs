﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT899
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(20)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(50)]
        public string OrderRef { get; set; }

        [StringLength(50)]
        public string OrderRelatedRef { get; set; }

        public string Message { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}