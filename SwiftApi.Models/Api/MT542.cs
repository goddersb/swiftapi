﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT542
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(16)]
        public string? CancelOrderRef { get; set; }

        public DateTime? TradeDate { get; set; }
        public DateTime? SettleDate { get; set; }
#nullable disable
        public bool IsEquity { get; set; }
        public double Amount { get; set; }
#nullable enable

        [StringLength(30)]
        public string? BrokerAccountNo { get; set; }

#nullable disable

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(4)]
        public string SafekeepingPlaceCode { get; set; }

#nullable enable

        [StringLength(11)]
        public string? SafekeepingSwiftCode { get; set; }

        [StringLength(12)]
        public string? AgentSwiftCode { get; set; }

        [StringLength(50)]
        public string? AgentCodeAndID { get; set; }

        [StringLength(12)]
        public string? AgentID { get; set; }

        [StringLength(12)]
        public string? SettlementSwiftCode { get; set; }

        [StringLength(100)]
        public string? Instructions { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}