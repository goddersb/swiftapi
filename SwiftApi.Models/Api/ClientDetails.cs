﻿using System;

namespace SwiftApi.Models.Api
{
    public class ClientDetails
    {
        public string ClientId { get; set; }
        public string Name { set; get; }
        public string CompanyName { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public int TypeId { get; set; }
        public int RequestsAvailable { get; set; }
        public bool Active { get; set; }
        public int RequestsAllocated { get; set; }
        public int RequestsUsed { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? AuthDate { get; set; }
    }
}