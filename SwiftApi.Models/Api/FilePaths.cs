﻿namespace SwiftApi.Models.Api
{
    public class FilePaths
    {
        public string PathName { get; set; }
        public string PathFilePath { get; set; }
    }
}