﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT95
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(50)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(35)]
        public string AccountNumber { get; set; }

        [StringLength(10)]
        public string StatementNumber { get; set; }

        [StringLength(3)]
        public string StatementSequenceNumber { get; set; }

        [DefaultValue(false)]
        public bool OpeningBalanceIsCredit { get; set; }

#nullable enable
        public DateTime? OpeningBalanceStatementDate { get; set; }
#nullable disable

        [StringLength(3)]
        public string OpeningBalanceCCY { get; set; }

        public double OpeningBalanceAmount { get; set; }

#nullable enable

        [StringLength(150)]
        public string? StatementLine { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool ClosingBalanceIsCredit { get; set; }

#nullable enable
        public DateTime? ClosingBalanceStatementDate { get; set; }
#nullable disable

        [StringLength(3)]
        public string ClosingBalanceCCY { get; set; }

        public double ClosingBalanceAmount { get; set; }

        [DefaultValue(false)]
        public bool ClosingAvailableBalanceIsCredit { get; set; }

#nullable enable
        public DateTime? ClosingAvailableBalanceStatementDate { get; set; }
#nullable disable

        [StringLength(3)]
        public string ClosingAvailableBalanceCCY { get; set; }

        public double ClosingAvailableBalanceAmount { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}