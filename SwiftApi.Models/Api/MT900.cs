﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT900
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(50)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(16)]
        public string RelatedOrderRef { get; set; }

        [StringLength(35)]
        public string AccountNumber { get; set; }

#nullable enable
        public DateTime? SettleDate { get; set; }
#nullable disable

        public double Amount { get; set; }

        [StringLength(3)]
        public string Ccy { get; set; }

        [StringLength(150)]
        public string OrderInstitution { get; set; }

        [StringLength(150)]
        public string Information { get; set; }

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}