﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT541
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(20)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(16)]
        public string? PreviousOrderRef { get; set; }

        [StringLength(3)]
        public string? LinkedMessageType { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [StringLength(4)]
        public string TradeType { get; set; }

#nullable enable
        public DateTime? PreparationDate { get; set; }
        public DateTime? TradeDate { get; set; }
        public DateTime? SettleDate { get; set; }
        public double? DealPrice { get; set; }
        public double? DealPerc { get; set; }

        [StringLength(3)]
        public string? DealCcy { get; set; }

        [StringLength(50)]
        public string? InstrumentISIN { get; set; }

#nullable disable

        [StringLength(4)]
        public string QuantityType { get; set; }

        public double Quantity { get; set; }

        [StringLength(4)]
        public string SafeKeepingAccountType { get; set; }

#nullable enable

        [StringLength(50)]
        public string? SafeKeepingAccount { get; set; }

#nullable disable

        [StringLength(4)]
        public string SafeKeepingPlaceCode { get; set; }

#nullable enable

        [StringLength(50)]
        public string? SafeKeepingSwiftCode { get; set; }

        [StringLength(11)]
        public string? BuyerSwiftCode { get; set; }

        [StringLength(35)]
        public string? BuyerName { get; set; }

        [StringLength(35)]
        public string? BuyerAddress { get; set; }

        [StringLength(11)]
        public string? SellerSwiftCode { get; set; }

        [StringLength(35)]
        public string? SellerName { get; set; }

        [StringLength(35)]
        public string? SellerAddress { get; set; }

        [StringLength(50)]
        public string? SettlementSwiftCode { get; set; }

#nullable disable

        [StringLength(4)]
        public string SettlementTransactionType { get; set; }

        [StringLength(50)]
        public string SettlementTransactionIndicator { get; set; }

#nullable enable

        [StringLength(50)]
        public string? AgentCodeAndID { get; set; }

        [StringLength(10)]
        public string? AgentID { get; set; }

#nullable disable

        [StringLength(4)]
        public string SettlementAmountType { get; set; }

#nullable enable
        public double? SettlementAmount { get; set; }

        [StringLength(3)]
        public string? SettlementCcy { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}