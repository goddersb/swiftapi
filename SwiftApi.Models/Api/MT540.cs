﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT540
    {
        [StringLength(30)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string BrokerSwiftCode { get; set; }

        [StringLength(4)]
        public string TradeType { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

#nullable enable

        [StringLength(16)]
        public string? CancelOrderRef { get; set; }

        public DateTime? TradeDate { get; set; }
        public DateTime? SettleDate { get; set; }
#nullable disable
        public double Amount { get; set; }

        public bool IsEquity { get; set; }
#nullable enable

        [StringLength(30)]
        public string? BrokerAccountNumber { get; set; }

#nullable disable

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(4)]
        public string SafeKeepingPlaceCode { get; set; }

#nullable enable

        [StringLength(11)]
        public string? SafeKeepingSwiftCode { get; set; }

        [StringLength(12)]
        public string? AgentSwiftCode { get; set; }

        [StringLength(30)]
        public string? AgentCodeAndID { get; set; }

        [StringLength(12)]
        public string? AgentID { get; set; }

        [StringLength(12)]
        public string? SettlementSwiftCode { get; set; }

        [StringLength(100)]
        public string? Instructions { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}