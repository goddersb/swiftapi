﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SwiftApi.Models.Api
{
    public class MT548
    {
        [StringLength(20)]
        public string ClientSwiftCode { get; set; }

        [StringLength(30)]
        public string OrderSwiftCode { get; set; }

        [StringLength(16)]
        public string OrderRef { get; set; }

        [StringLength(16)]
        public string RelatedRef { get; set; }

        [StringLength(4)]
        public string LinkedMessageType { get; set; }

        [StringLength(4)]
        public string TradeType { get; set; }

#nullable enable
        public DateTime? SettleDate { get; set; }
#nullable disable

        [StringLength(30)]
        public string InstrumentISIN { get; set; }

        [StringLength(35)]
        public string InstrumentDescription { get; set; }

        [StringLength(5)]
        public string QuantityType { get; set; }

        public int Quantity { get; set; }
#nullable enable
        public double? SettlementAmount { get; set; }
#nullable disable

        [StringLength(3)]
        public string Ccy { get; set; }

        [StringLength(5)]
        public string StatusCodeType { get; set; }

        [StringLength(5)]
        public string StatusCode { get; set; }

        [StringLength(5)]
        public string ReasonCodeType { get; set; }

        [StringLength(5)]
        public string ReasonCode { get; set; }

        [StringLength(35)]
        public string Reason { get; set; }

        [StringLength(4)]
        public string SafekeepingAccountType { get; set; }

        [StringLength(50)]
        public string SafekeepingAccount { get; set; }

        [StringLength(4)]
        public string TransactionIndicator { get; set; }

        [StringLength(4)]
        public string DeliveryIndicator { get; set; }

        [StringLength(4)]
        public string PaymentIndicator { get; set; }

#nullable enable

        [StringLength(11)]
        public string? AgentSwiftCode { get; set; }

        [StringLength(50)]
        public string? AgentCodeAndID { get; set; }

        [StringLength(10)]
        public string? AgentID { get; set; }

        [StringLength(11)]
        public string? SettlementSwiftCode { get; set; }

#nullable disable

        [DefaultValue(false)]
        public bool SwiftUrgent { get; set; }

        [DefaultValue(false)]
        public bool ValidateSwiftCode { get; set; }

        public string Email { get; set; }

        [DefaultValue(false)]
        public bool Ftp { get; set; }
    }
}